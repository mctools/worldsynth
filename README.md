![WorldSynth logo and title](readme-images/LogoTitle.png "")

# WorldSynth
WorldSynth is a graphically editable terrain generator.

![Synth example](readme-images/SynthExampleImage.png "Example of a synth generating caverns")

## Building with gradlew
Run: './gradlew build'

## Running with gradle
Run: './gradlew run'

## Seting up dev enviroment for Eclipse
- Run: './gradlew eclipse'
- Import project to Eclipse
- Setup the project build path JRE System Library to be a Java8 JDK with JavaFX included