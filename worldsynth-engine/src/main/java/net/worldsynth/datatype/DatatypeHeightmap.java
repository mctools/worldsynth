/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.BuildExtent;

public class DatatypeHeightmap extends AbstractDatatype implements IHeightmap {
	
	private float[][] heightmap;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per heightmap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the heightmap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeHeightmap() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the heightmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeHeightmap(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setHeightmap(float[][] heightmap) {
		int paramWidth = heightmap.length;
		int paramLength = heightmap[0].length;
		if (paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Heightmap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		this.heightmap = heightmap;
	}
	
	@Override
	public float[][] getHeightmap() {
		return heightmap;
	}
	
	@Override
	public boolean isLocalContained(int x, int z) {
		if (x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public float getLocalHeight(int x, int z) {
		return heightmap[x][z];
	}
	
	@Override
	public float getLocalLerpHeight(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, mapPointsWidth-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, mapPointsLength-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		double val = (1.0-v)*(1.0-u)*heightmap[x1][z1] + (1.0-v)*u*heightmap[x2][z1] + v*(1.0-u)*heightmap[x1][z2] + v*u*heightmap[x2][z2];
		
		return (float) val;
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public float getGlobalHeight(double x, double z) {
		x = (x - extent.getX()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return heightmap[(int) x][(int) z];
	}
	
	@Override
	public float getGlobalLerpHeight(double x, double z) {
		double localX = (x - extent.getX()) / resolution;
		double localZ = (z - extent.getZ()) / resolution;
		
		return getLocalLerpHeight(localX, localZ);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Heightmap";
	}

	@Override
	public DatatypeHeightmap clone() {
		DatatypeHeightmap dhm = new DatatypeHeightmap(extent, resolution);
		if (heightmap != null) {
			float[][] newMap = new float[mapPointsWidth][mapPointsLength];
			for (int u = 0; u < mapPointsWidth; u++) {
				newMap[u] = heightmap[u].clone();
			}
			dhm.heightmap = newMap;
		}
		return dhm;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeHeightmap)) return false;

		DatatypeHeightmap castRequestData = (DatatypeHeightmap) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeHeightmap(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
