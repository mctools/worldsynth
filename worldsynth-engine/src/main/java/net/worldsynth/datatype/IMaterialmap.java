/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import net.worldsynth.material.MaterialState;

public interface IMaterialmap {
	
	public void setMaterialmap(MaterialState<?, ?>[][] biomemap);
	
	public MaterialState<?, ?>[][] getMaterialmap();
	
	public boolean isLocalContained(int x, int z);
	
	public MaterialState<?, ?> getLocalMaterial(int x, int z);

	public boolean isGlobalContained(double x, double z);
	
	public MaterialState<?, ?> getGlobalMaterial(double x, double z);
}
