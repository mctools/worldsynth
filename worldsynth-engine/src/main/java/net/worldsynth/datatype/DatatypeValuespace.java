/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.BuildExtent;

public class DatatypeValuespace extends AbstractDatatype implements IValuespace {
	
	private float[][][] valuespace;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per valuespace point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the valuespace
	 */
	public final int spacePointsWidth, spacePointsHeight, spacePointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeValuespace() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the valuespace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeValuespace(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		spacePointsHeight = (int) Math.ceil(extent.getHeight() / resolution);
		spacePointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setValuespace(float[][][] valuespace) {
		int paramWidth = valuespace.length;
		int paramHeight = valuespace[0].length;
		int paramLength = valuespace[0][0].length;
		if (paramWidth != spacePointsWidth || paramHeight != spacePointsHeight || paramLength != spacePointsLength) {
			throw new IllegalArgumentException("Valuespace data has wrong size. Expected a " + spacePointsWidth + "x" + spacePointsHeight + "x" + spacePointsLength+ " map, got a " + paramWidth + "x" + paramHeight + "x" + paramLength  + " space.");
		}
		this.valuespace = valuespace;
	}
	
	@Override
	public float[][][] getValuespace() {
		return valuespace;
	}
	
	@Override
	public boolean isLocalContained(int x, int y, int z) {
		if (x < 0 || x >= spacePointsWidth || y < 0 || y >= spacePointsHeight || z < 0 || z >= spacePointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public float getLocalValue(int x, int y, int z) {
		return valuespace[x][y][z];
	}
	
	@Override
	public float getLocalLerpValue(double x, double y, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, spacePointsWidth-1);
		int y1 = (int) y;
		int y2 = y1+1;
		y2 = Math.min(y2, spacePointsHeight-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, spacePointsLength-1);
		
		double u = x - Math.floor(x);
		double v = y - Math.floor(y);
		double w = z - Math.floor(z);
		
		double valy1 = (1.0-w)*(1.0-u)*valuespace[x1][y1][z1] + (1.0-w)*u*valuespace[x2][y1][z1] + w*(1.0-u)*valuespace[x1][y1][z2] + w*u*valuespace[x2][y1][z2];
		double valy2 = (1.0-w)*(1.0-u)*valuespace[x1][y2][z1] + (1.0-w)*u*valuespace[x2][y2][z1] + w*(1.0-u)*valuespace[x1][y2][z2] + w*u*valuespace[x2][y2][z2];
		double val = (1.0-v)*valy1 + v*valy2;
		
		return (float) val;
	}
	
	@Override
	public boolean isGlobalContained(double x, double y, double z) {
		return extent.isContained(x, y, z);
	}
	
	@Override
	public float getGlobalValue(double x, double y, double z) {
		x = (x - extent.getX()) / resolution;
		y = (y - extent.getY()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return valuespace[(int) x][(int) y][(int) z];
	}
	
	@Override
	public float getGlobalLerpValue(double x, double y, double z) {
		double localX = (x - extent.getX()) / resolution;
		double localY = (y - extent.getY()) / resolution;
		double localZ = (z - extent.getZ()) / resolution;
		
		return getLocalLerpValue(localX, localY, localZ);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Valuespace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeValuespace dvs = new DatatypeValuespace(extent, resolution);
		if (valuespace != null) {
			float [][][] newSpace = new float[spacePointsWidth][spacePointsHeight][spacePointsLength];
			for (int u = 0; u < spacePointsWidth; u++) {
				for (int v = 0; v < spacePointsHeight; v++) {
					newSpace[u][v] = valuespace[u][v].clone();
				}
			}
			dvs.valuespace = newSpace;
		}
		return dvs;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeValuespace)) return false;

		DatatypeValuespace castRequestData = (DatatypeValuespace) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeValuespace(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
