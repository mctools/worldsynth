/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.extent.BuildExtent;

public class DatatypeObjects extends AbstractDatatype {
	
	public LocatedCustomObject[] objects;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * The resolutions of units per valuespace point
	 */
	public final double resolution;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeObjects() {
		this(null, new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	public DatatypeObjects(LocatedCustomObject[] objects, BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		this.objects = objects;
	}
	
	public DatatypeObjects(double[] objectx, double[] objecty, double[] objectz, long[] objectseed, BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		if (objectx.length == objecty.length && objecty.length == objectz.length && objectz.length == objectseed.length) {
			objects = new LocatedCustomObject[objectx.length];
			for (int i = 0; i < objectx.length; i++) {
				objects[i] = new LocatedCustomObject(objectx[i], objecty[i], objectz[i], objectseed[i]);
			}
		}
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(128, 0, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Objects";
	}

	@Override
	public AbstractDatatype clone() {
		if (this.objects != null) {
			LocatedCustomObject[] objects = new LocatedCustomObject[this.objects.length];
			for (int i = 0; i < objects.length; i++) {
				objects[i] = this.objects[i].clone();
			}
			return new DatatypeObjects(objects, extent, resolution);
		}
		return new DatatypeObjects();
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeObjects)) return false;

		DatatypeObjects castRequestData = (DatatypeObjects) requestData;
		if (castRequestData.objects.length != objects.length) return false;
		for (int i = 0; i < objects.length; i++) {
			LocatedCustomObject obj1 = castRequestData.objects[i];
			LocatedCustomObject obj2 = objects[i];
			boolean objectComparable = obj1.getX() == obj2.getX()
					&& obj1.getY() == obj2.getY()
					&& obj1.getZ() == obj2.getZ()
					&& obj1.getSeed() == obj2.getSeed();
			if (!objectComparable) return false;
		}
		return true;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		LocatedCustomObject[] objects = new LocatedCustomObject[10];
		for (int i = 0; i < objects.length; i++) {
			objects[i] = new LocatedCustomObject(0, 0, 0, i);
		}
		return new DatatypeObjects(objects, new BuildExtent(x, y, z, width, height, length), resolution);
	}
	
	public LocatedCustomObject[] getLocatedObjects() {
		return objects;
	}
}
