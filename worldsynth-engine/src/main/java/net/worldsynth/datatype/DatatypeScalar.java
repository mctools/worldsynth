/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeScalar extends AbstractDatatype implements IScalar {
	
	private double value;
	
	@Override
	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public double getValue() {
		return value;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 85, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Scalar";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeScalar df = new DatatypeScalar();
		df.value = value;
		return df;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeScalar)) return false;

		return true;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeScalar();
	}
}
