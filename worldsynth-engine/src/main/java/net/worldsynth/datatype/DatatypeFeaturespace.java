/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.featurepoint.Featurepoint3D;

public class DatatypeFeaturespace extends AbstractDatatype implements IFeaturespace {
	
	private Featurepoint3D[] points;
	
	/**
	 * The extent of the data
	 */
	public final BuildExtent extent;
	
	/**
	 * Resolution is not mainly used while generating or performing modifications to featuremaps, but is used when
	 * requesting other datatypes as inputs for the various operations.
	 */
	public final double resolution;
	
	public DatatypeFeaturespace() {
		this(new BuildExtent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution
	 */
	public DatatypeFeaturespace(BuildExtent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
	}
	
	@Override
	public void setFeaturepoints(Featurepoint3D[] points) {
		this.points = points;
	}
	
	@Override
	public Featurepoint3D[] getFeaturepoints() {
		return points;
	}
	
	@Override
	public boolean isGlobalContained(double x, double y, double z) {
		return extent.isContained(x, y, z);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(143, 65, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Featurespace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeaturespace dvb = new DatatypeFeaturespace(extent, resolution);
		if (points != null) {
			dvb.points = points.clone();
		}
		return dvb;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeFeaturespace)) return false;

		DatatypeFeaturespace castRequestData = (DatatypeFeaturespace) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeaturespace(new BuildExtent(x, y, z, width, height, length), resolution);
	}
}
