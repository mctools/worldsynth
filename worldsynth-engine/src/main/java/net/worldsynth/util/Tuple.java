/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util;

public class Tuple<T> {
	
	public final T[] values;
	
	@SuppressWarnings("unchecked")
	public Tuple(T ... defaultValues) {
		this.values = defaultValues;
	}
	
	public int getTupleSize() {
		return values.length;
	}
	
	public void setValue(int index, T value) {
		values[index] = value;
	}
	
	public T getValue(int index) {
		return values[index];
	}
}
