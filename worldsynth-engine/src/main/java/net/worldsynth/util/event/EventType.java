/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

public class EventType<T extends Event> {

	public static final EventType<Event> ROOT = new EventType("EVENT");

	private final EventType<? super T> superType;

	private final String name;

	private EventType(final String name) {
		this.superType = null;
		this.name = name;
	}

	public EventType(final EventType<? super T> superType, final String name) {
		if (superType == null) {
			throw new NullPointerException("Event super type must not be null!");
		}

		this.superType = superType;
		this.name = name;
	}

	public boolean isSubtypeOf(EventType<?> eventType) {
		if (superType == eventType) {
			return true;
		} else if (superType == null) {
			return false;
		}
		return superType.isSubtypeOf(eventType);
	}
}
