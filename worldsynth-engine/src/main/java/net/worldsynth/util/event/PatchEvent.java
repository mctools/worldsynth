/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class PatchEvent extends Event implements HistoricalEvent {

	public static final EventType<PatchEvent> PATCH_ANY = new EventType<>(Event.ANY, "PATCH_ANY");
	public static final EventType<PatchEvent> PATCH_MODULE_ADDED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_MODULE_ADDED");
	public static final EventType<PatchEvent> PATCH_MODULE_REMOVED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_MODULE_REMOVED");
	public static final EventType<PatchEvent> PATCH_MODULE_MODIFIED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_MODULE_MODIFIED");
	public static final EventType<PatchEvent> PATCH_CONNECTOR_ADDED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_CONNECTOR_ADDED");
	public static final EventType<PatchEvent> PATCH_CONNECTOR_REMOVED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_CONNECTOR_REMOVED");

	private WrapperEvent wrapperEvent;
	private ModuleWrapper wrapper;
	private ModuleConnector moduleConnector;

	public PatchEvent(Patch source, EventType<PatchEvent> eventType, WrapperEvent wrapperEvent) {
		super(source, eventType);
		this.wrapperEvent = wrapperEvent;
		this.wrapper = (ModuleWrapper) wrapperEvent.getSource();
	}

	public PatchEvent(Patch source, EventType<PatchEvent> eventType, ModuleWrapper wrapper) {
		super(source, eventType);
		this.wrapper = wrapper;
	}

	public PatchEvent(Patch source, EventType<PatchEvent> eventType, ModuleConnector moduleConnector) {
		super(source, eventType);
		this.moduleConnector = moduleConnector;
	}

	public WrapperEvent getWrapperEvent() {
		return wrapperEvent;
	}

	public ModuleWrapper getWrapper() {
		return wrapper;
	}

	public ModuleConnector getModuleConnector() {
		return moduleConnector;
	}

	@Override
	public void undo() {
		Patch sourcePatch = (Patch) getSource();

		if (getEventType() == PATCH_MODULE_ADDED) {
			sourcePatch.removeModuleWrapper(getWrapper());
		}
		else if (getEventType() == PATCH_MODULE_REMOVED) {
			sourcePatch.addModuleWrapper(getWrapper());
		}
		else if (getEventType() == PATCH_CONNECTOR_ADDED) {
			sourcePatch.removeModuleConnector(getModuleConnector());
		}
		else if (getEventType() == PATCH_CONNECTOR_REMOVED) {
			sourcePatch.addModuleConnector(getModuleConnector());
		}
		else if (getEventType() == PATCH_MODULE_MODIFIED) {
			getWrapperEvent().undo();
		}
	}

	@Override
	public void redo() {
		Patch sourcePatch = (Patch) getSource();

		if (getEventType() == PATCH_MODULE_ADDED) {
			sourcePatch.addModuleWrapper(getWrapper());
		}
		else if (getEventType() == PATCH_MODULE_REMOVED) {
			sourcePatch.removeModuleWrapper(getWrapper());
		}
		else if (getEventType() == PATCH_CONNECTOR_ADDED) {
			sourcePatch.addModuleConnector(getModuleConnector());
		}
		else if (getEventType() == PATCH_CONNECTOR_REMOVED) {
			sourcePatch.removeModuleConnector(getModuleConnector());
		}
		else if (getEventType() == PATCH_MODULE_MODIFIED) {
			getWrapperEvent().redo();
		}
	}
}
