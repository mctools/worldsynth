/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class EventDispatcher<T extends Event> {

	private PriorityQueue<Entry<T>> eventHandlers = new PriorityQueue<>();

	public void addEventHandler(EventType<T> eventType, EventHandler<T> handler) {
		eventHandlers.add(new Entry<>(eventType, handler, 0));
	}

	public void addEventHandler(EventType<T> eventType, EventHandler<T> handler, int priority) {
		eventHandlers.add(new Entry<>(eventType, handler, priority));
	}

	private ArrayList<Object> removableHandlers = new ArrayList<>();

	public void removeEventHandler(EventType<T> eventType, EventHandler<T> handler) {
		removableHandlers.add(new Entry<>(eventType, handler, 0));
		if (!isDispatching) realizeEventHandlerRemovals();
	}

	public void removeEventHandler(EventType<T> eventType, EventHandler<T> handler, int priority) {
		removableHandlers.add(new Entry<>(eventType, handler, priority));
		if (!isDispatching) realizeEventHandlerRemovals();
	}

	private void realizeEventHandlerRemovals() {
		for (Object handler: removableHandlers) {
			eventHandlers.remove(handler);
		}
		removableHandlers.clear();
	}

	private boolean isDispatching = false;
	public void dispatchEvent(T event) {
		isDispatching = true;
		eventHandlers.forEach(eh -> {
			if (event.getEventType() == eh.eventType || event.getEventType().isSubtypeOf(eh.eventType)) {
				eh.handler.handle(event);
			}
		});
		isDispatching = false;
		realizeEventHandlerRemovals();
	}

	private record Entry<T extends Event>(EventType<T> eventType, EventHandler<T> handler, int priority) implements Comparable<Entry> {
		@Override
		public int compareTo(Entry o) {
			return priority - o.priority;
		}
	}
}
