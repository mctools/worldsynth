/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import net.worldsynth.module.AbstractModule;

public class ModuleEvent extends Event implements HistoricalEvent {

	public static final EventType<ModuleEvent> MODULE_ANY = new EventType<>(Event.ANY, "MODULE_ANY");
	public static final EventType<ModuleEvent> MODULE_PARAMETERS_CHANGED = new EventType<>(ModuleEvent.MODULE_ANY, "MODULE_PARAMETERS_CHANGED");
	public static final EventType<ModuleEvent> MODULE_IO_CHANGED = new EventType<>(ModuleEvent.MODULE_ANY, "MODULE_IO_CHANGED");

	private ParameterEvent parameterEvent;

	public ModuleEvent(AbstractModule source, EventType<ModuleEvent> eventType) {
		super(source, eventType);
	}

	public ModuleEvent(AbstractModule source, EventType<ModuleEvent> eventType, ParameterEvent parameterEvent) {
		super(source, eventType);
		this.parameterEvent = parameterEvent;
	}

	public ParameterEvent getParameterEvent() {
		return parameterEvent;
	}

	@Override
	public void undo() {
		if (getEventType() == MODULE_PARAMETERS_CHANGED) {
			getParameterEvent().undo();
		}
	}

	@Override
	public void redo() {
		if (getEventType() == MODULE_PARAMETERS_CHANGED) {
			getParameterEvent().redo();
		}
	}
}
