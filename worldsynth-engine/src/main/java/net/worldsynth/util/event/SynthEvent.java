/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import net.worldsynth.synth.Synth;

public class SynthEvent extends Event implements HistoricalEvent {

	public static final EventType<SynthEvent> SYNTH_ANY = new EventType<>(Event.ANY, "SYNTH_ANY");
	public static final EventType<SynthEvent> SYNTH_PATCH = new EventType<>(SynthEvent.SYNTH_ANY, "SYNTH_PATCH");
	public static final EventType<SynthEvent> SYNTH_EXTENT = new EventType<>(SynthEvent.SYNTH_ANY, "SYNTH_EXTENT");

	private PatchEvent patchEvent;
	private ExtentManagerEvent extentManagerEvent;

	public SynthEvent(Synth source, PatchEvent patchEvent) {
		super(source, SYNTH_PATCH);
		this.patchEvent = patchEvent;
	}

	public SynthEvent(Synth source, ExtentManagerEvent extentManagerEvent) {
		super(source, SYNTH_EXTENT);
		this.extentManagerEvent = extentManagerEvent;
	}

	public PatchEvent getPatchEvent() {
		return patchEvent;
	}

	public ExtentManagerEvent getExtentManagerEvent() {
		return extentManagerEvent;
	}

	@Override
	public void undo() {
		Synth sourceSynth = (Synth) getSource();

		if (getEventType() == SYNTH_PATCH) {
			getPatchEvent().undo();
		}
		else if (getEventType() == SYNTH_EXTENT) {
			getExtentManagerEvent().undo();
		}
	}

	@Override
	public void redo() {
		Synth sourceSynth = (Synth) getSource();

		if (getEventType() == SYNTH_PATCH) {
			getPatchEvent().redo();
		}
		else if (getEventType() == SYNTH_EXTENT) {
			getExtentManagerEvent().redo();
		}
	}
}
