/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import java.util.EventObject;

public class Event extends EventObject {

	public static final EventType<Event> ANY = EventType.ROOT;

	private final EventType eventType;

	public Event(Object source, EventType eventType) {
		super(source);
		this.eventType = eventType;
	}

	public final EventType getEventType() {
		return eventType;
	}
}
