/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.event;

import com.fasterxml.jackson.databind.JsonNode;
import net.worldsynth.extent.WorldExtent;

public class ExtentEvent extends Event implements HistoricalEvent {

	public static final EventType<ExtentEvent> EXTENT_ANY = new EventType<>(Event.ANY, "EXTENT_ANY");
	public static final EventType<ExtentEvent> EXTENT_ADDED = new EventType<>(ExtentEvent.EXTENT_ANY, "EXTENT_ADDED");
	public static final EventType<ExtentEvent> EXTENT_EDITED = new EventType<>(ExtentEvent.EXTENT_ANY, "EXTENT_EDITED");
	public static final EventType<ExtentEvent> EXTENT_REMOVED = new EventType<>(ExtentEvent.EXTENT_ANY, "EXTENT_REMOVED");

	private JsonNode oldJson, newJson;

	public ExtentEvent(WorldExtent source, EventType<ExtentEvent> eventType) {
		super(source, eventType);
	}

	public ExtentEvent(WorldExtent source, EventType<ExtentEvent> eventType, JsonNode oldJson, JsonNode newJson) {
		super(source, eventType);
		this.oldJson = oldJson;
		this.newJson = newJson;
	}

	@Override
	public void undo() {
		WorldExtent sourceExtent = (WorldExtent) getSource();

		if (getEventType() == EXTENT_EDITED) {
			sourceExtent.fromJson(oldJson);
		}
	}

	@Override
	public void redo() {
		WorldExtent sourceExtent = (WorldExtent) getSource();

		if (getEventType() == EXTENT_EDITED) {
			sourceExtent.fromJson(newJson);
		}
	}
}
