/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util;

import java.io.File;

public class ExecutionInfo {
	
	public static File getWorldSynthDirectory() {
		String path = ExecutionInfo.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.replaceAll("%20", " ");
		// When running from eclipse
		if (path.matches(".+/bin.*")) {
			path = path.substring(0, path.lastIndexOf("/bin"));
		}
		// When running from gradle
		else if (path.matches(".+/build.*")) {
			path = path.substring(0, path.lastIndexOf("/build"));
		}
		// When running from gradle runnable build
		else if (path.matches(".+/lib.*")) {
			path = path.substring(0, path.lastIndexOf("/lib"));
		}
		else {
			path = path.substring(0, path.lastIndexOf("/"));
		}
		return new File(path);
	}
	
	public static String getSystemOsName() {
		return System.getProperty("os.name");
	}
	
	public static boolean systemIsWindows() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("windows");
	}

	public static boolean systemIsMac() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("mac");
	}

	public static boolean systemIsUnix() {
		String OS = getSystemOsName().toLowerCase();
		return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
	}

	public static boolean systemIsSolaris() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("sunos");
	}
}
