/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util;

public class HeightmapUtil {
	
	/**
	 * Applies a mask to the given heightmap. Areas that are masked out are replaced
	 * with 0 through linear interpolation.
	 * 
	 * @param map              The map to be masked
	 * @param mask
	 * @param normalizedHeight
	 */
	public static void applyMask(final float[][] map, float[][] mask, float normalizedHeight) {
		int mpw = map.length;
		int mpl = map[0].length;
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = map[u][v] * mask[u][v]/normalizedHeight;
			}
		}
	}
	
	/**
	 * Applies a mask to the given heightmap. Areas that are masked out are replaced
	 * with the input heightmap through linear interpolation.
	 * 
	 * @param map              The map to be masked
	 * @param inputMap         The input map to yes where the map is masked out
	 * @param mask
	 * @param normalizedHeight
	 */
	public static void applyMask(final float[][] map, float[][] inputMap, float[][] mask, float normalizedHeight) {
		int mpw = map.length;
		int mpl = map[0].length;
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = (map[u][v] - inputMap[u][v]) * mask[u][v]/normalizedHeight + inputMap[u][v];
			}
		}
	}
}
