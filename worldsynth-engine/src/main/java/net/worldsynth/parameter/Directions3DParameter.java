/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.util.LinkedHashMap;

import net.worldsynth.util.Direction;

public class Directions3DParameter extends AbstractSelctionSetParameter<Direction> {
	
	private static LinkedHashMap<Direction, Boolean> createSelectionSet(boolean north, boolean south, boolean east, boolean west, boolean up, boolean down) {
		LinkedHashMap<Direction, Boolean> selectionSet = new LinkedHashMap<Direction, Boolean>();
		
		selectionSet.put(Direction.NORTH, north);
		selectionSet.put(Direction.SOUTH, south);
		selectionSet.put(Direction.EAST, east);
		selectionSet.put(Direction.WEST, west);
		selectionSet.put(Direction.UP, up);
		selectionSet.put(Direction.DOWN, down);
		
		return selectionSet;
	}
	
	public Directions3DParameter(String name, String displayName, String description, boolean north, boolean south, boolean east, boolean west, boolean up, boolean down) {
		super(name, displayName, description, createSelectionSet(north, south, east, west, up, down));
	}
}
