/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.IntegerParameterField;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class IntegerParameter extends AbstractParameter<Integer> {
	
	protected int min;
	protected int max;
	
	protected int sliderMin;
	protected int sliderMax;
	
	protected UiType uiType;
	
	public IntegerParameter(String name, String displayName, String description, int defaultValue, int min, int max) {
		this(name, displayName, description, defaultValue, min, max, min, max);
	}
	
	public IntegerParameter(String name, String displayName, String description, int defaultValue, int min, int max, int sliderMin, int sliderMax) {
		this(name, displayName, description, UiType.SLIDER, defaultValue, min, max, sliderMin, sliderMax);
	}
	
	public IntegerParameter(String name, String displayName, String description, UiType uiType, int defaultValue, int min, int max) {
		this(name, displayName, description, uiType, defaultValue, min, max, min, max);
	}
	
	public IntegerParameter(String name, String displayName, String description, UiType uiType, int defaultValue, int min, int max, int sliderMin, int sliderMax) {
		super(name, displayName, description, defaultValue);
		this.min = min;
		this.max = max;
		this.sliderMin = sliderMin;
		this.sliderMax = sliderMax;
		this.uiType = uiType;
	}
	
	@Override
	public ParameterUiElement<Integer> getUi() {
		if (uiType == UiType.FIELD) {
			return new IntegerParameterField(this);
		}
		return new IntegerParameterSlider(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue(node.asInt());
	}
	
	public int getMin() {
		return min;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getSliderMin() {
		return sliderMin;
	}
	
	public int getSliderMax() {
		return sliderMax;
	}
	
	public enum UiType {
		SLIDER,
		FIELD;
	}
}
