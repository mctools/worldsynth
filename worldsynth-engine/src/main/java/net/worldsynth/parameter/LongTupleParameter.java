/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;

public class LongTupleParameter extends AbstractNumberTupleParameter<Long> {

	public LongTupleParameter(String name, String displayName, String description, Long min, Long max, Long ... defaultValues) {
		super(name, displayName, description, min, max, defaultValues);
	}

	@Override
	public Long parseValue(JsonNode valueNode) throws NumberFormatException {
		return valueNode.asLong();
	}
	
	@Override
	public Long parseValue(String valueString) throws NumberFormatException {
		return Long.parseLong(valueString);
	}

	@Override
	public boolean insideBounds(Long value) {
		return  getMin() <= value && value <= getMax();
	}
}
