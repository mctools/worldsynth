/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.list;

import java.util.ArrayList;
import java.util.List;

public class RowList<S> {
	private ArrayList<Row<S>> rowList = new ArrayList<Row<S>>();
	
	public RowList(List<S> list) {
		for (S value: list) {
			rowList.add(new Row<S>(value));
		}
	}
	
	public ArrayList<Row<S>> getRowList() {
		return rowList;
	}
}
