/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.list;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import net.worldsynth.parameter.table.TableColumn;
import net.worldsynth.parameter.table.TableParameter;

public abstract class ListParameter<T extends Object> extends TableParameter<Row<T>> {
	
	public ListParameter(String name, String displayName, String description, List<T> defaultValue, Supplier<TableColumn<Row<T>, T>> column) {
		super(name, displayName, description, new RowList<T>(defaultValue).getRowList(), column.get());
	}
	
	public List<T> getValueList() {
		ArrayList<T> list = new ArrayList<T>();
		for (Row<T> row: getValue()) {
			list.add(row.rowValue);
		}
		return list;
	}
	
	public void setValueList(List<T> value) {
		setValue(new RowList<T>(value).getRowList());
	}
}
