/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.FileListParameterUi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileListParameter extends AbstractParameter<List<File>> {

	public FileListParameter(String name, String displayName, String description, List<File> defaultValue) {
		super(name, displayName, description, defaultValue);
	}

	@Override
	public ParameterUiElement<List<File>> getUi() {
		return new FileListParameterUi(this);
	}

	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();

		ArrayNode filesNode = objectMapper.createArrayNode();
		for (File f : getValue()) {
			filesNode.add(f.getAbsolutePath());
		}

		return filesNode;
	}

	@Override
	public void fromJson(JsonNode node) {
		List<File> objectFiles = new ArrayList<>();
		for (JsonNode e : node) {
			objectFiles.add(new File(e.asText()));
		}
		setValue(objectFiles);
	}
}
