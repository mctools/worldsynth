/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import net.worldsynth.util.Axis;

import java.util.LinkedHashMap;

public class Axes3DParameter extends AbstractSelctionSetParameter<Axis> {

	private static LinkedHashMap<Axis, Boolean> createSelectionSet(boolean x, boolean y, boolean z) {
		LinkedHashMap<Axis, Boolean> selectionSet = new LinkedHashMap<Axis, Boolean>();

		selectionSet.put(Axis.X, x);
		selectionSet.put(Axis.Y, y);
		selectionSet.put(Axis.Z, z);

		return selectionSet;
	}

	public Axes3DParameter(String name, String displayName, String description, boolean x, boolean y, boolean z) {
		super(name, displayName, description, createSelectionSet(x, y, z));
	}
}
