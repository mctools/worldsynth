/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import java.util.function.Consumer;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.event.*;

public abstract class AbstractParameter<T extends Object> {
	
	private T value;
	
	protected final String name;
	protected final String displayName;
	protected final String description;

	protected final EventDispatcher<ParameterEvent> eventDispatcher = new EventDispatcher<>();
	
	public AbstractParameter(String name, String displayName, String description, T defaultValue) {
		this.name = name;
		this.displayName = displayName;
		this.description = description;
		this.value = defaultValue;
	}
	
	public final String getName() {
		return name;
	}
	
	public final String getDisplayName() {
		return displayName;
	}
	
	public final String getDescription() {
		return description;
	}
	
	public final T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		if (this.value == value) return;

		JsonNode oldJson = toJson();

		this.value = value;
		if (onChange != null) {
			onChange.accept(value);
		}
		eventDispatcher.dispatchEvent(new ParameterEvent(this, ParameterEvent.PARAMETER_CHANGED, oldJson, toJson()));
	}
	
	private Consumer<T> onChange = null;
	public final void setOnChange(Consumer<T> onChange) {
		this.onChange = onChange;
	}

	public final void addEventHandler(EventType<ParameterEvent> eventType, EventHandler<ParameterEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public final void removeEventHandler(EventType<ParameterEvent> eventType, EventHandler<ParameterEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}
	
	public abstract ParameterUiElement<T> getUi();
	
	public abstract JsonNode toJson();
	public abstract void fromJson(JsonNode node);
}
