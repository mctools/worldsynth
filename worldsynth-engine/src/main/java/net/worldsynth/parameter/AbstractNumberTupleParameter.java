/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import net.worldsynth.standalone.ui.parameters.NumberTupleParameterField;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.Tuple;

public abstract class AbstractNumberTupleParameter<T extends Number> extends AbstractParameter<Tuple<T>> {
	
	protected T min;
	protected T max;
	
	@SuppressWarnings("unchecked")
	public AbstractNumberTupleParameter(String name, String displayName, String description, T min, T max, T ... defaultValues) {
		super(name, displayName, description, new Tuple<T>(defaultValues));
		this.min = min;
		this.max = max;
	}
	
	@Override
	public ParameterUiElement<Tuple<T>> getUi() {
		return new NumberTupleParameterField<T>(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode node = objectMapper.createArrayNode();
		
		for (int i = 0; i < getValue().getTupleSize(); i++) {
			JsonNode valueNode = objectMapper.convertValue(getValue().getValue(i), JsonNode.class);
			node.add(valueNode);
		}
		
		return node;
	}

	@Override
	public void fromJson(JsonNode node) {
		for (int i = 0; i < getValue().getTupleSize(); i++) {
			if (node.has(i)) {
				getValue().setValue(i, parseValue(node.get(i)));
			}
		}
	}
	
	public abstract T parseValue(JsonNode valueNode);
	
	public abstract T parseValue(String valueString) throws NumberFormatException;
	
	public T getMin() {
		return min;
	}
	
	public T getMax() {
		return max;
	}
	
	public abstract boolean insideBounds(T value);
}
