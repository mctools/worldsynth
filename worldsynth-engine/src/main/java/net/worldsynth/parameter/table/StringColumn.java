/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.control.Control;
import javafx.scene.control.TextField;

public abstract class StringColumn<P> extends TableColumn<P, String> {

	public StringColumn(String name, String displayName, String description) {
		super(name, displayName, description);
	}

	@Override
	public Control getNewUiControl(String value, OnChange notifier) {
		TextField textField = new TextField(value);
		textField.textProperty().addListener((observable, oldValue, newValue) -> {
			notifier.call();
		});
		return textField;
	}
	
	@Override
	public String getColumnValueFromUiControl(Control control) {
		TextField textField = (TextField) control;
		return textField.getText();
	}
	
	@Override
	public JsonNode valueToJson(String value) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(value, JsonNode.class);
	}
	
	@Override
	public String valueFromJson(JsonNode node) {
		return node.asText();
	}
}
