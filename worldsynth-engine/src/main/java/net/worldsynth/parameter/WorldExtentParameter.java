/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.event.EventHandler;
import net.worldsynth.util.event.ExtentEvent;
import net.worldsynth.util.event.ParameterEvent;

public class WorldExtentParameter extends AbstractParameter<WorldExtent> {

	private final AbstractModule parentModule;

	private EventHandler<ExtentEvent> extentHandler;
	
	public WorldExtentParameter(String name, String displayName, String description, AbstractModule parentModule) {
		super(name, displayName, description, null);
		this.parentModule = parentModule;
		
		extentHandler = e -> {
			JsonNode oldJson = toJson();

			if (e.getEventType() == ExtentEvent.EXTENT_REMOVED) {
				setValue(null);
			}

			eventDispatcher.dispatchEvent(new ParameterEvent(this, ParameterEvent.PARAMETER_CHANGED, oldJson, toJson()));
		};
	}
	
	private WorldExtentManager getExtentManager() {
		return parentModule.getExtentManager();
	}

	@Override
	public void setValue(WorldExtent value) {
		if (getValue() != null) {
			getValue().removeEventHandler(ExtentEvent.EXTENT_ANY, extentHandler);
		}

		super.setValue(value);

		if (value != null) {
			value.addEventHandler(ExtentEvent.EXTENT_ANY, extentHandler);
		}
	}

	@Override
	public ParameterUiElement<WorldExtent> getUi() {
		return new ExtentParameterDropdownSelector(this, getExtentManager());
	}

	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();

		if (getValue() != null) {
			return objectMapper.convertValue(getValue().getName() + "#" + getValue().getId(), JsonNode.class);
		} else {
			return objectMapper.convertValue(null, JsonNode.class);
		}
	}

	@Override
	public void fromJson(JsonNode node) {
		if (!node.isNull()) {
			String extentString = node.asText();

			if (getExtentManager() != null && extentString != null) {
				String s = extentString.substring(extentString.indexOf("#") + 1);
				long id = Long.parseLong(s);
				setValue(getExtentManager().getWorldExtentById(id));
			}
		}
	}
}
