/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class FloatParameter extends AbstractParameter<Float> {
	
	protected float min;
	protected float max;
	
	protected float sliderMin;
	protected float sliderMax;
	protected float displayMultiplier;
	
	public FloatParameter(String name, String displayName, String description, float defaultValue, float min, float max) {
		this(name, displayName, description, defaultValue, min, max, min, max, 1.0f);
	}
	
	public FloatParameter(String name, String displayName, String description, float defaultValue, float min, float max, float displayMultiplier) {
		this(name, displayName, description, defaultValue, min, max, min, max, displayMultiplier);
	}
	
	public FloatParameter(String name, String displayName, String description, float defaultValue, float min, float max, float sliderMin, float sliderMax) {
		this(name, displayName, description, defaultValue, min, max, sliderMin, sliderMax, 1.0f);
	}
	
	public FloatParameter(String name, String displayName, String description, float defaultValue, float min, float max, float sliderMin, float sliderMax, float displayMultiplier) {
		super(name, displayName, description, defaultValue);
		this.min = min;
		this.max = max;
		this.sliderMin = sliderMin;
		this.sliderMax = sliderMax;
		this.displayMultiplier = displayMultiplier;
	}
	
	@Override
	public ParameterUiElement<Float> getUi() {
		return new FloatParameterSlider(this);
	}
	
	@Override
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node) {
		setValue((float) node.asDouble());
	}
	
	public float getMin() {
		return min;
	}
	
	public float getMax() {
		return max;
	}
	
	public float getSliderMin() {
		return sliderMin;
	}
	
	public float getSliderMax() {
		return sliderMax;
	}
	
	public float getDisplayMultiplier() {
		return displayMultiplier;
	}
}
