/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.module.AbstractModuleRegister;

public class AddonLoader implements IAddon {
	private static final Logger logger = LogManager.getLogger(AddonLoader.class);
	
	private final ArrayList<AddonJarLoader> jarLoaders = new ArrayList<AddonJarLoader>();
	private final ArrayList<IAddon> addons = new ArrayList<IAddon>();
	
	public AddonLoader(File addonDirectory, IAddon... injectedAddons) {
		if (injectedAddons != null) {
			this.addons.addAll(Arrays.asList(injectedAddons));
		}
		loadAddonsFromDirectory(addonDirectory);
		try {
			addons.addAll(getInstancesAssignableFrom(IAddon.class));
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addons", e);
		}
	}
	
	private void loadAddonsFromDirectory(File addonDirectory) {
		for (File sub: addonDirectory.listFiles()) {
			if (sub.isDirectory()) {
				loadAddonsFromDirectory(sub);
			}
			else if (sub.getAbsolutePath().endsWith(".jar")) {
				try {
					loadAddon(sub);
				} catch (ClassNotFoundException | IOException e) {
					logger.error("Problem loading addon: " + sub.getName(), e);
				}
			}
		}
	}
	
	private void loadAddon(File jar) throws ClassNotFoundException, IOException {
		AddonJarLoader jarLoader = new AddonJarLoader(jar);
		jarLoaders.add(jarLoader);
	}
	
	private <T> List<T> getInstancesAssignableFrom(Class<T> classType) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<T> instances = new ArrayList<T>();
		for (AddonJarLoader jarLoader: jarLoaders) {
			instances.addAll(jarLoader.getInstancesAssignableFrom(classType));
		}
		return instances;
	}
	
	public void initAddons(WorldSynthDirectoryConfig directoryConfig) {
		ArrayList<IAddon> addonLoaders = new ArrayList<IAddon>();
		for (IAddon addon: addons) {
			addonLoaders.add(addon);
		}
		
		for (IAddon addon: addonLoaders) {
			addon.initAddon(directoryConfig);
		}
	}
	
	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {}
	
	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<AbstractModuleRegister>();
		for (IAddon addon: addons) {
			moduleRegisters.addAll(addon.getAddonModuleRegisters());
		}
		return moduleRegisters;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<MaterialProfile> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile> materialProfiles = new ArrayList<MaterialProfile>();
		for (IAddon addon: addons) {
			materialProfiles.addAll(addon.getAddonMaterialProfiles());
		}
		return materialProfiles;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<BiomeProfile> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile> biomeProfiles = new ArrayList<BiomeProfile>();
		for (IAddon addon: addons) {
			biomeProfiles.addAll(addon.getAddonBiomeProfiles());
		}
		return biomeProfiles;
	}
	
	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<CustomObjectFormat>();
		for (IAddon addon: addons) {
			objectFormats.addAll(addon.getAddonCustomObjectFormats());
		}
		return objectFormats;
	}
}
