/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

public class Property {
	private String name;
	private String value;

	public Property(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof Property) {
			Property anotherProperty = (Property) obj;
			return anotherProperty.name.equals(name) && anotherProperty.value.equals(value);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode() ^ value.hashCode();
	}

	@Override
	public String toString() {
		return name + "=" + value;
	}
}
