/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleValuespaceSmoothMinMax extends AbstractModule {
	
	private DoubleParameter factor = new DoubleParameter("factor", "Factor",
			"Negative values give smooth min, positive values give smooth max.",
			1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				factor
		};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int mpw = requestData.spacePointsWidth;
		int mph = requestData.spacePointsHeight;
		int mpl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		double factor = this.factor.getValue();
		
		//Check if both inputs are available
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		//Read in the primary and secondary valuespace
		float[][][] primarySpace = ((DatatypeValuespace) inputs.get("primary")).getValuespace();
		float[][][] secondarySpace = ((DatatypeValuespace) inputs.get("secondary")).getValuespace();
		
		
		//Read mask
		float[][][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeValuespace) inputs.get("mask")).getValuespace();
		}
				
		//----------BUILD----------//
		
		float[][][] space = new float[mpw][mph][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mph; v++) {
				for (int w = 0; w < mpl; w++) {
					double i0 = primarySpace[u][v][w];
					double i1 = secondarySpace[u][v][w];
					
					//Smooth max
					double o = (i0 * Math.exp(factor * i0) + i1 * Math.exp(factor * i1)) / (Math.exp(factor * i0) + Math.exp(factor * i1)) ;
					
					space[u][v][w] = (float) o;
				}
			}
		}
		
		//Apply mask
		if (mask != null) {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mph; v++) {
					for (int w = 0; w < mpl; w++) {
						space[u][v][w] = space[u][v][w] * mask[u][v][w] + primarySpace[u][v][w] * (1-mask[u][v][w]);
					}
				}
			}
		}
		
		requestData.setValuespace(space);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Smooth min/max";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Primary"),
				new ModuleInput(new DatatypeValuespace(), "Secondary"),
				new ModuleInput(new DatatypeValuespace(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

}
