/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.biomemap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BiomeParameter;

public class ModuleBiomemapConstant extends AbstractModule {
	
	private BiomeParameter biome = new BiomeParameter("constant", "Biome", null, BiomeRegistry.getDefaultBiome());
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				biome
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBiomemap requestData = (DatatypeBiomemap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		Biome[][] biomemap = new Biome[mpw][mpl];
		
		Biome b = biome.getValue();
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				biomemap[u][v] = b;
			}
		}
		
		requestData.setBiomemap(biomemap);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Constant biome";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeBiomemap(), "Output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
