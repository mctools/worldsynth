/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.biomemap;

import java.util.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.util.converter.FloatStringConverter;
import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleBiomemapParameterVoronoi extends AbstractModule {

	private BiomeEntriesParameter biomelist = new BiomeEntriesParameter("biomelist", "Biomes", null,
			Arrays.asList(new BiomeEntry(BiomeRegistry.getDefaultBiome(), 75.0f, 75.0f)));

 	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				biomelist
		};
		return p;
	}
 	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBiomemap requestData = (DatatypeBiomemap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		Biome[][] biomemap = new Biome[mpw][mpl];
		
		if (!inputs.containsKey("heatmap") || !inputs.containsKey("precipitationmap")) {
			//If the main or secondary input is not available, there is not enough input and then just return null
			return null;
		}
		else if (biomelist.getValue().size() == 0) {
			return null;
		}
		
		float[][] heatmap = ((DatatypeHeightmap) inputs.get("heatmap")).getHeightmap();
		float[][] precipitationmap = ((DatatypeHeightmap) inputs.get("precipitationmap")).getHeightmap();
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				biomemap[u][v] = voronoiBiome(heatmap[u][v], precipitationmap[u][v], biomelist.getValue());
			}
		}
		
		requestData.setBiomemap(biomemap);
		
		return requestData;
	}
	
	private Biome voronoiBiome(double heat, double presipitation, List<BiomeEntry> biomelist) {
		return voronoiBiomeEntry(heat, presipitation, biomelist).getBiome();
	}
	
	private BiomeEntry voronoiBiomeEntry(double heat, double presipitation, List<BiomeEntry> biomelist) {
		double closestDist = -1;
		BiomeEntry closestBiomeEntry = biomelist.get(0);

		for (BiomeEntry biomeEntry: biomelist) {
			if (biomeEntry.getBiome() == Biome.NULL) {
				continue;
			}

			//Difference from biome ideal
			double d = Math.sqrt(Math.pow(heat - biomeEntry.getHeat(), 2) + Math.pow(presipitation - biomeEntry.getPrecipitation(), 2));

			if (closestDist > d || closestDist < 0) {
				closestBiomeEntry = biomeEntry;
				closestDist = d;
			}
		}
		
		return closestBiomeEntry;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBiomemap bmap = (DatatypeBiomemap) outputRequest.data;
		
		DatatypeHeightmap hmap = new DatatypeHeightmap(bmap.extent, bmap.resolution);
		inputRequests.put("heatmap", new ModuleInputRequest(getInput(0), hmap));
		inputRequests.put("precipitationmap", new ModuleInputRequest(getInput(1), hmap));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Parameter voronoi biome";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Heat"),
				new ModuleInput(new DatatypeHeightmap(), "Downfall")
		};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeBiomemap(), "Output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private Biome[] getSelectableBiomes() {
		ArrayList<Biome> alphabeticalCollection = BiomeRegistry.getBiomesAlphabetically();
		Biome[] selectables = new Biome[alphabeticalCollection.size()+1];
		selectables[0] = Biome.NULL;
		for (int i = 0; i < alphabeticalCollection.size(); i++) {
			selectables[i+1] = alphabeticalCollection.get(i);
		}
		return selectables;
	}

	public static class BiomeEntry {
		private final SimpleObjectProperty<Biome> biome;
		private final SimpleFloatProperty heat;
		private final SimpleFloatProperty precipitation;

		public BiomeEntry(Biome biome, float heat, float precipitation) {
			this.biome = new SimpleObjectProperty<Biome>(biome);
			this.heat = new SimpleFloatProperty(heat);
			this.precipitation = new SimpleFloatProperty(precipitation);
		}

		public final Biome getBiome() {
			return biome.get();
		}

		public final void setBiome(Biome biome) {
			this.biome.set(biome);
		}

		public final float getHeat() {
			return heat.get();
		}

		public final void setHeat(float heat) {
			this.heat.set(heat);
		}

		public final float getPrecipitation() {
			return precipitation.get();
		}

		public final void setPrecipitation(float precipitation) {
			this.precipitation.set(precipitation);
		}

		@Override
		protected BiomeEntry clone() {
			return new BiomeEntry(getBiome(), getHeat(), getPrecipitation());
		}
	}

	private class BiomeEntriesParameter extends AbstractParameter<List<BiomeEntry>> {

		public BiomeEntriesParameter(String name, String displayName, String description, List<BiomeEntry> defaultValue) {
			super(name, displayName, description, defaultValue);
		}

		@Override
		public ParameterUiElement<List<BiomeEntry>> getUi() {
			return new BiomeEntriesParameterUi(this);
		}

		@Override
		public JsonNode toJson() {
			ObjectMapper objectMapper = new ObjectMapper();
			ArrayNode biomesListNode = objectMapper.createArrayNode();

			for (BiomeEntry b: getValue()) {
				ObjectNode biomeNode = objectMapper.createObjectNode();
				if (b.getBiome() == Biome.NULL) {
					continue;
				}
				biomeNode.put("biome", b.getBiome().getIdName());
				biomeNode.put("heat", b.getHeat());
				biomeNode.put("precipitation", b.getPrecipitation());

				biomesListNode.add(biomeNode);
			}

			return biomesListNode;
		}

		@Override
		public void fromJson(JsonNode node) {
			ArrayList<BiomeEntry> recoveredBiomes = new ArrayList<>();

			for(JsonNode n: node) {
				Biome biome = BiomeRegistry.getBiome(n.get("biome").asText());
				float heat = (float) n.get("heat").asDouble();
				float precipitation = 0;
				if (n.has("precepitation")) {
					// Read precipitation value with spelling error in old keys
					precipitation = (float) n.get("precepitation").asDouble();
				}
				else {
					// Read precipitation normally
					precipitation = (float) n.get("precipitation").asDouble();
				}
				BiomeEntry b = new BiomeEntry(biome, heat, precipitation);
				recoveredBiomes.add(b);
			}

			setValue(recoveredBiomes);
		}
	}

	private class BiomeEntriesParameterUi extends ParameterUiElement<List<BiomeEntry>> {
		private final GridPane pane = new GridPane();
		private final TableView<BiomeEntry> biomeTable = new TableView<BiomeEntry>();
		private final VoronoiPane voronoiPane = new VoronoiPane(biomeTable, 400, 400, this);

		public BiomeEntriesParameterUi(BiomeEntriesParameter parameter) {
			super(parameter);

			pane.setHgap(10.0);
			List<BiomeEntry> uiBiomelist = new ArrayList<>();
			for (BiomeEntry b: biomelist.getValue()) {
				uiBiomelist.add(b.clone());
			}

			////////// Biometable //////////
			biomeTable.setPrefHeight(400);
			biomeTable.setEditable(true);
			TableColumn<BiomeEntry, Biome> biomeColumn = new TableColumn<BiomeEntry, Biome>("Biome");
			TableColumn<BiomeEntry, Float> heatColumn = new TableColumn<BiomeEntry, Float>("Heat");
			TableColumn<BiomeEntry, Float> precepitationColumn = new TableColumn<BiomeEntry, Float>("Downfall");
			biomeColumn.setResizable(false);
			biomeColumn.prefWidthProperty().bind(biomeTable.widthProperty().multiply(0.5));
			heatColumn.setResizable(false);
			heatColumn.prefWidthProperty().bind(biomeTable.widthProperty().multiply(0.25));
			precepitationColumn.setResizable(false);
			precepitationColumn.prefWidthProperty().bind(biomeTable.widthProperty().multiply(0.25));
			biomeTable.getColumns().addAll(biomeColumn, heatColumn, precepitationColumn);

			biomeColumn.setCellValueFactory(new PropertyValueFactory<BiomeEntry, Biome>("biome"));
			biomeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(getSelectableBiomes()));
			biomeColumn.setOnEditCommit(t -> {
				t.getRowValue().setBiome(t.getNewValue());
				if (biomeTable.getItems().get(biomeTable.getItems().size()-1).getBiome() != Biome.NULL) {
					biomeTable.getItems().add(new BiomeEntry(Biome.NULL, 0.0f, 0.0f));
				}
				updateUiValue(biomeTable);
				voronoiPane.paint();
			});

			heatColumn.setCellValueFactory(new PropertyValueFactory<BiomeEntry, Float>("heat"));
			heatColumn.setCellFactory(TextFieldTableCell.forTableColumn(new ImprovedFloatStringConverter()));
			heatColumn.setOnEditCommit(t -> {
				t.getRowValue().setHeat(t.getNewValue());
				updateUiValue(biomeTable);
				voronoiPane.paint();
			});

			precepitationColumn.setCellValueFactory(new PropertyValueFactory<BiomeEntry, Float>("precipitation"));
			precepitationColumn.setCellFactory(TextFieldTableCell.forTableColumn(new ImprovedFloatStringConverter()));
			precepitationColumn.setOnEditCommit(t -> {
				t.getRowValue().setPrecipitation(t.getNewValue());
				updateUiValue(biomeTable);
				voronoiPane.paint();
			});

			biomeTable.getItems().addAll(uiBiomelist);
			biomeTable.getItems().add(new BiomeEntry(Biome.NULL, 0, 0));

			pane.add(biomeTable, 0, 0);
			pane.add(voronoiPane, 1, 0);

			updateUiValue(biomeTable);
			voronoiPane.paint();
		}

		public void updateUiValue(TableView<BiomeEntry> biomeTable) {
			List<BiomeEntry> entries = new ArrayList<>();
			for (BiomeEntry entry: biomeTable.getItems()) {
				if (entry.getBiome() != Biome.NULL) {
					entries.add(entry.clone());
				}
			}
			uiValue = entries;
		}

		@Override
		public void setDisable(boolean disable) {

		}

		@Override
		public void addToGrid(GridPane pane, int row) {
			pane.add(this.pane, 1, row);
		}
	}

	private class ImprovedFloatStringConverter extends FloatStringConverter {
		@Override
		public Float fromString(String value) {
			// If the specified value is null or zero-length, return null
			if (value == null) {
				return null;
			}

			value = value.trim();

			if (value.length() < 1) {
				return null;
			}

			return Float.valueOf(value.replace(",", "."));
		}
	}

	private class VoronoiPane extends Canvas {

		private TableView<BiomeEntry> biometable;

		private BiomeEntry draggedEntry = null;

		public VoronoiPane(TableView<BiomeEntry> biometable, double width, double height, BiomeEntriesParameterUi parameterUi) {
			super(width, height);
			this.biometable = biometable;

			setOnMouseDragged(e -> {
				if (draggedEntry != null && e.getButton() == MouseButton.PRIMARY) {
					float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
					double downfallValue = e.getX()/getWidth();
					downfallValue = MathHelperScalar.clamp(downfallValue, 0.0, 1.0)*normalizedHeight;
					double heatValue = 1.0-e.getY()/getHeight();
					heatValue = MathHelperScalar.clamp(heatValue, 0.0, 1.0)*normalizedHeight;
					draggedEntry.setHeat((float) heatValue);
					draggedEntry.setPrecipitation((float) downfallValue);
					biometable.refresh();
					biometable.getSelectionModel().select(draggedEntry);
					paint();
					parameterUi.updateUiValue(biometable);
				}
			});

			setOnMousePressed(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
					double downfallValue = e.getX()/getWidth();
					downfallValue = MathHelperScalar.clamp(downfallValue, 0.0, 1.0)*normalizedHeight;
					double heatValue = 1.0-e.getY()/getHeight();
					heatValue = MathHelperScalar.clamp(heatValue, 0.0, 1.0)*normalizedHeight;
					draggedEntry = voronoiBiomeEntry(heatValue, downfallValue, biometable.getItems());
					biometable.getSelectionModel().select(draggedEntry);
				}
			});

			setOnMouseReleased(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					draggedEntry = null;
				}
			});
		}

		public void paint() {
			float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();

			GraphicsContext g = getGraphicsContext2D();

			List<BiomeEntry> blist = biometable.getItems();

			for (int x = 0; x < getWidth(); x++) {
				for (int y = 0; y < getHeight(); y++) {
					g.setFill(voronoiBiome((1.0f-(float)y/(float)getWidth())*normalizedHeight, (float)x/(float)getHeight() * normalizedHeight, blist).getFxColor());
					g.fillRect(x, y, 1, 1);
				}
			}

			for (BiomeEntry b: biometable.getItems()) {
				if (b.getBiome() == Biome.NULL) {
					continue;
				}

				g.setFill(b.getBiome().getFxColor().invert());
				g.fillOval((int)(b.getPrecipitation()/normalizedHeight*(float)getWidth())-3, (int)((1.0f-b.getHeat()/normalizedHeight)*(float)getHeight())-3, 7, 7);
			}
		}
	}
}
