/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleHeightmapExpander extends AbstractModule {
	
	private IntegerParameter radius = new IntegerParameter("kernelradius", "Kernel size", null, 1, 0, Integer.MAX_VALUE, 0, 20);
	private EnumParameter<ExpandState> expandState = new EnumParameter<ExpandState>("expandstate", "State", null, ExpandState.class, ExpandState.HIGHEST);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				radius,
				expandState
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		ExpandState expandState = this.expandState.getValue();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in radius
		int radiusValue = this.radius.getValue();
		float[][] radiusMap = null;
		if (inputs.get("radius") != null) {
			radiusMap = ((DatatypeHeightmap) inputs.get("radius")).getHeightmap();
		}
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		//Has radius map
		if (radiusMap != null) {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					outputMap[u][v] = expand(radiusValue, expandState, radiusMap[u][v]/normalizedHeight, inputMap, res, u, v);
				}
			}
		}
		else {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					outputMap[u][v] = expand(radiusValue, expandState, 1.0, inputMap, res, u, v);
				}
			}
		}
		
		//Apply mask
		if (mask != null) {
			int activeRadius = (int) Math.floor((double) radiusValue/res);
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					outputMap[u][v] = (outputMap[u][v] - inputMap[u+activeRadius][v+activeRadius]) * mask[u][v]/normalizedHeight + inputMap[u+activeRadius][v+activeRadius];
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float expand(int radius, ExpandState expandState, double radiusModifier, float[][] heightmap, double res, int x, int y) {
		boolean firstSelected = false;
		float selectedValue = 0;
		
		int appliedRadius = (int) Math.floor((double) radius*radiusModifier/res);
		radius = (int) Math.floor((double) radius/res);
		
		for (int kx = -appliedRadius; kx <= appliedRadius; kx++) {
			for (int ky = -appliedRadius; ky <= appliedRadius; ky++) {
				
				float a = heightmap[x+kx+radius][y+ky+radius];
				
				if (!firstSelected) {
					selectedValue = a;
					firstSelected = true;
				}
				else if (expandState == ExpandState.HIGHEST && a > selectedValue) {
					selectedValue = a;
				}
				else if (expandState == ExpandState.LOWEST && a < selectedValue) {
					selectedValue = a;
				}
			}
		}
		
		return selectedValue;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap) outputRequest.data;
		double expandRadius = Math.floor((double) radius.getValue()/d.resolution)*d.resolution;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(BuildExtent.expandedBuildExtent(d.extent, expandRadius, 0, expandRadius), d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		inputRequests.put("radius", new ModuleInputRequest(getInput("Radius"), (DatatypeHeightmap) outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap) outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Expander";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Radius"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum ExpandState {
		HIGHEST, LOWEST;
	}
}
