/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapRamp extends AbstractModule {
	
	private EnumParameter<RampTiling> rampType = new EnumParameter<RampTiling>("tiling", "Ramp type", null, RampTiling.class, RampTiling.STANDARD);
	private FloatParameter period = new FloatParameter("period", "Ramp period", null, 128.0f, Float.MIN_VALUE, Float.POSITIVE_INFINITY, 1.0f, 256.0f);
	private BooleanParameter normalize = new BooleanParameter("normalize", "Normalize height", null, false);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				rampType,
				period,
				normalize
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		RampTiling rampType = this.rampType.getValue();
		boolean normalize = this.normalize.getValue();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in frequency
		float period = this.period.getValue();
		if (inputs.get("frequency") != null) {
			period = (float) ((DatatypeScalar) inputs.get("frequency")).getValue();
		}
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				outputMap[u][v] = ramp(inputMap[u][v], period, rampType, normalize, normalizedHeight);
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(outputMap, inputMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float ramp(float height, float period, RampTiling method, boolean normalizeScale, float normalizedHeight) {
		double h = height/period;
		double i = Math.floor(h);
		h = height-i*period;
		if (method == RampTiling.CONTINOUS && i%2 != 0) {
			h = period-h;
		}
		if (normalizeScale) {
			h *= normalizedHeight/period;
		}
		
		return (float) h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("frequency", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Ramp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "Frequency"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum RampTiling {
		STANDARD, CONTINOUS;
	}
}
