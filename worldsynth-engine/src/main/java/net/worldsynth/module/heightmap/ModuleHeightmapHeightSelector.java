/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapHeightSelector extends AbstractModule {
	
	private FloatParameter lowSelect = new FloatParameter("lowselect", "Low select", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, -256.0f, 256.0f);
	private FloatParameter highSelect = new FloatParameter("highselect", "High select", null, 1.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, -256.0f, 256.0f);
	private FloatParameter falloff = new FloatParameter("falloff", "Selection falloff", "Selection falloff", 0.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 256.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highSelect,
				lowSelect,
				falloff
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in high select
		float highSelect = this.highSelect.getValue();
		if (inputs.get("high") != null) {
			highSelect = (float) ((DatatypeScalar) inputs.get("high")).getValue();
		}
		
		//Read in low select
		float lowSelect = this.lowSelect.getValue();
		if (inputs.get("low") != null) {
			lowSelect = (float) ((DatatypeScalar) inputs.get("low")).getValue();
		}
		
		//Read in falloff
		float falloff = this.falloff.getValue();
		if (inputs.get("falloff") != null) {
			falloff = (float) ((DatatypeScalar) inputs.get("falloff")).getValue();
		}
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float i0 = inputMap[u][v];
				outputMap[u][v] = select(i0, lowSelect, highSelect, falloff) * normalizedHeight;
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(outputMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float select(double slope, double lowSelect , double highSelect, double falloff) {
		if (falloff == 0.0) {
			if (slope <= highSelect && slope >= lowSelect) return 1.0f;
			else return 0.0f;
		}
		
		if (slope < lowSelect) {
			return (float) Math.max(0.0f, (slope - lowSelect + falloff) / falloff);
		}
		else if (slope > highSelect) {
			return (float) Math.max(0.0f, (falloff - slope + highSelect) / falloff);
		}
		return 1.0f;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		inputRequests.put("high", new ModuleInputRequest(getInput("High"), new DatatypeScalar()));
		inputRequests.put("low", new ModuleInputRequest(getInput("Low"), new DatatypeScalar()));
		inputRequests.put("falloff", new ModuleInputRequest(getInput("Falloff"), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Height selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.SELECTOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "High"),
				new ModuleInput(new DatatypeScalar(), "Low"),
				new ModuleInput(new DatatypeScalar(), "Falloff"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
