/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.parameter.WorldExtentParameter;

public class ModuleHeightmapScatter extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private WorldExtentParameter featureExtent = new WorldExtentParameter("extent", "Feature extent", null, this);
	private BooleanParameter enableScaling = new BooleanParameter("scale", "Scale", null, true);
	private BooleanParameter enableRotation = new BooleanParameter("rotate", "Rotate", null, true);
	private DoubleParameter smallestScale = new DoubleParameter("smallestscale", "Smallest scale", null, 0.5, 0.0, Double.MAX_VALUE, 0.0, 2.0);
	private DoubleParameter largestScale = new DoubleParameter("largestscale", "Largest scale", null, 1.0, 0.0, Double.MAX_VALUE, 0.0, 2.0);
	private EnumParameter<BlendOperation> blendOperation = new EnumParameter<BlendOperation>("blend", "Blend", null, BlendOperation.class, BlendOperation.ADDITION);
	private EnumParameter<ScatterBlendOperation> scatterBlendOperation = new EnumParameter<ScatterBlendOperation>("scatterblend", "Scatter blend", null, ScatterBlendOperation.class, ScatterBlendOperation.ADDITION);
	private EnumParameter<SampleMethod> sampleMethod = new EnumParameter<SampleMethod>("samplemethod", "Sample method", null, SampleMethod.class, SampleMethod.BILINEAR);
	
	private final int permutationSize = 256;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[][] dp;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			dp = createPermutatationTable(permutationSize, 2, newValue);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				featureExtent,
				enableRotation,
				enableScaling,
				smallestScale,
				largestScale,
				blendOperation,
				scatterBlendOperation,
				sampleMethod,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		
		WorldExtent featureExtent = this.featureExtent.getValue();
		DatatypeHeightmap featureHeightmapRequestData = new DatatypeHeightmap(new BuildExtent(featureExtent), ord.resolution); // Feature
		
		// Placement data
		double expand = Math.max(featureExtent.getWidth(), featureExtent.getLength());
		BuildExtent expandedExtent = BuildExtent.expandedBuildExtent(ord.extent, expand, 0.0, expand);
		DatatypeFeaturemap scatterFeaturmapRequestData = new DatatypeFeaturemap(expandedExtent, ord.resolution); // Placement
		DatatypeVectormap contextRequestData = new DatatypeVectormap(expandedExtent, ord.resolution); // Scale and rotation context
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), ord));
		inputRequests.put("feature", new ModuleInputRequest(getInput(1), featureHeightmapRequestData));
		inputRequests.put("placement", new ModuleInputRequest(getInput(2), scatterFeaturmapRequestData));
		inputRequests.put("context", new ModuleInputRequest(getInput(3), contextRequestData));
		inputRequests.put("mask", new ModuleInputRequest(getInput(4), ord));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		boolean enableRotation = this.enableRotation.getValue();
		boolean enableScaling = this.enableScaling.getValue();
		double smallestScale = this.smallestScale.getValue();
		double largestScale = this.largestScale.getValue();
		BlendOperation blendOperation = this.blendOperation.getValue();
		ScatterBlendOperation scatterBlendOperation = this.scatterBlendOperation.getValue();
		SampleMethod sampleMethod = this.sampleMethod.getValue();
		
		DatatypeHeightmap heightmap = (DatatypeHeightmap) inputs.get("input");
		DatatypeHeightmap fetaureHeightmap = (DatatypeHeightmap) inputs.get("feature");
		DatatypeFeaturemap scatterFeaturemap = (DatatypeFeaturemap) inputs.get("placement");
		DatatypeVectormap contextMap = (DatatypeVectormap) inputs.get("context");
		DatatypeHeightmap maskHeightmap = (DatatypeHeightmap) inputs.get("mask");
		
		if (heightmap == null || fetaureHeightmap == null || scatterFeaturemap == null) {
			return null;
		}
		
		//----------BUILD----------//
		
		//Make a clean scattermap for the output
		float[][] outputHeightmap = new float[mpw][mpl];
		requestData.setHeightmap(outputHeightmap);
		
		for (Featurepoint2D point: scatterFeaturemap.getFeaturepoints()) {
			double xi = point.getX();
			double zi = point.getZ();
			
			double rotate = 0.0;
			double scale = 1.0;
			if (enableScaling) {
				if (contextMap != null) {
					float[] v = contextMap.getGlobalVector(xi, zi);
					scale = smallestScale + Math.sqrt(v[0] * v[0] + v[1] * v[1]) * (largestScale - smallestScale);
				}
				else {
					scale = smallestScale + hash((int) xi, (int) zi, 0) * (largestScale - smallestScale);
				}
			}
			if (enableRotation) {
				if (contextMap != null) {
					float[] v = contextMap.getGlobalVector(xi, zi);
					rotate = Math.atan2(v[1], v[0]);
				}
				else {
					rotate = hash((int) xi, (int) zi, 1)*Math.PI*2.0;
				}
			}
			placeHeightmap(requestData, fetaureHeightmap, xi, zi, scale, rotate, scatterBlendOperation, sampleMethod, normalizedHeight);
		}
		
		// Merge scatterHeightmap and input heightmap
		if (maskHeightmap == null) {
			// Merge without mask
			for (int u = 0; u < heightmap.mapPointsWidth; u++) {
				for (int v = 0; v < heightmap.mapPointsLength; v++) {
					outputHeightmap[u][v] = mix(heightmap.getLocalHeight(u, v), outputHeightmap[u][v], blendOperation, 1.0f, normalizedHeight);
				}
			}
		}
		else {
			// Mewrge with mask
			for (int u = 0; u < heightmap.mapPointsWidth; u++) {
				for (int v = 0; v < heightmap.mapPointsLength; v++) {
					outputHeightmap[u][v] = mix(heightmap.getLocalHeight(u, v), outputHeightmap[u][v], blendOperation, maskHeightmap.getLocalHeight(u, v)/normalizedHeight, normalizedHeight);
				}
			}
		}
		
		return requestData;
	}
	
	private void placeHeightmap(DatatypeHeightmap scatterHeightmap, DatatypeHeightmap featureHeightmap, double x, double z, double scale, double rotation, ScatterBlendOperation scatterBlendOperation, SampleMethod sampleMethod, float normalizedHeight) {
		x = (x - scatterHeightmap.extent.getX()) / scatterHeightmap.resolution;
		z = (z - scatterHeightmap.extent.getZ()) / scatterHeightmap.resolution;
		float[][] scatterHeightmapArray = scatterHeightmap.getHeightmap();
		
		//Create the transform matrix for mapping points in the scatter heightmap to points in the feature heightmap
		double[][] inverse_transform = {
				{Math.cos(-rotation)/scale, -Math.sin(-rotation)/scale, (-x*Math.cos(-rotation)+z*Math.sin(-rotation))/scale + (double)featureHeightmap.mapPointsWidth/2},
				{Math.sin(-rotation)/scale,  Math.cos(-rotation)/scale, (-x*Math.sin(-rotation)-z*Math.cos(-rotation))/scale + (double)featureHeightmap.mapPointsLength/2},
				{0                        , 0                         , 1          }};
		
		int[] bounds = bounds(x, z, featureHeightmap.mapPointsWidth, featureHeightmap.mapPointsLength, scale, rotation);
		
		int minX = Math.max(bounds[0], 0);
		int maxX = Math.min(bounds[1], scatterHeightmap.mapPointsWidth);
		int minZ = Math.max(bounds[2], 0);
		int maxZ = Math.min(bounds[3], scatterHeightmap.mapPointsLength);
		
		for (int u = minX; u < maxX; u++) {
			for (int v = minZ; v < maxZ; v++) {
				double[] sample = applyTransform(u, v, inverse_transform);
				if (!featureHeightmap.isLocalContained((int) sample[0], (int) sample[1])) continue;
				switch (sampleMethod) {
				case CLOSEST:
					scatterHeightmapArray[u][v] = mix(scatterHeightmap.getLocalHeight(u, v), featureHeightmap.getLocalHeight((int) sample[0], (int) sample[1]) * (float)scale, scatterBlendOperation, 1.0f, normalizedHeight);
					break;
				case BILINEAR:
					scatterHeightmapArray[u][v] = mix(scatterHeightmap.getLocalHeight(u, v), featureHeightmap.getLocalLerpHeight(sample[0], sample[1]) * (float)scale, scatterBlendOperation, 1.0f, normalizedHeight);
					break;
				}
			}
		}
	}
	
	//Returns {minX, maxX, minZ, maxZ}
	private int[] bounds(double x, double z, double w, double l, double scale, double rotate) {
		//Create the transform matrix for mapping points in the feature heightmap to points in the scatter heightmap
		double[][] transform = {
				{scale*Math.cos(rotate), -scale*Math.sin(rotate), (-w*scale*Math.cos(rotate)+l*scale*Math.sin(rotate))/2+x},
				{scale*Math.sin(rotate),  scale*Math.cos(rotate), (-w*scale*Math.sin(rotate)-l*scale*Math.cos(rotate))/2+z},
				{0                     , 0                      , 1                                                       }};
		
		double[] c0 = applyTransform(0, 0, transform);
		double[] c1 = applyTransform(w, 0, transform);
		double[] c2 = applyTransform(w, l, transform);
		double[] c3 = applyTransform(0, l, transform);
		
		int minX = (int) Math.floor(min(c0[0], c1[0], c2[0], c3[0]));
		int maxX = (int) Math.floor(max(c0[0], c1[0], c2[0], c3[0]));
		int minZ = (int) Math.floor(min(c0[1], c1[1], c2[1], c3[1]));
		int maxZ = (int) Math.floor(max(c0[1], c1[1], c2[1], c3[1]));
		
		return new int[] {minX, maxX, minZ, maxZ};
	}
	
	private double min(double...values) {
		double min = values[0];
		
		for (double val: values) {
			min = Math.min(min, val);
		}
		
		return min;
	}
	
	private double max(double...values) {
		double max = values[0];
		
		for (double val: values) {
			max = Math.max(max, val);
		}
		
		return max;
	}
	
	private double[] applyTransform(double x, double z, double[][] transform) {
		return new double[] {
			x*transform[0][0] + z*transform[0][1] + transform[0][2],
			x*transform[1][0] + z*transform[1][1] + transform[1][2]
		};
	}
	
	private float mix(float i0, float i1, BlendOperation operation, float fac, float normalizedHeight) {
		float o = 0;
		switch (operation) {
		case ADDITION:
			o = i0 + i1 * fac;
			break;
		case SUBTRACTION:
			o = i0 - i1 * fac;
			break;
		case MULTIPLICATION:
			o = i0 * i1 / normalizedHeight * fac;
			break;
		case DIVISION:
			o = i1 == 0 ? 0.0f : (i0 / i1 * normalizedHeight * fac);
			break;
		case MODULO:
			o = i0 % i1 * fac;
			break;
		case AVERAGE:
			o = (i0 + i1 * fac) / (1 + fac);
			break;
		case MAX:
			o = Math.max(i0, i1 * fac);
			break;
		case MIN:
			o = Math.min(i0, i1 * fac);
			break;
		case OWERFLOW:
			o = (i0 + i1 * fac);
			o = o % normalizedHeight;
			break;
		case UNDERFLOW:
			o = (i0 - i1 * fac);
			o = o < 0.0f ? o + normalizedHeight : o;
			break;
		}
		
		return o;
	}
	
	private float mix(float i0, float i1, ScatterBlendOperation operation, float fac, float normalizedHeight) {
		float o = 0;
		switch (operation) {
		case ADDITION:
			o = i0 + i1 * fac;
			break;
		case MAX:
			o = Math.max(i0, i1 * fac);
			break;
		case OWERFLOW:
			o = (i0 + i1 * fac);
			o = o % normalizedHeight;
			break;
		}
		
		return o;
	}
	
	private int[][] createPermutatationTable(int size, int dim, long seed) {
		int[][] dp = new int[size*2][dim];
		
		for (int i = 0; i < dim; i++) {
			//Create a random generator with supplied seed
			Random r = new Random(seed + i);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for (int j = 0; j < size; j++) {
				valueTabel.add(j);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while (valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			for (int k = 0; k < dp.length; k++) {
				dp[k][i] = permutationTable[k%permutationSize];
			}
		}
		
		return dp;
	}
	
	private double hash(int x, int z, int table) {
		if (x >= 0) x = x % permutationSize;
		else x = (permutationSize-1)+((x+1)%permutationSize);
		if (z >= 0) z = z % permutationSize;
		else z = (permutationSize-1)+((z+1)%permutationSize);
		
		return (double) dp[dp[x][table]+z][table] / (double) permutationSize;
	}

	@Override
	public String getModuleName() {
		return "Heightmap scatter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Feature"),
				new ModuleInput(new DatatypeFeaturemap(), "Placement"),
				new ModuleInput(new DatatypeVectormap(), "Context"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum BlendOperation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN, OWERFLOW, UNDERFLOW;
	}
	
	private enum ScatterBlendOperation {
		ADDITION, MAX, OWERFLOW;
	}
	
	private enum SampleMethod {
		CLOSEST, BILINEAR; // BICUBIC;
	}
}
