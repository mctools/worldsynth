/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.gen.Permutation;

public class ModuleHeightmapCellularize extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 32.0, 0.0, Double.POSITIVE_INFINITY, 1.0, 100.0);
	private EnumParameter<CellDistribution> cellDistribution = new EnumParameter<CellDistribution>("distribution", "Cell distribution", null, CellDistribution.class, CellDistribution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<DistanceFunction>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	
	private final double S60 = Math.sin(Math.toRadians(60));

	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				cellDistribution,
				distanceFunction,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();

		double scale = this.scale.getValue();
		CellDistribution cellDistribution = this.cellDistribution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap input = (DatatypeHeightmap) inputs.get("input");
		
		//Read in mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, input, cellDistribution, distanceFunction);
				
				if (mask != null) {
					float in = input.getGlobalHeight(x+u*res, z+v*res);
					map[u][v] = (map[u][v] - in) * mask[u][v]/normalizedHeight + in;
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap)outputRequest.data;
		
		double expandRadius = Math.ceil(Math.abs(scale.getValue() * 2.0) / ord.resolution) * ord.resolution;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(BuildExtent.expandedBuildExtent(ord.extent, expandRadius, 0.0, expandRadius), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, DatatypeHeightmap heightmap, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		Point2D p = closestPoint(x/scale, y/scale, cellDistribution, distanceFunction);
		return heightmap.getGlobalLerpHeight(p.x * scale, p.z * scale);
	}
	
	private Point2D closestPoint(double x, double y, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		double initx = x, inity = y;
		
		if (cellDistribution == CellDistribution.HEXAGON) {
			y *= 1.0/S60;
		}
		if (repeat > 0) {
			if (x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if (y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		//Calculate the local coordinates of the 9 closest featurepoints
		double[][] fx = new double[3][3];
		double[][] fy = new double[3][3];
		
		double minDist = Double.POSITIVE_INFINITY;
		double minDistX = 0, minDistY = 0;
		for (int ix = 0; ix < 3; ix++) {
			for (int iy = 0; iy < 3; iy++) {
				int cx = inc(xi, ix-1);
				int cy = inc(yi, iy-1);
				
				int xh = permutation.lHash(0, cx, cy);
				int yh = permutation.lHash(1, cx, cy);
				
				double xoffset = (double) xh/(double) repeat;
				double yoffset = (double) yh/(double) repeat;
				
				if (cellDistribution == CellDistribution.SQUARE) {
					xoffset = 0.0;
					yoffset = 0.0;
				}
				
				//Code for hexagon cells
				if (cellDistribution == CellDistribution.HEXAGON) {
					xoffset = 0;
					if (yi%2 == 0) {
						double c = 0.5;
						double m = 0 + Math.abs(iy-1);
						xoffset =  c * m;
					}
					else {
						double c = 0.5;
						double m = 1 - Math.abs(iy-1);
						xoffset =  c * m;
					}
					yoffset = 0;
				}
				
				fx[ix][iy] = (double) (ix-1) + xoffset;
				fy[ix][iy] = (double) (iy-1) + yoffset;
				
				double xdist = fx[ix][iy] - xf;
				double ydist = fy[ix][iy] - yf;
				if (cellDistribution == CellDistribution.HEXAGON) {
					ydist *= S60;
				}
				
				//Distance from point
				double dist = 0;
				switch (distanceFunction) {
				case EUCLIDEAN:
					dist = xdist*xdist + ydist*ydist;
					break;
				case MANHATTAN:
					dist = Math.abs(xdist) + Math.abs(ydist);
					break;
				case CHEBYSHEV:
					dist = Math.max(Math.abs(xdist), Math.abs(ydist));
					break;
				case MIN:
					dist = Math.min(Math.abs(xdist), Math.abs(ydist));
					break;
				}
				
				if (dist < minDist) {
					minDistX = xdist;
					minDistY = ydist;
					minDist = dist;
				}
			}
		}
		
		return new Point2D(initx + minDistX, inity + minDistY);
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if (num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "Cellularize";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}
	
	private enum CellDistribution {
		VORONOI, HEXAGON, SQUARE;
	}
	
	private class Point2D {
		public double x, z;
		public Point2D(double x, double z) {
			this.x = x;
			this.z = z;
		}
	}
}
