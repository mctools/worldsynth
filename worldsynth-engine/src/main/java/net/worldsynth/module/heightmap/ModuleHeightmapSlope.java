/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapSlope extends AbstractModule {
	
	private EnumParameter<Type> type = new EnumParameter<Type>("type", "Type", "ANGLE: Angle of the slope, 0 - 90 degrees\nRATE: Change in height", Type.class, Type.ANGLE);
	private FloatParameter gain = new FloatParameter("gain", "Gain", null, 1.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 5.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				type,
				gain
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		float res = (float) requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		Type type = this.type.getValue();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in gain
		double gainValue = this.gain.getValue();
		if (inputs.get("gain") != null) {
			gainValue = ((DatatypeScalar) inputs.get("gain")).getValue();
		}
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		for (int u = 0; u < outputMap.length; u++) {
			for (int v = 0; v < outputMap[u].length; v++) {
				float dx = ((inputMap[u+1][v+1] - inputMap[u][v+1]) + (inputMap[u+2][v+1] - inputMap[u+1][v+1])) / 2.0f;
				float dy = ((inputMap[u+1][v+1] - inputMap[u+1][v]) + (inputMap[u+1][v+2] - inputMap[u+1][v+1])) / 2.0f;
				double slope = Math.sqrt(dx*dx + dy*dy) / res;
				
				if (type == Type.ANGLE) {
					slope = Math.atan(slope);
					slope = slope / (0.5 * Math.PI);
					slope *= normalizedHeight;
				}
				
				outputMap[u][v] = (float) (slope * gainValue);
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(outputMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(BuildExtent.expandedBuildExtent(d.extent, d.resolution, 0.0, d.resolution), d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		inputRequests.put("gain", new ModuleInputRequest(getInput("Gain"), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Slope";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "Gain"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum Type {
		ANGLE,
		RATE;
	}
}
