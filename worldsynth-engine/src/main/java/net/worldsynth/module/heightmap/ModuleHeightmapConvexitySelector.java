/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapConvexitySelector extends AbstractModule {
	
	private DoubleParameter lowSelect = new DoubleParameter("lowselect", "Low select", "Lower selection threshold in degrees per unit", -5.0, -90.0, 90.0, -5.0, 5.0);
	private DoubleParameter highSelect = new DoubleParameter("highselect", "High select", "Higher selection threshold in degrees per unit", 5.0, -90.0, 90.0, -5.0, 5.0);
	private DoubleParameter falloff = new DoubleParameter("falloff", "Selection falloff", "Selection falloff in degrees per unit", 0.0, 0.0, 90.0, 0.0, 5.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highSelect,
				lowSelect,
				falloff
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		float res = (float) requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in high select
		double highSelect = this.highSelect.getValue();
		if (inputs.get("highselect") != null) {
			highSelect = ((DatatypeScalar) inputs.get("highselect")).getValue();
		}
		
		//Read in low select
		double lowSelect = this.lowSelect.getValue();
		if (inputs.get("lowselect") != null) {
			lowSelect = ((DatatypeScalar) inputs.get("lowselect")).getValue();
		}
		
		//Read in falloff
		double falloff = this.falloff.getValue();
		if (inputs.get("falloff") != null) {
			falloff = ((DatatypeScalar) inputs.get("falloff")).getValue();
		}
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		//Convert thresholds to radians
		lowSelect = Math.toRadians(lowSelect);
		highSelect = Math.toRadians(highSelect);
		falloff = Math.toRadians(falloff);
		
		float[][] outputMap = new float[mpw][mpl];
		
		for (int u = 0; u < outputMap.length; u++) {
			for (int v = 0; v < outputMap[u].length; v++) {
				double ddx = (((inputMap[u+1][v+1] - inputMap[u][v+1]) - (inputMap[u+2][v+1] - inputMap[u+1][v+1])));
				double ddy = (((inputMap[u+1][v+1] - inputMap[u+1][v]) - (inputMap[u+1][v+2] - inputMap[u+1][v+1])));
				double convexity = Math.atan(Math.signum(ddx+ddy) * Math.sqrt(ddx*ddx + ddy*ddy) / res) / (0.5 * Math.PI);
				
				outputMap[u][v] = select(convexity, lowSelect, highSelect, falloff) * normalizedHeight;
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(outputMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float select(double slope, double lowSelect , double highSelect, double falloff) {
		if (falloff == 0.0) {
			if (slope <= highSelect && slope >= lowSelect) return 1.0f;
			else return 0.0f;
		}
		
		if (slope < lowSelect) {
			return (float) Math.max(0.0f, (slope - lowSelect + falloff) / falloff);
		}
		else if (slope > highSelect) {
			return (float) Math.max(0.0f, (falloff - slope + highSelect) / falloff);
		}
		return 1.0f;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(BuildExtent.expandedBuildExtent(d.extent, d.resolution, 0.0, d.resolution), d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		inputRequests.put("highselect", new ModuleInputRequest(getInput("High"), new DatatypeScalar()));
		inputRequests.put("lowselect", new ModuleInputRequest(getInput("Low"), new DatatypeScalar()));
		inputRequests.put("falloff", new ModuleInputRequest(getInput("Falloff"), new DatatypeScalar()));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Convexity selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.SELECTOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "High"),
				new ModuleInput(new DatatypeScalar(), "Low"),
				new ModuleInput(new DatatypeScalar(), "Falloff"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
