/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.biomespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.biome.Biome;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomespace;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleBiomespaceSelector extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBiomespace requestData = (DatatypeBiomespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		if (!inputs.containsKey("selector") || !inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If the main or secondary input is null, there is not enough input and then just return null
			return null;
		}
		
		MaterialState<?, ?>[][][] selector = ((DatatypeBlockspace) inputs.get("selector")).getBlockspace();
		Biome[][][] primary = ((DatatypeBiomespace) inputs.get("primary")).getBiomespace();
		Biome[][][] secondary = ((DatatypeBiomespace) inputs.get("secondary")).getBiomespace();
		
		Biome[][][] biomespace = new Biome[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					Biome o = primary[u][v][w];
					if (selector[u][v][w].isAir()) {
						o = secondary[u][v][w];
					}
					biomespace[u][v][w] = o;
				}
			}
		}
		
		requestData.setBiomespace(biomespace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBiomespace ord = (DatatypeBiomespace) outputRequest.data;
		
		DatatypeBlockspace blockspaceRequestData = new DatatypeBlockspace(ord.extent, ord.resolution);
		inputRequests.put("selector", new ModuleInputRequest(getInput("Selector"), blockspaceRequestData));
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Selector"),
				new ModuleInput(new DatatypeBiomespace(), "Primary"),
				new ModuleInput(new DatatypeBiomespace(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBiomespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
