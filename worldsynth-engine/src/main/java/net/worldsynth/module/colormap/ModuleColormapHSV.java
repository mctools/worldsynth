/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.IHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleColormapHSV extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
		};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap ord = (DatatypeColormap) outputRequest.data;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		inputRequests.put("h", new ModuleInputRequest(getInput("H"), heightmapRequestData));
		inputRequests.put("s", new ModuleInputRequest(getInput("S"), heightmapRequestData));
		inputRequests.put("v", new ModuleInputRequest(getInput("V"), heightmapRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Check if all inputs are available
		if (inputs.get("h") == null || inputs.get("s") == null || inputs.get("v") == null) {
			//If any of the HSL inputs are null, there is not enough input and then just return null.
			return null;
		}
		//Read in the primary and secondary colormaps
		IHeightmap hMap = (IHeightmap) inputs.get("h");
		IHeightmap sMap = (IHeightmap) inputs.get("s");
		IHeightmap vMap = (IHeightmap) inputs.get("v");
				
		//----------BUILD----------//
		
		float[][][] map = new float[mpw][mpl][3];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float ch = MathHelperScalar.clamp(hMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
				float cs = MathHelperScalar.clamp(sMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
				float cv = MathHelperScalar.clamp(vMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
				map[u][v] = hsvToRgb(ch, cs, cv);
				
				for (int i = 0; i < 3; i++) {
					if (checkColorChannel(map[u][v][i])) {
						map[u][v] = hsvToRgb(ch, cs, cv);
						throw new IllegalArgumentException("channel " + i + " is " + map[u][v][i] + " at (" + u + ", " + v + ")");
					}
				}
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	private boolean checkColorChannel(double c) {
		return c < 0 || c > 1;
	}
	
	private float[] hsvToRgb(float h, float s, float v) {
		float c = v * s;
		h *= 6.0f;
		float x = c * (1 - Math.abs(h % 2 - 1));
		float m = v - c;
		
		float r, g, b;
		if(h < 1) {
			r = c + m;
			g = x + m;
			b = 0 + m;
		}
		else if (h < 2) {
			r = x + m;
			g = c + m;
			b = 0 + m;
		}
		else if (h < 3) {
			r = 0 + m;
			g = c + m;
			b = x + m;
		}
		else if (h < 4) {
			r = 0 + m;
			g = x + m;
			b = c + m;
		}
		else if (h < 5) {
			r = x + m;
			g = 0 + m;
			b = c + m;
		}
		else {
			r = c + m;
			g = 0 + m;
			b = x + m;
		}
		
		r = MathHelperScalar.clamp(r, 0.0f, 1.0f);
		g = MathHelperScalar.clamp(g, 0.0f, 1.0f);
		b = MathHelperScalar.clamp(b, 0.0f, 1.0f);
		
		return new float[] {r, g, b};
	}
	
	@Override
	public String getModuleName() {
		return "HSV";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "H"),
				new ModuleInput(new DatatypeHeightmap(), "S"),
				new ModuleInput(new DatatypeHeightmap(), "V")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
