/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.IHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleColormapRGB extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
		};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap ord = (DatatypeColormap) outputRequest.data;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		inputRequests.put("R", new ModuleInputRequest(getInput("R"), heightmapRequestData));
		inputRequests.put("G", new ModuleInputRequest(getInput("G"), heightmapRequestData));
		inputRequests.put("B", new ModuleInputRequest(getInput("B"), heightmapRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Check if all inputs are available
		if (inputs.get("R") == null || inputs.get("G") == null || inputs.get("B") == null) {
			//If any of the RGB inputs are null, there is not enough input and then just return null.
			return null;
		}
		//Read in the primary and secondary colormaps
		IHeightmap rMap = (IHeightmap) inputs.get("R");
		IHeightmap gMap = (IHeightmap) inputs.get("G");
		IHeightmap bMap = (IHeightmap) inputs.get("B");
				
		//----------BUILD----------//
		
		float[][][] map = new float[mpw][mpl][3];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v][0] = MathHelperScalar.clamp(rMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
				map[u][v][1] = MathHelperScalar.clamp(gMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
				map[u][v][2] = MathHelperScalar.clamp(bMap.getLocalHeight(u, v) / normalizedHeight, 0.0f, 1.0f);
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	@Override
	public String getModuleName() {
		return "RGB";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "R"),
				new ModuleInput(new DatatypeHeightmap(), "G"),
				new ModuleInput(new DatatypeHeightmap(), "B")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
