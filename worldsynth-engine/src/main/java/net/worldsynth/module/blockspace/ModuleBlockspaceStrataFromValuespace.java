/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.list.MaterialStateListParameter;

public class ModuleBlockspaceStrataFromValuespace extends AbstractModule {

	private MaterialStateListParameter strata = new MaterialStateListParameter("strata", "Strata", null, Arrays.asList(MaterialRegistry.getDefaultMaterial().getDefaultState()));
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				strata
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		List<MaterialState<?, ?>> strata = this.strata.getValueList();
		int strataLayers = strata.size();
		double dStrataLayers = (double) strataLayers;
		
		if (inputs.get("valuespace") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputSpace0 = ((DatatypeValuespace) inputs.get("valuespace")).getValuespace();
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					float i0 = inputSpace0[u][v][w];
					
					
					double layer = (i0 % 1.0) * dStrataLayers + (i0 >= 0.0 ? 0.0 : dStrataLayers);
					int layerIndex = (int) (layer >= 0 ? layer : layer - 1);
					
					layerIndex = layerIndex % strataLayers;
					blockspace[u][v][w] = strata.get(layerIndex);
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeValuespace dvv = new DatatypeValuespace(ord.extent, ord.resolution);
		
		inputRequests.put("valuespace", new ModuleInputRequest(getInput(0), dvv));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Strata from valuespace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
