/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleBlockspaceFromHeightmap extends AbstractModule {
	
	private MaterialStateParameter material = new MaterialStateParameter("material", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				material
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double y = requestData.extent.getY();
		double height = requestData.extent.getHeight();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		float[][] inputMapHigh = null;
		if (inputs.get("high") != null) {
			inputMapHigh = ((DatatypeHeightmap) inputs.get("high")).getHeightmap();
		}
		
		float[][] inputMapLow = null;
		if (inputs.get("low") != null) {
			inputMapLow = ((DatatypeHeightmap) inputs.get("low")).getHeightmap();
		}
		
		MaterialState<?, ?> material = this.material.getValue();
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int w = 0; w < spl; w++) {
				
				double instantMinHeight = Math.max(y, Integer.MIN_VALUE);
				double instantMaxHeight = Math.min(y+height, Integer.MAX_VALUE);
				if (inputMapHigh != null) {
					instantMaxHeight = Math.min(instantMaxHeight, inputMapHigh[u][w]);
				}
				if (inputMapLow != null) {
					instantMinHeight = Math.max(instantMinHeight, inputMapLow[u][w]);
				}
				
				int minArrayHeight = (int)((instantMinHeight-y)/res);
				int maxArrayHeight = (int)((instantMaxHeight-y)/res);
				
				for (int v = minArrayHeight; v < maxArrayHeight; v++) {
					blockspace[u][v][w] = material;
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("high", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("low", new ModuleInputRequest(getInput(1), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace from heightmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "High"),
				new ModuleInput(new DatatypeHeightmap(), "Low")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
