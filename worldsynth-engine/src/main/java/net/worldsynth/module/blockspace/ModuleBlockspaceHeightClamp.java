/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleBlockspaceHeightClamp extends AbstractModule {
	
	private FloatParameter highClamp = new FloatParameter("highclamp", "High clamp", null, 256.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, 0.0f, 256.0f);
	private FloatParameter lowClamp = new FloatParameter("lowclamp", "Low clamp", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, 0.0f, 256.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highClamp,
				lowClamp
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double y = requestData.extent.getY();
		double height = requestData.extent.getHeight();
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		double res = requestData.resolution;
		
		float highClamp = this.highClamp.getValue();
		float lowClamp = this.lowClamp.getValue();
		
		MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
		
		float[][] inputMap1 = null;
		float[][] inputMap2 = null;
		
		if (inputs.get("input") == null) {
			//If the main or secondary input is null, there is not enough input and then just return null
			return null;
		}
		if (inputs.get("high") != null) {
			inputMap1 = ((DatatypeHeightmap) inputs.get("high")).getHeightmap();
		}
		if (inputs.get("low") != null) {
			inputMap2 = ((DatatypeHeightmap) inputs.get("low")).getHeightmap();
		}
		
		MaterialState<?, ?>[][][] inputBlockspace = ((DatatypeBlockspace) inputs.get("input")).getBlockspace();
		
		for (int u = 0; u < spw; u++) {
			for (int w = 0; w < spl; w++) {
				
				double instantMinHeight = Math.max(y, lowClamp);
				double instantMaxHeight = Math.min(y+height, highClamp);
				if (inputMap1 != null) {
					instantMaxHeight = Math.min(y+height, inputMap1[u][w]-y);
				}
				if (inputMap2 != null) {
					instantMinHeight = Math.max(y, inputMap2[u][w] - y);
				}
				
				int minHeight = (int)(instantMinHeight/res);
				int maxHeight = (int)(instantMaxHeight/res);
				
				for (int v = minHeight; v < maxHeight; v++) {
					blockspace[u][v][w] = inputBlockspace[u][v][w];
				}
			}
		}
		
		requestData.setBlockspace(blockspace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("high", new ModuleInputRequest(getInput(1), heightmapRequestData));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace height clamp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "High"),
				new ModuleInput(new DatatypeHeightmap(), "Low")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
