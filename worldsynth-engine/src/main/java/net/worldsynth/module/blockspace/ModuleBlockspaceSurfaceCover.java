/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.parameter.MaterialStateParameter;

public class ModuleBlockspaceSurfaceCover extends AbstractModule {
	
	private MaterialStateParameter material = new MaterialStateParameter("material", "Material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	private FloatParameter depth = new FloatParameter("depth", "Cover depth", null, 1.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 100.0f);
	private BooleanParameter coverOnTop = new BooleanParameter("ontop", "Cover on top", null, false);
	private BooleanParameter onlyFirstSurface = new BooleanParameter("onlyfirst", "Only first surface", null, true);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				material,
				depth,
				coverOnTop,
				onlyFirstSurface
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		MaterialState<?, ?> material = this.material.getValue();
		float depth = this.depth.getValue();
		boolean coverOnTop = this.coverOnTop.getValue();
		boolean onlyOnFirstSurface = this.onlyFirstSurface.getValue();
		
		if (inputs.get("input") == null) {
			return null;
		}
		MaterialState<?, ?>[][][] blockspace = ((DatatypeBlockspace) inputs.get("input").clone()).getBlockspace();
		
		MaterialState<?, ?>[][] inputMaterialmap = null;
		if (inputs.get("material") != null) {
			inputMaterialmap = ((DatatypeMaterialmap) inputs.get("material")).getMaterialmap();
		}
		
		float[][] inputHeightmap = null;
		if (inputs.get("depth") != null) {
			inputHeightmap = ((DatatypeHeightmap) inputs.get("depth")).getHeightmap();
		}
		
		//----------BUILD----------//

		for (int u = 0; u < spw; u++) {
			for (int w = 0; w < spl; w++) {
				if (inputMaterialmap != null) {
					material = inputMaterialmap[u][w];
				}
				
				double localDepth = depth;
				if (inputHeightmap != null) {
					localDepth *= inputHeightmap[u][w] / normalizedHeight;
				}
				
				if (coverOnTop) {
					coverColumnUp(u, sph, w, res, blockspace, material, localDepth, onlyOnFirstSurface);
				}
				else {
					coverColumnDown(u, sph, w, res, blockspace, material, localDepth, onlyOnFirstSurface);
				}
			}
		}

		requestData.setBlockspace(blockspace);
		
		return requestData;
	}
	
	private void coverColumnDown(int u, int height, int w, double res, MaterialState<?, ?>[][][] blockspace, MaterialState<?, ?> coverMaterial, double depth, boolean onlyFirst) {
		double remainingDepth = depth;
		for (int v = height-1; v >= 0; v--) {
			if (blockspace[u][v][w].isAir()) {
				remainingDepth = depth;
			}
			else {
				while (v >= 0 && remainingDepth > 0) {
					if (blockspace[u][v][w].isAir()) {
						remainingDepth = depth;
						break;
					}
					
					blockspace[u][v][w] = coverMaterial;
					remainingDepth -= res;
					v--;
				}
				if (onlyFirst) {
					return;
				}
			}
		}
	}
	
	private void coverColumnUp(int u, int height, int w, double res, MaterialState<?, ?>[][][] blockspace, MaterialState<?, ?> coverMaterial, double depth, boolean onlyFirst) {
		double remainingDepth = depth;
		for (int v = height-1; v >= 0; v--) {
			if (blockspace[u][v][w].isAir()) {
				remainingDepth = depth;
				continue;
			}
			else {
				while (++v < height && remainingDepth > 0) {
					if (!blockspace[u][v][w].isAir()) {
						remainingDepth = depth;
						break;
					}
					
					blockspace[u][v][w] = coverMaterial;
					remainingDepth -= res;
				}
				if (onlyFirst) {
					return;
				}
				v--;
			}
		}
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeMaterialmap materialmapRequeatData = new DatatypeMaterialmap(ord.extent, ord.resolution);
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), ord));
		inputRequests.put("material", new ModuleInputRequest(getInput("Material"), materialmapRequeatData));
		inputRequests.put("depth", new ModuleInputRequest(getInput("Depth"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Surface cover";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input"),
				new ModuleInput(new DatatypeMaterialmap(), "Material"),
				new ModuleInput(new DatatypeHeightmap(), "Depth")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
