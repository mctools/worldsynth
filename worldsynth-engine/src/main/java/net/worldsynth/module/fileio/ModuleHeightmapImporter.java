/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.fileio;

import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.FileParameter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ModuleHeightmapImporter extends AbstractModule {
	
	private FileParameter heightmapImageFile = new FileParameter("filepath", "Heightmap file", null, null, false, false, new ExtensionFilter("PNG", "*.png"));
	private DoubleParameter xOffset = new DoubleParameter("xoffset", "X offset", null, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 1000);
	private DoubleParameter zOffset = new DoubleParameter("zoffset", "Z offset", null, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1000, 1000);
	private BooleanParameter cache = new BooleanParameter("cache", "Cache colormap", null, true);
	
	private float[][] cachedHeightmap = null;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				xOffset,
				zOffset,
				heightmapImageFile,
				cache
				};
		return p;
	}

	@Override
	protected void onParametersChange() {
		cachedHeightmap = null;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double xOffset = this.xOffset.getValue();
		double zOffset = this.zOffset.getValue();
		File heightmapImageFile = this.heightmapImageFile.getValue();
		
		if (heightmapImageFile == null || !heightmapImageFile.exists()) {
			return null;
		}
		
		float[][] fileInputMap = cachedHeightmap;
		if (cache.getValue() && cachedHeightmap == null) {
			fileInputMap = cachedHeightmap = readeImageFromFile(heightmapImageFile);
		}
		else if (!cache.getValue()) {
			fileInputMap = readeImageFromFile(heightmapImageFile);
		}
		
		int inputMapWidth = fileInputMap.length;
		int inputMapHeight = fileInputMap[0].length;
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		for (int u = 0; u < mpw; u++) {
			int lx = (int) Math.floor((double) u * res + x - xOffset);
			if (lx < 0 || lx >= inputMapWidth) {
				continue;
			}
			for (int v = 0; v < mpl; v++) {
				int lz = (int) Math.floor((double) v * res + z - zOffset);
				if (lz < 0 || lz >= inputMapHeight) {
					continue;
				}
				map[u][v] = fileInputMap[lx][lz] * normalizedHeight;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.IMPORTER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Heightmap")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private float[][] readeImageFromFile(File f) {
		float[][] heightmap = null;
		
		try {
			BufferedImage image = ImageIO.read(f);
			double divident = 256.0;
			
			switch (image.getType()) {
			case BufferedImage.TYPE_USHORT_GRAY:
				divident = 65536.0;
				break;
			case BufferedImage.TYPE_USHORT_565_RGB:
				divident = 32.0;
				break;
			case BufferedImage.TYPE_USHORT_555_RGB:
				divident = 32.0;
				break;
			default:
				divident = 256.0;
				break;
			}
			
			heightmap = new float[image.getWidth()][image.getHeight()];
			for (int x = 0; x < heightmap.length; x++) {
				for (int y = 0; y < heightmap[0].length; y++) {
					double[] height = image.getRaster().getPixel(x, y, new double[4]);
					heightmap[x][y] = (float) (height[0] / divident);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return heightmap;
	}
}
