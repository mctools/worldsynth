/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleVectorfieldAngle extends AbstractModule {
	
	private FloatParameter gain = new FloatParameter("gain", "Gain", null, 1.0f, 0.0f, Float.MAX_VALUE, 0.0f, 1.0f);
	private BooleanParameter normalize = new BooleanParameter("normalize", "Normalize", null, false);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain,
				normalize
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeVectormap vectorfieldRequestData = new DatatypeVectormap(ord.extent, ord.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), vectorfieldRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		float gain = this.gain.getValue();
		boolean normalize = this.normalize.getValue();
		
		if (inputs.get("input") == null) {
			//If the inputs is null, there is not enough input and then just return null.
			return null;
		}
		float[][][] inputField = ((DatatypeVectormap) inputs.get("input")).getVectormap();
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				double angle = Math.atan2(inputField[u][v][1], inputField[u][v][0]);
				if (normalize) {
					angle *= normalizedHeight / (2.0 * Math.PI);
					if (angle < 0) {
						angle += normalizedHeight;
					}
				}
				map[u][v] = (float) angle * gain;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public String getModuleName() {
		return "Vectormap angle";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeVectormap(), "Input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
