/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.patch.ModuleWrapper;

public class ModuleConstruction {
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(Class<? extends AbstractModule> moduleClass, JsonNode jsonNode, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		Constructor<? extends AbstractModule> ct = moduleClass.getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromJson(jsonNode);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		return moduleInstance;
	}
	
	public static AbstractModule constructModule(ModuleEntry moduleEntry, JsonNode jsonNode, ModuleWrapper wrapper) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		Constructor<? extends AbstractModule> ct = moduleEntry.getModuleClass().getConstructor();
		AbstractModule moduleInstance = ct.newInstance();
		moduleInstance.init(wrapper);
		moduleInstance.fromJson(jsonNode);
		return moduleInstance;
	}
}
