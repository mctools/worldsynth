/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.datatype.AbstractDatatype;

/**
 * Used in the process of building a node tree, to carry a request for data to a module.
 * This is the request constructed by a module based on the outputrequest it takes in.
 */
public class ModuleInputRequest {
	public ModuleInput input;
	public AbstractDatatype data;
	
	public ModuleInputRequest(ModuleInput input, AbstractDatatype data) {
		this.input = input;
		this.data = data;
	}
	
	public ModuleInput getInput() {
		return input;
	}
	
	public AbstractDatatype getData() {
		return data;
	}
}
