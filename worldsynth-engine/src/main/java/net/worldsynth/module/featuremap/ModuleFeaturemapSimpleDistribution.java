/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.featuremap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.gen.Permutation;

public class ModuleFeaturemapSimpleDistribution extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.MAX_VALUE, 1.0, 200.0);
	private DoubleParameter probability = new DoubleParameter("probability", "Probability", null, 0.2, 0.0, 1.0);
	private DoubleParameter displacement = new DoubleParameter("displacement", "Displacement", null, 0.0, 0.0, 1.0);
	
	private final int permutationSize = 1024;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 4);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				probability,
				displacement,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeFeaturemap ord = (DatatypeFeaturemap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		inputRequests.put("probabilitymap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double width = requestData.extent.getWidth();
		double length = requestData.extent.getLength();
		double resolution = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		float[][] inputMap0_probabilityMap = null;
		if (inputs.get("probabilitymap") != null) {
			inputMap0_probabilityMap = ((DatatypeHeightmap) inputs.get("probabilitymap")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		double scale = this.scale.getValue();
		double displacement = this.displacement.getValue();
		double probability = this.probability.getValue();
		
		int minXPoint = (int)Math.floor(x/scale);
		int maxXPoint = (int)Math.ceil((x+width)/scale);
		int minZPoint = (int)Math.floor(z/scale);
		int maxZPoint = (int)Math.ceil((z+length)/scale);
		
		ArrayList<Featurepoint2D> generatedPoints = new ArrayList<Featurepoint2D>();
		
		for (int u = minXPoint; u < maxXPoint; u++) {
			for (int v = minZPoint; v < maxZPoint; v++) {
				int[] hashCoordinates = hashCoordianteAt(u, v);
				
				double xPos = u * scale;
				double zPos = v * scale;
				
				double xDisplace = permutation.lUnitHash(0, hashCoordinates[0], hashCoordinates[1]);
				double zDisplace = permutation.lUnitHash(1, hashCoordinates[0], hashCoordinates[1]);
				
				xPos += -scale*displacement/2.0f + xDisplace*scale*displacement;
				zPos += -scale*displacement/2.0f + zDisplace*scale*displacement;
				
				if (insideBoundary(xPos, zPos, requestData)) {
					if (inputMap0_probabilityMap != null) {
						probability = inputMap0_probabilityMap[(int) ((xPos-x)/resolution)][(int) ((zPos-z)/resolution)]/normalizedHeight;
					}
					if (permutation.lUnitHash(2, hashCoordinates[0], hashCoordinates[1]) < probability) {
						generatedPoints.add(new Featurepoint2D(xPos, zPos, permutation.lHash(3, hashCoordinates[0], hashCoordinates[1])));
					}
				}
			}
		}
		
		Featurepoint2D[] points = new Featurepoint2D[generatedPoints.size()];
		generatedPoints.toArray(points);
		requestData.setFeaturepoints(points);
		
		return requestData;
	}
	
	private boolean insideBoundary(double x, double z, DatatypeFeaturemap requestData) {
		return requestData.extent.isContained(x, z);
	}
	
	private int[] hashCoordianteAt(int x, int z) {
		
		if (repeat > 0) {
			x = x < 0 ? repeat+(x%repeat) : x%repeat;
			z = z < 0 ? repeat+(z%repeat) : z%repeat;
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int zi = (int)z & 255;
		
		return new int[] {xi, zi};
	}

	@Override
	public String getModuleName() {
		return "Simple distribution";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Probability")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturemap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
