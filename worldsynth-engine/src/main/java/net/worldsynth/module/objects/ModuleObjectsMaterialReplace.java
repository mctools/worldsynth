/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.table.BooleanColumn;
import net.worldsynth.parameter.table.MaterialStateColumn;
import net.worldsynth.parameter.table.TableParameter;

public class ModuleObjectsMaterialReplace extends AbstractModule {
	
	private MaterialStateColumn<MaterialMaterialEntry> inMaterialColumn = new MaterialStateColumn<MaterialMaterialEntry>("inmaterial", "Material", null) {

		@Override
		public MaterialState<?, ?> getColumnValue(MaterialMaterialEntry par) {
			return par.getInMaterialState();
		}

		@Override
		public void setColumnValue(MaterialMaterialEntry row, MaterialState<?, ?> value) {
			row.setInMaterialState(value);
		}
	};
	
	private BooleanColumn<MaterialMaterialEntry> inMaterialAllStatesColumn = new BooleanColumn<MaterialMaterialEntry>("allstates", "All states", null) {

		@Override
		public Boolean getColumnValue(MaterialMaterialEntry par) {
			return par.getAllStates();
		}

		@Override
		public void setColumnValue(MaterialMaterialEntry row, Boolean value) {
			row.setAllStates(value);
		}
	};
	
	private MaterialStateColumn<MaterialMaterialEntry> outMaterialColumn = new MaterialStateColumn<MaterialMaterialEntry>("outmaterial", "Replace with", null) {
		
		@Override
		public MaterialState<?, ?> getColumnValue(MaterialMaterialEntry par) {
			return par.getOutMaterialState();
		}
		
		@Override
		public void setColumnValue(MaterialMaterialEntry row, MaterialState<?, ?> value) {
			row.setOutMaterialState(value);
		}
	};
	
	private TableParameter<MaterialMaterialEntry> palette = new TableParameter<MaterialMaterialEntry>("palette", "Palette", null,
			Arrays.asList(
					new MaterialMaterialEntry(MaterialRegistry.getDefaultMaterial().getDefaultState(), false, MaterialRegistry.getDefaultMaterial().getDefaultState())
					),
			inMaterialColumn, inMaterialAllStatesColumn, outMaterialColumn) {

		@Override
		public MaterialMaterialEntry newRow() {
			return new MaterialMaterialEntry(Material.NULL.getDefaultState(), false, Material.NULL.getDefaultState());
		}
	};
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				palette
				};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		//----------READ INPUTS----------//
		
		List<MaterialMaterialEntry> palette = this.palette.getValue();
		IdentityHashMap<MaterialState<?, ?>, MaterialState<?, ?>> paletteMapStates = new IdentityHashMap<MaterialState<?,?>, MaterialState<?,?>>();
		IdentityHashMap<Material<?, ?>, MaterialState<?, ?>> paletteMapMaterials = new IdentityHashMap<Material<?,?>, MaterialState<?,?>>();
		for (MaterialMaterialEntry entry: palette) {
			paletteMapStates.put(entry.getInMaterialState(), entry.getOutMaterialState());
			if (entry.getAllStates()) {
				paletteMapMaterials.put(entry.getInMaterialState().getMaterial(), entry.getOutMaterialState());
			}
		}
		
		//Check if inputs are available
		if (inputs.get("objects") == null) {
			//If the objects input is null, there is not enough input and then just return null
			return null;
		}
		
		LocatedCustomObject[] inputObjects = ((DatatypeObjects) inputs.get("objects")).getLocatedObjects();
		
		//----------BUILD----------//
		
		LocatedCustomObject[] outputObjects = new LocatedCustomObject[inputObjects.length];
		
		IdentityHashMap<CustomObject, CustomObject> modifiedObjects = new IdentityHashMap<CustomObject, CustomObject>();
		
		for (int i = 0; i < inputObjects.length; i++) {
			LocatedCustomObject io = inputObjects[i];
			double x = io.getX();
			double y = io.getY();
			double z = io.getZ();
			long seed = io.getSeed();
			CustomObject object = io.getObject();
			
			if (!modifiedObjects.containsKey(object)) {
				modifiedObjects.put(object, replaceObjectMaterials(object, paletteMapStates, paletteMapMaterials));
			}
			
			outputObjects[i] = new LocatedCustomObject(x, y, z, seed, modifiedObjects.get(object));
		}
		
		return new DatatypeObjects(outputObjects, requestData.extent, requestData.resolution);
	}
	
	private CustomObject replaceObjectMaterials(CustomObject object, Map<MaterialState<?, ?>, MaterialState<?, ?>> paletteStates, Map<Material<?, ?>, MaterialState<?, ?>> paletteMaterials) {
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (Block b: object.getBlocks()) {
			
			if (paletteStates.containsKey(b.material)) {
				blocks.add(new Block(b.x, b.y, b.z, paletteStates.get(b.material)));
			}
			else if (paletteMaterials.containsKey(b.material.getMaterial())) {
				blocks.add(new Block(b.x, b.y, b.z, paletteMaterials.get(b.material.getMaterial())));
			}
			else {
				blocks.add(new Block(b.x, b.y, b.z, b.material));
			}
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("objects", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Objects replace material";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeObjects(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	public static class MaterialMaterialEntry {
		private MaterialState<?, ?> inMaterialState;
		private Boolean allStates;
		private MaterialState<?, ?> outMaterialState;
		
		public MaterialMaterialEntry(MaterialState<?, ?> inMaterialstate, Boolean allStates, MaterialState<?, ?> outMaterialstate) {
			this.inMaterialState = inMaterialstate;
			this.allStates = allStates;
			this.outMaterialState = outMaterialstate;
		}
		
		public final MaterialState<?, ?> getInMaterialState() {
			return inMaterialState;
		}
		
		public final void setInMaterialState(MaterialState<?, ?> materialstate) {
			this.inMaterialState = materialstate;
		}
		
		public Boolean getAllStates() {
			return allStates;
		}
		
		public void setAllStates(Boolean allStates) {
			this.allStates = allStates;
		}
		
		public final MaterialState<?, ?> getOutMaterialState() {
			return outMaterialState;
		}
		
		public final void setOutMaterialState(MaterialState<?, ?> materialstate) {
			this.outMaterialState = materialstate;
		}
		
		@Override
		protected MaterialMaterialEntry clone() {
			return new MaterialMaterialEntry(getInMaterialState(), getAllStates(), getOutMaterialState());
		}
	}
}
