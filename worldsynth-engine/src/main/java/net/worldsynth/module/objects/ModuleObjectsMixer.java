/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;

public class ModuleObjectsMixer extends AbstractModule {
	
	private DoubleParameter bias = new DoubleParameter("bias", "Selection bias", null, 0.0, -1.0, 1.0);
	private LongParameter seed = new LongParameter("seed", "Seed", null, new Random().nextLong(), Long.MIN_VALUE, Long.MAX_VALUE);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				bias,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double bias = this.bias.getValue();
		Random random = new Random(seed.getValue());
		
		ArrayList<LocatedCustomObject> primaryRequestObjects = new ArrayList<LocatedCustomObject>();
		ArrayList<LocatedCustomObject> secondaryRequestObjects = new ArrayList<LocatedCustomObject>();
		
		DatatypeObjects requestData = (DatatypeObjects) outputRequest.data;
		for (LocatedCustomObject lo: requestData.getLocatedObjects()) {
			double rv = random.nextDouble() * 2.0 - 1.0;
			if (rv >= bias) {
				primaryRequestObjects.add(lo);
			}
			else {
				secondaryRequestObjects.add(lo);
			}
		}
		
		DatatypeObjects primaryRequestData = new DatatypeObjects(primaryRequestObjects.toArray(new LocatedCustomObject[0]), requestData.extent, requestData.resolution);
		DatatypeObjects secondaryRequestData = new DatatypeObjects(secondaryRequestObjects.toArray(new LocatedCustomObject[0]), requestData.extent, requestData.resolution);
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), primaryRequestData));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), secondaryRequestData));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		//----------READ INPUTS----------//
		
		// Check if inputs are available
		if (inputs.get("primary") == null || inputs.get("secondary") == null) {
			// If there is not enough inputs, then just return null.
			return null;
		}
		
		LocatedCustomObject[] primaryInputObjects = ((DatatypeObjects) inputs.get("primary")).getLocatedObjects();
		LocatedCustomObject[] secondaryInputObjects = ((DatatypeObjects) inputs.get("secondary")).getLocatedObjects();
		
		//----------BUILD----------//
		
		LocatedCustomObject[] outputObjects = new LocatedCustomObject[primaryInputObjects.length + secondaryInputObjects.length];
		
		int i = 0;
		for (LocatedCustomObject lo: primaryInputObjects) {
			outputObjects[i++] = lo;
		}
		for (LocatedCustomObject lo: secondaryInputObjects) {
			outputObjects[i++] = lo;
		}
		
		return new DatatypeObjects(outputObjects, requestData.extent, requestData.resolution);
	}

	@Override
	public String getModuleName() {
		return "Objects mixer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeObjects(), "Primary"),
				new ModuleInput(new DatatypeObjects(), "Secondary"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
