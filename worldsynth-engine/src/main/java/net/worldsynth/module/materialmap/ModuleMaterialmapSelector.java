/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleMaterialmapSelector extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if (!inputs.containsKey("selector") || !inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If any of the inputs are not available, there is not enough input and then just return null
			return null;
		}
		
		float[][] slectorMap = ((DatatypeHeightmap) inputs.get("selector")).getHeightmap();
		MaterialState<?, ?>[][] primaryMaterialMap = ((DatatypeMaterialmap) inputs.get("primary")).getMaterialmap();
		MaterialState<?, ?>[][] secondaryMaterialMap = ((DatatypeMaterialmap) inputs.get("secondary")).getMaterialmap();
		
		MaterialState<?, ?>[][] map = new MaterialState<?, ?>[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				MaterialState<?, ?> bid = secondaryMaterialMap[u][v];
				if (slectorMap[u][v] > 0) {
					bid = primaryMaterialMap[u][v];
				}
				map[u][v] = bid;
			}
		}
		
		requestData.setMaterialmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeMaterialmap ord = (DatatypeMaterialmap) outputRequest.data;
		
		DatatypeHeightmap eslectorRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("selector", new ModuleInputRequest(getInput("Selector"), eslectorRequestData));
		
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), ord));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), ord));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Selector"),
				new ModuleInput(new DatatypeMaterialmap(), "Primary"),
				new ModuleInput(new DatatypeMaterialmap(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
