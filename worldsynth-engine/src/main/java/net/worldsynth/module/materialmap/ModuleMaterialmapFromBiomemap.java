/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.MaterialStateParameter;
import net.worldsynth.parameter.table.BiomeColumn;
import net.worldsynth.parameter.table.MaterialStateColumn;
import net.worldsynth.parameter.table.TableParameter;

public class ModuleMaterialmapFromBiomemap extends AbstractModule {
	
	private MaterialStateParameter defaultMaterial = new MaterialStateParameter("defaultmaterial", "Default material", null, MaterialRegistry.getDefaultMaterial().getDefaultState());
	
	private BiomeColumn<BiomeMaterialEntry> biomeColumn = new BiomeColumn<BiomeMaterialEntry>("biome", "Biome", null) {

		@Override
		public Biome getColumnValue(BiomeMaterialEntry par) {
			return par.getBiome();
		}

		@Override
		public void setColumnValue(BiomeMaterialEntry row, Biome value) {
			row.setBiome(value);
		}
	};
	
	private MaterialStateColumn<BiomeMaterialEntry> materialColumn = new MaterialStateColumn<BiomeMaterialEntry>("material", "Material", null) {
		
		@Override
		public MaterialState<?, ?> getColumnValue(BiomeMaterialEntry par) {
			return par.getMaterialState();
		}
		
		@Override
		public void setColumnValue(BiomeMaterialEntry row, MaterialState<?, ?> value) {
			row.setMaterialState(value);
		}
	};
	
	private TableParameter<BiomeMaterialEntry> palette = new TableParameter<BiomeMaterialEntry>("palette", "Palette", null,
			Arrays.asList(
					new BiomeMaterialEntry(BiomeRegistry.getDefaultBiome(), MaterialRegistry.getDefaultMaterial().getDefaultState())
					),
			biomeColumn, materialColumn) {

				@Override
				public BiomeMaterialEntry newRow() {
					return new BiomeMaterialEntry(Biome.NULL, Material.NULL.getDefaultState());
				}
	};
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		AbstractParameter<?>[] p = {
				defaultMaterial,
				palette
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		MaterialState<?, ?> defaultMaterial = this.defaultMaterial.getValue();
		List<BiomeMaterialEntry> palette = this.palette.getValue();
		
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		Biome[][] inputBiomemap0 = ((DatatypeBiomemap) inputs.get("input")).getBiomemap();
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][] materialmap = new MaterialState<?, ?>[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				Biome b = inputBiomemap0[u][v];
				materialmap[u][v] = selectMaterialFromPalette(b, palette, defaultMaterial);
			}
		}
		
		requestData.setMaterialmap(materialmap);
		
		return requestData;
	}
	
	private MaterialState<?, ?> selectMaterialFromPalette(Biome biome, List<BiomeMaterialEntry> palette, MaterialState<?, ?> defaultMaterial) {
		MaterialState<?, ?> returnMaterial = defaultMaterial;
		
		for (int i = 0; i < palette.size(); i++) {
			if (palette.get(i).getBiome() == biome) {
				returnMaterial = palette.get(i).getMaterialState();
				break;
			}
		}
		
		return returnMaterial;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeMaterialmap ord = (DatatypeMaterialmap) outputRequest.data;
		
		DatatypeBiomemap hmap = new DatatypeBiomemap(ord.extent, ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), hmap));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Materialmap from biomemap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBiomemap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	public static class BiomeMaterialEntry {
		private Biome biome;
		private MaterialState<?, ?> materialstate;
		
		public BiomeMaterialEntry(Biome biome, MaterialState<?, ?> materialstate) {
			this.materialstate = materialstate;
			this.biome = biome;
		}
		
		public final Biome getBiome() {
			return biome;
		}
		
		public final void setBiome(Biome biome) {
			this.biome = biome;
		}
		
		public final MaterialState<?, ?> getMaterialState() {
			return materialstate;
		}
		
		public final void setMaterialState(MaterialState<?, ?> materialstate) {
			this.materialstate = materialstate;
		}
		
		@Override
		protected BiomeMaterialEntry clone() {
			return new BiomeMaterialEntry(getBiome(), getMaterialState());
		}
	}
}
