/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

public class BuildExtent {
	
	private final double x, y, z;
	private final double width, height, length;
	
	/**
	 * Constructs a new build extent with the given bound.
	 * 
	 * @param x      The lower bound of the extent in the x axis
	 * @param y      The lower bound of the extent in the y axis
	 * @param z      The lower bound of the extent in the z axis
	 * @param width  The width of the extent
	 * @param height The height of the extent
	 * @param length THe length 0f the extent
	 */
	public BuildExtent(double x, double y, double z, double width, double height, double length) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.length = length;
	}
	
	/**
	 * Constructs a new build extent from the given {@link WorldExtent}.
	 * 
	 * @param worldExtent
	 */
	public BuildExtent(WorldExtent worldExtent) {
		this.x = worldExtent.getX();
		this.y = worldExtent.getY();
		this.z = worldExtent.getZ();
		this.width = worldExtent.getWidth();
		this.height = worldExtent.getHeight();
		this.length = worldExtent.getLength();
	}
	
	/**
	 * Constructs a new build extent based on the base extent, expanded equally in
	 * both direction for each axis.
	 * 
	 * @param baseExtent
	 * @param expandX    The amount to bidirectionally in the x axis
	 * @param expandY    The amount to bidirectionally in the y axis
	 * @param expandZ    The amount to bidirectionally in the z axis
	 * @return
	 */
	public static BuildExtent expandedBuildExtent(BuildExtent baseExtent, double expandX, double expandY, double expandZ) {
		return expandedBuildExtent(baseExtent, expandX, expandY, expandZ, expandX, expandY, expandZ);
	}
	
	/**
	 * Constructs a new build extent based on the base extent, expanded in each
	 * direction.
	 * 
	 * @param baseExtent
	 * @param expandLowX  The amount to the lower bound in the x axis
	 * @param expandLowY  The amount to the lower bound in the y axis
	 * @param expandLowZ  The amount to the lower bound in the z axis
	 * @param expandHighX The amount to the higher bound in the x axis
	 * @param expandHighY The amount to the higher bound in the y axis
	 * @param expandHighZ The amount to the higher bound in the z axis
	 * @return
	 */
	public static BuildExtent expandedBuildExtent(BuildExtent baseExtent, double expandLowX, double expandLowY, double expandLowZ, double expandHighX, double expandHighY, double expandHighZ) {
		double x = baseExtent.x - expandLowX;
		double y = baseExtent.y - expandLowY;
		double z = baseExtent.z - expandLowZ;
		double width = baseExtent.width + expandLowX + expandHighX;
		double height = baseExtent.height + expandLowY + expandHighY;
		double length = baseExtent.length + expandLowZ + expandHighZ;
		return new BuildExtent(x, y, z, width, height, length);
	}
	
	/**
	 * Construct a new build extent based on the given extent, with a translation
	 * offset.
	 * 
	 * @param extent
	 * @param xTranslate
	 * @param yTranslate
	 * @param zTransalate
	 * @return
	 */
	public static BuildExtent translatedBuildExtent(BuildExtent extent, double xTranslate, double yTranslate, double zTransalate) {
		return new BuildExtent(extent.getX()+xTranslate, extent.getY()+yTranslate, extent.getZ()+zTransalate, extent.getWidth(), extent.getHeight(), extent.getLength());
		
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getLength() {
		return length;
	}
	
	public boolean isContained(double x, double y, double z) {
		if (x < this.x || x >= this.x + width || y < this.y || y >= this.y + height || z < this.z || z >= this.z + length) {
			return false;
		}
		return true;
	}
	
	public boolean isContained(double x, double z) {
		if (x < this.x || x >= this.x + width || z < this.z || z >= this.z + length) {
			return false;
		}
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BuildExtent that = (BuildExtent) o;

		if (Double.compare(that.x, x) != 0) return false;
		if (Double.compare(that.y, y) != 0) return false;
		if (Double.compare(that.z, z) != 0) return false;
		if (Double.compare(that.width, width) != 0) return false;
		if (Double.compare(that.height, height) != 0) return false;
		return Double.compare(that.length, length) == 0;
	}
}
