/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.worldsynth.util.event.*;

import java.util.Iterator;

public class WorldExtentManager {
	private ObservableList<WorldExtent> observableExtentsList = FXCollections.observableArrayList();
	private SimpleObjectProperty<WorldExtent> currentWorldExtent = new SimpleObjectProperty<WorldExtent>();
	
	private final EventDispatcher<ExtentManagerEvent> eventDispatcher = new EventDispatcher<>();
	private final EventHandler<ExtentEvent> extentEditHandler = extentEvent -> {
		eventDispatcher.dispatchEvent(new ExtentManagerEvent(WorldExtentManager.this, ExtentManagerEvent.EXTENT_EDITED, extentEvent));
	};
	
	public WorldExtentManager() {
		addWorldExtent(new WorldExtent("Default extent 1/4k", -128, -128, 256));
		addWorldExtent(new WorldExtent("1/2k", -256, -256, 512));
		addWorldExtent(new WorldExtent("1k", -512, -512, 1024));
		addWorldExtent(new WorldExtent("2k", -1024, -1024, 2048));
		addWorldExtent(new WorldExtent("4k", -2048, -2048, 4096));
		
		currentWorldExtent.set(observableExtentsList.get(0));
	}
	
	public WorldExtentManager(JsonNode jsonNode) {
		fromJson(jsonNode);
		currentWorldExtent.set(observableExtentsList.get(0));
	}
	
	public void addWorldExtent(WorldExtent extent) {
		extent.addEventHandler(ExtentEvent.EXTENT_EDITED, extentEditHandler);
		observableExtentsList.add(extent);

		eventDispatcher.dispatchEvent(new ExtentManagerEvent(this, ExtentManagerEvent.EXTENT_ADDED, extent));
		extent.fireExtentAddedEvent();
	}
	
	public void removeWorldExtent(WorldExtent extent) {
		int i = observableExtentsList.indexOf(extent);
		
		if (observableExtentsList.size() == 1) {
			return;
//			WorldExtent defaultExtent = new WorldExtent("Default extent 1/4k", -128, -128, 256);
//			addWorldExtent(defaultExtent);
//			setCurrentWorldExtent(defaultExtent);
		}
		else if (i+1 < observableExtentsList.size()) {
			setCurrentWorldExtent(observableExtentsList.get(i+1));
		}
		else {
			setCurrentWorldExtent(observableExtentsList.get(i-1));
		}

		extent.fireExtentRemovedEvent();
		eventDispatcher.dispatchEvent(new ExtentManagerEvent(this, ExtentManagerEvent.EXTENT_REMOVED, extent));

		observableExtentsList.remove(extent);
		extent.removeEventHandler(ExtentEvent.EXTENT_EDITED, extentEditHandler);
	}
	
	public ObservableList<WorldExtent> getObservableExtentsList() {
		return observableExtentsList;
	}
	
	public WorldExtent getCurrentWorldExtent() {
		return currentWorldExtent.getValue();
	}
	
	public void setCurrentWorldExtent(WorldExtent extent) {
		currentWorldExtent.setValue(extent);
	}
	
	public SimpleObjectProperty<WorldExtent> currentWorldExtentProperty() {
		return currentWorldExtent;
	}
	
	public WorldExtent getWorldExtentById(long id) {
		for (WorldExtent e: getObservableExtentsList()) {
			if (e.getId() == id) {
				return e;
			}
		}
		return null;
	}
	
	public final void addEventHandler(EventType<ExtentManagerEvent> eventType, final EventHandler<ExtentManagerEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public final void removeEventHandler(EventType<ExtentManagerEvent> eventType, final EventHandler<ExtentManagerEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode extentsNode = objectMapper.createArrayNode();
		
		Iterator<WorldExtent> extentsIterator = observableExtentsList.listIterator();
		while (extentsIterator.hasNext()) {
			extentsNode.add(extentsIterator.next().toJson());
		}
			
		return extentsNode;
	}
	
	protected void fromJson(JsonNode extentsNode) {
		for (JsonNode e: extentsNode) {
			addWorldExtent(new WorldExtent(e));
		}
	}
}
