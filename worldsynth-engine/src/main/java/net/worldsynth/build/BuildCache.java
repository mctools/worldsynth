/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.build;

import net.worldsynth.datatype.*;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.patch.ModuleWrapper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.SoftReference;
import java.util.*;

public class BuildCache {
	private static final Logger LOGGER = LogManager.getLogger(BuildCache.class);
	private HashMap<ModuleWrapper, HashMap<ModuleOutput, List<CacheEntry>>> cache = new HashMap<>();
	private LinkedHashSet<CacheEntry> accessOrder = new LinkedHashSet<>();

	public void addCache(ModuleWrapper wrapper, ModuleOutputRequest request, AbstractDatatype data) {
		if (!cache.containsKey(wrapper)) {
			cache.put(wrapper, new HashMap<>());
		}

		List<CacheEntry> cached = cache.get(wrapper).get(request.output);
		if (cached == null) {
			cached = new ArrayList<>();
			cache.get(wrapper).put(request.output, cached);
		}

		CacheEntry cacheEntry = new CacheEntry(data, wrapper, request.output);
		cacheEntry.soften();
		cached.add(cacheEntry);
		accessOrder.add(cacheEntry);

		if (getUsage(true) > 0.8) {
			int toFree = accessOrder.size() / 10;
			System.out.println("Freeing " + toFree + " entries");
			free(toFree);
			System.gc();

			getUsage(true);
		}
	}

	private double getUsage(boolean log) {
		double heapSize = Runtime.getRuntime().totalMemory() / (double) (1024 * 1024 * 1024);
		double heapMaxSize = Runtime.getRuntime().maxMemory() / (double) (1024 * 1024 * 1024);
		double freeMemory = Runtime.getRuntime().freeMemory() / (double) (1024 * 1024 * 1024);
		double usage = (heapSize - freeMemory) / heapMaxSize;

		if (log) {
			LOGGER.printf(Level.DEBUG, "Cache size: %3d\tusage: %.4f\tfree: %.4f\t heap: %.4f\t used: %.4f\tmax: %.4f",
					accessOrder.size(), usage, freeMemory, heapSize, (heapSize - freeMemory), heapMaxSize);
		}

		return usage;
	}

	public AbstractDatatype getCache(ModuleWrapper wrapper, ModuleOutputRequest request) {
		if (!cache.containsKey(wrapper)) {
			return null;
		}

		List<CacheEntry> cached = cache.get(wrapper).get(request.output);
		if (cached != null) {
			Iterator<CacheEntry> it = cached.iterator();
			while (it.hasNext()) {
				CacheEntry cacheEntry = it.next();
				AbstractDatatype data = cacheEntry.get();
				if (data == null) {
					it.remove();
					accessOrder.remove(cacheEntry);
					continue;
				}

				if (data.compareRequestData(request.data)) {
					accessOrder.remove(cacheEntry);
					accessOrder.add(cacheEntry);
					return data;
				}
			}
		}

		return null;
	}
	
	public int hasCachedData(ModuleWrapper wrapper) {
		if (!cache.containsKey(wrapper)) {
			return 0;
		}

		int cachedInstancesCount = 0;
		for (List<CacheEntry> cached: cache.get(wrapper).values()) {
			if (cached != null) {
				Iterator<CacheEntry> it = cached.iterator();
				while (it.hasNext()) {
					CacheEntry cacheEntry = it.next();
					if (cacheEntry.isCleared()) {
						it.remove();
						accessOrder.remove(cacheEntry);
					}
					else {
						cachedInstancesCount++;
					}
				}
			}
		}
		
		return cachedInstancesCount;
	}

	public int clearCachedData(ModuleWrapper wrapper) {
		if (!cache.containsKey(wrapper)) {
			return 0;
		}

		int cachedInstancesRemoved = 0;
		for (List<CacheEntry> cached: cache.get(wrapper).values()) {
			if (cached != null) {
				Iterator<CacheEntry> it = cached.iterator();
				while (it.hasNext()) {
					CacheEntry cacheEntry = it.next();
					if (cacheEntry.isCleared()) {
						cachedInstancesRemoved++;
					}
					it.remove();
					accessOrder.remove(cacheEntry);
				}
			}
		}

		return cachedInstancesRemoved;
	}

	private void free(int entries) {
		Iterator<CacheEntry> it = accessOrder.iterator();
		int i = 0;
		while (it.hasNext() && i < entries) {
			CacheEntry entry = it.next();
			cache.get(entry.getWrapper()).get(entry.getOutput()).remove(entry);
			it.remove();
			i++;
		}
	}

	private class CacheEntry {
		private final ModuleWrapper wrapper;
		private final ModuleOutput output;
		private AbstractDatatype hardReferent;
		private SoftReference<AbstractDatatype> softReferent;

		public CacheEntry(AbstractDatatype data, ModuleWrapper wrapper, ModuleOutput output) {
			this.wrapper = wrapper;
			this.output = output;
			this.hardReferent = data;
			this.softReferent = new SoftReference<>(data);
		}

		public AbstractDatatype get() {
			return softReferent.get();
		}

		public boolean isCleared() {
			return softReferent.refersTo(null);
		}

		public boolean isSoft() {
			return hardReferent == null;
		}

		public void soften() {
			hardReferent = null;
		}

		public ModuleWrapper getWrapper() {
			return wrapper;
		}

		public ModuleOutput getOutput() {
			return output;
		}
	}
}
