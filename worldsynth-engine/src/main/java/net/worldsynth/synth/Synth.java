/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.patch.Patch;
import net.worldsynth.util.event.*;

import java.io.File;

public class Synth {

	private EventDispatcher<SynthEvent> eventDispatcher = new EventDispatcher<>();
	
	private String name = "";
	private File file;

	protected WorldExtentManager extentManager = new WorldExtentManager();
	protected SynthParameters parameters = new SynthParameters();
	protected Patch patch = new Patch(this);
	
	public Synth(String name) {
		this.name = name;
		patch.addEventHandler(PatchEvent.PATCH_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});
	}
	
	public Synth(JsonNode jsonNode) {
		fromJson(jsonNode, this);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setFile(File synthFile) {
		this.file = synthFile;
	}
	
	public File getFile() {
		return file;
	}
	
	public WorldExtentManager getExtentManager() {
		return extentManager;
	}
	
	public Patch getPatch() {
		return patch;
	}
	
	public void addEventHandler(EventType<SynthEvent> eventType, EventHandler<SynthEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}
	
	public void removeEventHandler(EventType<SynthEvent> eventType, EventHandler<SynthEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode synthNode = objectMapper.createObjectNode();
		
		synthNode.put("name", name);
		synthNode.set("extents", getExtentManager().toJson());
		synthNode.set("parameters", getParameters().toJson());
		synthNode.set("patch", patch.toJson());
		
		return synthNode;
	}
	
	protected void fromJson(JsonNode synthNode, Synth parentSynth) {
		name = synthNode.get("name").asText();
		extentManager = new WorldExtentManager(synthNode.get("extents"));
		extentManager.addEventHandler(ExtentManagerEvent.EXTENT_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});

		parameters = new SynthParameters(synthNode.get("parameters"));

		patch = new Patch(synthNode.get("patch"), this);
		patch.addEventHandler(PatchEvent.PATCH_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});
	}
	
	public SynthParameters getParameters() {
		return parameters;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Improve equals check
		return super.equals(obj);
	}
}
