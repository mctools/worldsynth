/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;

public class BiomeBuilder<B extends Biome> {
	protected String idName;
	protected String displayName;
	protected WsColor color = WsColor.MAGENTA;
	protected HashSet<String> tags;
	
	public BiomeBuilder(String idName, String displayName) {
		this.idName = idName;
		this.displayName = displayName;
	}
	
	public BiomeBuilder(String idName, JsonNode node, File biomesFile) {
		this.idName = idName;
		
		if (node.has("displayName")) {
			displayName = node.get("displayName").asText();
		}
		else {
			displayName = idName;
		}
		
		if (node.has("color")) {
			ArrayNode colorNode = (ArrayNode) node.get("color");
			int[] c = new int[colorNode.size()];
			for (int i = 0; i < colorNode.size(); i++) {
				c[i] = colorNode.get(i).asInt();
			}
			if (colorNode.size() == 3) color = new WsColor(c[0], c[1], c[2]);
			else if (colorNode.size() == 4) color = new WsColor(c[0], c[1], c[2], c[3]);
		}
		
		if (node.has("tags")) {
			tags = new ObjectMapper().convertValue(node.get("tags"), new TypeReference<LinkedHashSet<String>>(){});
		}
	}
	
	public BiomeBuilder<B> displayName(String displayName) {
		this.displayName = displayName;
		return this;
	}
	
	public BiomeBuilder<B> color(WsColor color) {
		this.color = color;
		return this;
	}
	
	public BiomeBuilder<B> color(Color color) {
		return color(new WsColor(color));
	}
	
	public BiomeBuilder<B> color(String colorstring) {
		return color(new WsColor(colorstring));
	}
	
	public BiomeBuilder<B> color(float red, float green, float blue) {
		return color(new WsColor(red, green, blue));
	}
	
	public BiomeBuilder<B> color(float red, float green, float blue, float opacity) {
		return color(new WsColor(red, green, blue, opacity));
	}
	
	public BiomeBuilder<B> color(int red, int green, int blue) {
		return color(new WsColor(red, green, blue));
	}
	
	public BiomeBuilder<B> color(int red, int green, int blue, int opacity) {
		return color(new WsColor(red, green, blue, opacity));
	}
	
	public BiomeBuilder<B> tags(String... tags) {
		this.tags = new HashSet<String>(Arrays.asList(tags));
		return this;
	}
	
	public BiomeBuilder<B> addTags(String... tags) {
		if (this.tags == null) {
			return tags(tags);
		}
		this.tags.addAll(Arrays.asList(tags));
		return this;
	}
	
	public BiomeBuilder<B> addTag(String tag) {
		return addTags(tag);
	}
	
	public B createBiome() {
		return (B) new Biome(idName, displayName, color, tags);
	}
}
