/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.Datatypes;
import net.worldsynth.module.ModuleUnknown;

public class ModuleConnector {
	
	/**
	 * The {@link ModuleWrapper} that outputs into this connector
	 */
	public ModuleWrapper module1;
	/**
	 * The output {@link ModuleWrapperIO} of the {@link ModuleWrapper} that outputs into this connector
	 */
	public ModuleWrapperIO module1Io;
	
	/**
	 * The {@link ModuleWrapper} that takes input from this connector
	 */
	public ModuleWrapper module2;
	/**
	 * The input {@link ModuleWrapperIO} of the {@link ModuleWrapper} that takes input from this connector
	 */
	public ModuleWrapperIO module2Io;
	
	public ModuleConnector(ModuleWrapper module1, ModuleWrapperIO module1Io, ModuleWrapper module2, ModuleWrapperIO module2Io) {
		this.module1 = module1;
		this.module1Io = module1Io;
		this.module2 = module2;
		this.module2Io = module2Io;
	}
	
	public ModuleConnector(JsonNode jsonNode, Patch patch) {
		fromJson(jsonNode, patch);
	}

	/**
	 * Sets the {@link ModuleWrapper} and {@link ModuleWrapperIO} that outputs into this connector
	 * @param module1
	 * @param module1Io
	 */
	public void setOutputWrapper(ModuleWrapper module1, ModuleWrapperIO module1Io) {
		this.module1 = module1;
		this.module1Io = module1Io;
	}
	
	/**
	 * Sets the {@link ModuleWrapper} and {@link ModuleWrapperIO} that takes input from this connector
	 * @param module2
	 * @param module2Io
	 */
	public void setInputWrapper(ModuleWrapper module2, ModuleWrapperIO module2Io) {
		this.module2 = module2;
		this.module2Io = module2Io;
	}
	
	/**
	 * Gets the datatype of {@link #module1Io}
	 */
	public AbstractDatatype getModule1IoDatatype() {
		if (module1 != null) {
			return module1Io.getDatatype();
		}
		return null;
	}
	
	/**
	 * Gets the datatype of {@link #module2Io}
	 */
	public AbstractDatatype getModule2IoDatatype() {
		if (module2 != null) {
			return module2Io.getDatatype();
		}
		return null;
	}
	
	public boolean verifyInputOutputType() {
		return Datatypes.verifyCompatibleTypes(getModule1IoDatatype(), getModule2IoDatatype());
	}
	
	public boolean verifyConnection() {
		if (module2Io == null) {
			return false;
		}
		else if (module1Io == null) {
			return false;
		}
		return true;
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();
		
		node.put("module1id", module1.wrapperID);
		node.put("module1io", module1Io.getName());
		node.put("module2id", module2.wrapperID);
		node.put("module2io", module2Io.getName());
		
		return node;
	}
	
	public void fromJson(JsonNode node, Patch patch) {
		String module1Id = node.get("module1id").asText();
		String module1Io = node.get("module1io").asText();
		String module2Id = node.get("module2id").asText();
		String module2Io = node.get("module2io").asText();
		
		module1 = patch.getModuleWrapperByID(module1Id);
		if (module1.module instanceof ModuleUnknown) {
			ModuleUnknown m = (ModuleUnknown) module1.module;
			m.addOutput(module1Io);
		}
		this.module1Io = module1.wrapperOutputs.get(module1Io);
		
		module2 = patch.getModuleWrapperByID(module2Id);
		if (module2.module instanceof ModuleUnknown) {
			ModuleUnknown m = (ModuleUnknown) module2.module;
			m.addInput(module2Io);
		}
		this.module2Io = module2.wrapperInputs.get(module2Io);
	}
}
