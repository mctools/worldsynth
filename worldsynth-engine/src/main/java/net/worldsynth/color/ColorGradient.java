/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.color;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ColorGradient {
	
	ArrayList<GradientStop> gradient = new ArrayList<GradientStop>();
	
	public ColorGradient(float [][] colorgradient) {
		if (colorgradient[0].length == 5) {
			for (int i = 0; i < colorgradient.length; i++) {
				gradient.add(new GradientStop(colorgradient[i][0], colorgradient[i][1], colorgradient[i][2], colorgradient[i][3], colorgradient[i][4]));
			}
		}
		else if (colorgradient[0].length == 4) {
			for (int i = 0; i < colorgradient.length; i++) {
				gradient.add(new GradientStop(colorgradient[i][0], colorgradient[i][1], colorgradient[i][2], colorgradient[i][3]));
			}
		}
		else if (colorgradient[0].length == 2) {
			for (int i = 0; i < colorgradient.length; i++) {
				gradient.add(new GradientStop(colorgradient[i][0], colorgradient[i][1]));
			}
		}
		else {
			throw new IllegalArgumentException("Color gradient array expects an aray of {o, r, g, b, a}, {o, r, g, b} or {o, g}");
		}
		
		gradient.sort(null);
	}
	
	public ColorGradient(JsonNode node) {
		fromJson(node);
	}
	
	public float[] getColorComponents(float offset) {
		int i = 0;
		while (gradient.get(Math.min(i, gradient.size()-1)).getOffset() < offset && i < gradient.size()) {
			i++;
		}
		
		GradientStop stop1 = gradient.get(Math.max(0, i-1));
		GradientStop stop2 = gradient.get(Math.min(i, gradient.size()-1));
		
		float y = (offset - stop1.getOffset()) / (stop2.getOffset() - stop1.getOffset());
		if (!Float.isFinite(y)) {
			y = 1.0f;
		}
		
		float[] colorComponents1 = stop1.getWsColor().getColorComponents();
		float[] colorComponents2 = stop2.getWsColor().getColorComponents();
		
		float[] colorComponents = new float[4];
		for (int j = 0; j < 4; j++) {
			colorComponents[j] = colorComponents2[j] * y + colorComponents1[j] * (1.0f - y);
		}
		
		return colorComponents;
	}
	
	public WsColor getColor(float offset) {
		float[] cc = getColorComponents(offset);
		return new WsColor(cc[0], cc[1], cc[2], cc[3]);
	}
	
	public GradientStop[] getStops() {
		GradientStop[] stops = new GradientStop[gradient.size()];
		gradient.toArray(stops);
		return stops;
	}
	
	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode node = objectMapper.createArrayNode();
		
		for (GradientStop stop: gradient) {
			node.add(stop.toJson());
		}
		
		return node;
	}
	
	public void fromJson(JsonNode node) {
		ArrayList<GradientStop> recoveredGradient = new ArrayList<GradientStop>();
		for (JsonNode stopNode: node) {
			recoveredGradient.add(new GradientStop(stopNode));
		}
		recoveredGradient.sort(null);
		gradient = recoveredGradient;
	}
	
	public class GradientStop implements Comparable<GradientStop> {
		private float offset;
		private WsColor color;
		
		public GradientStop(float offset, WsColor color) {
			this.offset = offset;
			this.color = color;
		}
		
		public GradientStop(float offset, float red, float green, float blue, float opacity) {
			this(offset, new WsColor(red, green, blue, opacity));
		}
		
		public GradientStop(float offset, float red, float green, float blue) {
			this(offset, red, green, blue, 1.0f);
		}
		
		public GradientStop(float offset, float g) {
			this(offset, g, g, g);
		}
		
		public GradientStop(JsonNode node) {
			fromJson(node);
		}
		
		public float getOffset() {
			return offset;
		}
		
		public WsColor getWsColor() {
			return color;
		}
		
		public float getRed() {
			return color.getRed();
		}
		
		public float getGreen() {
			return color.getGreen();
		}
		
		public float getBlue() {
			return color.getBlue();
		}
		
		public float getOpacity() {
			return color.getOpacity();
		}

		@Override
		public int compareTo(GradientStop o) {
			if (getOffset() < o.getOffset()) return -1;
			else if (getOffset() > o.getOffset()) return 1;
			return 0;
		}
		
		@Override
		public String toString() {
			return "O" + getOffset() + "R" + getRed() + "G" + getGreen() + "B" + getBlue() + "A" + getOpacity();
		}
		
		public JsonNode toJson() {
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode stopNode = objectMapper.createObjectNode();
			
			stopNode.put("offset", getOffset());
			stopNode.put("red", getRed());
			stopNode.put("green", getGreen());
			stopNode.put("blue", getBlue());
			stopNode.put("opacity", getOpacity());
			
			return stopNode;
		}
		
		public void fromJson(JsonNode node) {
			offset = (float) node.get("offset").asDouble();
			float red = (float) node.get("red").asDouble();
			float green = (float) node.get("green").asDouble();
			float blue = (float) node.get("blue").asDouble();
			float opacity = 1.0f;
			if (node.has("opacity")) {
				opacity = (float) node.get("opacity").asDouble();
			}
			color = new WsColor(red, green, blue, opacity);
		}
	}
}
