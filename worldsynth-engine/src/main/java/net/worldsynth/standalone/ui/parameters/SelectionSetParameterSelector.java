/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.controlsfx.control.SegmentedButton;

import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.AbstractSelctionSetParameter;

public class SelectionSetParameterSelector<T> extends ParameterUiElement<LinkedHashMap<T, Boolean>> {
	
	private Label nameLabel;
	private SegmentedButton segmentedButton;
	
	public SelectionSetParameterSelector(AbstractSelctionSetParameter<T> parameter) {
		super(parameter);
		
		// Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		// Toggle buttons
		segmentedButton = new SegmentedButton();
		segmentedButton.setToggleGroup(null);
		for (T k: uiValue.keySet()) {
			ToggleButton tb = new ToggleButton(k.toString());
			tb.setSelected(uiValue.get(k));
			tb.selectedProperty().addListener((observable, oldValue, newValue) -> {
				@SuppressWarnings("unchecked")
				LinkedHashMap<T, Boolean> newUiValue = (LinkedHashMap<T, Boolean>) uiValue.clone();
				newUiValue.put(k, newValue);
				uiValue = newUiValue;
				notifyChangeHandlers();
			});
			segmentedButton.getButtons().add(tb);
		}
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		segmentedButton.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(segmentedButton, 1, row, 2, 1);
	}
	
	public Label getLabel() {
		return nameLabel;
	}
	
	public SegmentedButton getSegmentedButton() {
		return segmentedButton;
	}
}
