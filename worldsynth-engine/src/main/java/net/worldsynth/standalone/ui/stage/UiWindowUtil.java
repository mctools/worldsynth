/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import javafx.beans.property.BooleanProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.WindowEvent;

public class UiWindowUtil {
	
	private static UiWindowUtil instance;
	private static  EventHandler<WindowRequestEvent> windowRequestHandler;
	
	UiWindowUtil(EventHandler<WindowRequestEvent> windowRequestHandler) {
		UiWindowUtil.instance = this;
		UiWindowUtil.windowRequestHandler = windowRequestHandler;
	}
	
	public static void openUtilityWindow(Pane contentPane, String title, boolean resizable, EventHandler<WindowEvent> windowClosingHandler) {
		windowRequestHandler.handle(instance.new WindowRequestEvent(contentPane, WindowType.UTILITY, title, resizable, windowClosingHandler));
	}
	
	public static void openPinnableUtilityWindow(Pane contentPane, String title, boolean resizable, EventHandler<WindowEvent> windowClosingHandler, BooleanProperty pinningProperty) {
		windowRequestHandler.handle(instance.new WindowRequestEvent(contentPane, WindowType.UTILITY_PINNABLE, title, resizable, windowClosingHandler, pinningProperty));
	}
	
	public static ToggleButton constructPinButton() {
		Image pinIcon = new Image(UiWindowUtil.class.getClassLoader().getResourceAsStream("pin16x16_white.png"));
		ToggleButton pinButton = new ToggleButton(null, new ImageView(pinIcon));
		return pinButton;
	}
	
	class WindowRequestEvent extends Event {
		private static final long serialVersionUID = 1057967318697791052L;

		private final Pane contentPane;
		private final WindowType windowType;
		private final String windowTitle;
		private final boolean windowResizable;
		private final EventHandler<WindowEvent> windowClosingHandler;
		private final BooleanProperty windowPinningProperty;
		
		public WindowRequestEvent(Pane contentPane, WindowType windowType, String windowTitle, boolean windowResizable, EventHandler<WindowEvent> windowClosingHandler) {
			this(contentPane, windowType, windowTitle, windowResizable, windowClosingHandler, null);
		}
		
		public WindowRequestEvent(Pane contentPane, WindowType windowType, String windowTitle, boolean windowResizable, EventHandler<WindowEvent> windowClosingHandler, BooleanProperty windowPinningProperty) {
			super(null);
			this.windowType = windowType;
			this.windowTitle = windowTitle;
			this.windowResizable = windowResizable;
			this.contentPane = contentPane;
			this.windowClosingHandler = windowClosingHandler;
			this.windowPinningProperty = windowPinningProperty;
		}
		
		public Pane getContentPane() {
			return contentPane;
		}
		
		public WindowType getWindowType() {
			return windowType;
		}
		
		public String getWindowTitle() {
			return windowTitle;
		}
		
		public boolean getWindowResizable() {
			return windowResizable;
		}
		
		public EventHandler<WindowEvent> getWindowClosingHandler() {
			return windowClosingHandler;
		}
		
		public BooleanProperty getWindowPinningProperty() {
			return windowPinningProperty;
		}
	}
	
	enum WindowType {
		UTILITY,
		UTILITY_PINNABLE;
	}
}
