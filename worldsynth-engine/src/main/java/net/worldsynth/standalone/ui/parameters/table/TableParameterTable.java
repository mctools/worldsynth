/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters.table;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import net.worldsynth.parameter.table.TableColumn;
import net.worldsynth.parameter.table.TableParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class TableParameterTable<P> extends ParameterUiElement<List<P>> {
	
	private final TableParameter<P> tableParameter;
	private final List<TableColumn<P, ?>> collumns;
	
	private ObservableList<RowUi> rowUiList = FXCollections.observableList(new ArrayList<RowUi>());
	
	private Label nameLabel;
	private GridPane tableLayout;
	private ScrollPane tableScrolPane;
	private VBox tablePane;
	
	private Button addRowButton;
	
	private int lastRemovedListIndex;

	public TableParameterTable(TableParameter<P> parameter, List<TableColumn<P, ?>> collumns) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
				
		tableParameter = parameter;
		this.collumns = collumns;
		
		uiValue = new ArrayList<P>(parameter.getValue());
		
		for (P p: uiValue) {
			rowUiList.add(new RowUi(p, collumns));
		}
		
		rowUiList.addListener(new ListChangeListener<RowUi>() {
			@Override
			public void onChanged(Change<? extends RowUi> c) {
				updateListUi();
				updateUiValue();
			}
		});
		
		tableLayout = new GridPane();
		tableLayout.setPadding(new Insets(0, 0, 4, 0));
		tableLayout.setHgap(4);
		
		addRowButton = new Button("Add row");
		addRowButton.setOnAction(e -> {
			rowUiList.add(new RowUi(parameter.newRow(), collumns));
		});
		
		Pane spacerPane = new Pane();
		VBox.setVgrow(spacerPane, Priority.ALWAYS);
		
		tablePane = new VBox(tableLayout, spacerPane, addRowButton);
		tablePane.setPadding(new Insets(4));
		
		tableScrolPane = new ScrollPane(tablePane);
		tableScrolPane.setPrefHeight(300);
		tableScrolPane.setFitToHeight(true);
		tableScrolPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		tableScrolPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		
		//Build gridLayout
		updateListUi();
	}
	
	private void updateListUi() {
		tableLayout.getChildren().clear();
		
		// Add column labels
		int k = 0;
		for (TableColumn<P, ?> col: collumns) {
			Label columnLabel = new Label(col.getDisplayName());
			columnLabel.setPadding(new Insets(5));
			columnLabel.setStyle("-fx-font-weight: bold");
			GridPane.setHalignment(columnLabel, HPos.CENTER);
			tableLayout.add(columnLabel, ++k, 0);
		}
		
		// Add rows
		int i = 0;
		for (RowUi e: rowUiList) {
			int j = 0;
			tableLayout.add(new Label(String.valueOf(i)), j++, i+1);
			
			for (Control c: e.columnControlMap.values()) {
				GridPane.setHalignment(c, HPos.CENTER);
				tableLayout.add(c, j++, i+1);
			}
		
			tableLayout.add(e.removeButton, j++, i+1);
			
			if (i++ == lastRemovedListIndex) {
				e.removeButton.requestFocus();
			}
		}
		
		lastRemovedListIndex = -1;
	}
	
	private void updateUiValue() {
		uiValue = new ArrayList<P>();
		for (RowUi e: rowUiList) {
			uiValue.add(e.getRowValue());
		}
		
		notifyChangeHandlers();
	}

	@Override
	public void setDisable(boolean disable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(tableScrolPane, 1, row, 2, 1);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public ScrollPane getTable() {
		return tableScrolPane;
	}
	
	private class RowUi {
		Map<TableColumn<P, ?>, Control> columnControlMap;
		Button removeButton;
		
		public RowUi(P row, List<TableColumn<P, ?>> columns) {
			columnControlMap = new LinkedHashMap<TableColumn<P, ?>, Control>();
			for (TableColumn<P, ?> column: columns) {
				
				Control newControl = column.getNewUiControlFromRow(row, () -> {
					updateUiValue();
				});
				
				columnControlMap.put(column, newControl);
			}
			
			removeButton = new Button("Remove");
			removeButton.setOnAction(e -> {
				lastRemovedListIndex = rowUiList.indexOf(this);
				rowUiList.remove(this);
			});
		}
		
		public P getRowValue() {
			P row = tableParameter.newRow();
			for (Entry<TableColumn<P, ?>, Control> e: columnControlMap.entrySet()) {
				e.getKey().setColumnValueFromUiControl(row, e.getValue());
			}
			return row;
		}
	}
}
