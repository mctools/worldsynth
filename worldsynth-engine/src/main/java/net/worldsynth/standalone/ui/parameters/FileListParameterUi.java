/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.parameter.AbstractParameter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileListParameterUi extends ParameterUiElement<List<File>> {

	private Label nameLabel;
	private ListView<File> objectListView;
	private Button addObjectsButton = new Button("Add");
	private Button removeObjectsButton = new Button("Remove");

	public FileListParameterUi(AbstractParameter<List<File>> parameter) {
		super(parameter);

		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}

		objectListView = new ListView<>(FXCollections.observableArrayList(parameter.getValue()));
		objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		addObjectsButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			ArrayList<String> allExtensions = new ArrayList<String>();
			for (CustomObjectFormat format : CustomObjectFormatRegistry.getFormats()) {
				allExtensions.add("*." + format.formatSuffix());
			}
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("All custom objects", allExtensions));
			for (CustomObjectFormat format : CustomObjectFormatRegistry.getFormats()) {
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(format.formatName(), "*." + format.formatSuffix()));
			}

			List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
			if (selectedFiles != null) {
				objectListView.getItems().addAll(selectedFiles);

				uiValue = new ArrayList<>(objectListView.getItems());
			}
		});

		removeObjectsButton.setOnAction(e -> {
			objectListView.getItems().removeAll(objectListView.getSelectionModel().getSelectedItems());

			uiValue = new ArrayList<>(objectListView.getItems());
		});
	}

	@Override
	public void setDisable(boolean disable) {

	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		GridPane listPane = new GridPane();

		objectListView.setPrefSize(600, 400);
		GridPane.setColumnSpan(objectListView, 3);
		listPane.add(objectListView, 0, 0);

		listPane.add(addObjectsButton, 0, 1);
		listPane.add(removeObjectsButton, 1, 1);

		pane.add(nameLabel, 0, row);
		pane.add(listPane, 1, row, 2, 1);
	}
}
