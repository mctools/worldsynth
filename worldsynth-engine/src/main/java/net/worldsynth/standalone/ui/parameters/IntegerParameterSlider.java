/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.IntegerParameter;

public class IntegerParameterSlider extends ParameterUiElement<Integer> {
	
	private Label nameLabel;
	private Slider parameterSlider;
	private TextField parameterField;
	
	private boolean adjusting = false;
	
	public IntegerParameterSlider(IntegerParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Slider
		parameterSlider = new Slider(parameter.getSliderMin(), parameter.getSliderMax(), uiValue);
		parameterSlider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (adjusting) {
					return;
				}
				
				adjusting = true;
				uiValue = (int) Math.round(parameterSlider.getValue());
				parameterField.setText(String.valueOf(uiValue));
				parameterField.setStyle(null);
				adjusting = false;
				notifyChangeHandlers();
			}
		});
		
		//Field
		parameterField = new TextField(String.valueOf(uiValue));
		parameterField.setPrefColumnCount(10);
		parameterField.setTooltip(new Tooltip("Range: " + parameter.getMin() + " - " + parameter.getMax()));
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (adjusting) {
					return;
				}
				
				adjusting = true;
				String text = parameterField.getText();
				try {
					int v = Integer.parseInt(text);
					if (v < parameter.getMin() || v > parameter.getMax()) throw new Exception("Value is out of range");
					uiValue = v;
					parameterSlider.setValue(uiValue);
					parameterField.setStyle(null);
					
				} catch (Exception exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
				adjusting = false;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterSlider.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterSlider, 1, row);
		pane.add(parameterField, 2, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public Slider getSlider() {
		return parameterSlider;
	}

	public TextField getField() {
		return parameterField;
	}
}
