/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import net.worldsynth.parameter.EnumParameter;

public class EnumParameterDropdownSelector<E extends Enum<E>> extends ParameterUiElement<E> {
	
	private Label nameLabel;
	private ComboBox<E> parameterDropdownSelector;
	
	public EnumParameterDropdownSelector(EnumParameter<E> parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Dropdown selector
		parameterDropdownSelector = new ComboBox<E>();
		for (E par: parameter.getSelectables()) {
			parameterDropdownSelector.getItems().add(par);
		}
		parameterDropdownSelector.setMaxWidth(Double.MAX_VALUE);
		parameterDropdownSelector.getSelectionModel().select(uiValue);
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<E>() {

			@Override
			public void changed(ObservableValue<? extends E> observable, E oldValue, E newValue) {
				uiValue = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public ComboBox<E> getDropdown() {
		return parameterDropdownSelector;
	}
}
