/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import java.io.File;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import net.worldsynth.parameter.FileParameter;

public class FileParameterField extends ParameterUiElement<File> {
	private Label nameLabel;
	private TextField parameterField;
	private Button directoryDialogButton;
	
	public FileParameterField(FileParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Field
		parameterField = new TextField(String.valueOf(uiValue));
		parameterField.setPrefColumnCount(50);
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				boolean validValue = false;
				File newParameterFile = new File(newValue);
				if (parameter.isDirectory()) {
					//Validate directory
					if (newParameterFile.isDirectory()) {
						validValue = true;
					}
				}
				else {
					//Validate file
					if (!newParameterFile.isDirectory()) {
						if (parameter.getFilter() != null) {
							for (String ext: parameter.getFilter().getExtensions()) {
								ext = ext.replace("*", "");
								if (newParameterFile.getAbsolutePath().endsWith(ext)) {
									validValue = true;
									break;
								}
							}
						}
					}
				}
				
				if (validValue) {
					parameterField.setStyle(null);
					uiValue = newParameterFile;
					notifyChangeHandlers();
				}
				else {
					parameterField.setStyle("-fx-background-color: RED;");
				}
			}
		});
		
		//Button
		directoryDialogButton = new Button("...");
		directoryDialogButton.setOnAction(e -> {
			if (parameter.isDirectory()) {
				DirectoryChooser directoryChooserDialog = new DirectoryChooser();
				if (uiValue != null) {
					if (uiValue.exists()) {
						directoryChooserDialog.setInitialDirectory(uiValue);
					}
				}
				
				File returnVal = directoryChooserDialog.showDialog(parameterField.getScene().getWindow());
				if (returnVal != null) {
					uiValue = returnVal;
					parameterField.setText(uiValue.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
			else {
				FileChooser fileChooserDialog = new FileChooser();
				if (uiValue != null) {
					if (uiValue.exists()) {
						fileChooserDialog.setInitialDirectory(uiValue.getParentFile());
						fileChooserDialog.setInitialFileName(uiValue.getName());
						
					}
				}
				
				if (parameter.getFilter() != null) {
					fileChooserDialog.getExtensionFilters().add(parameter.getFilter());
				}
				
				File returnVal = null;
				if (parameter.isSave()) {
					returnVal = fileChooserDialog.showSaveDialog(parameterField.getScene().getWindow());
				}
				else {
					returnVal = fileChooserDialog.showOpenDialog(parameterField.getScene().getWindow());
				}
				
				if (returnVal != null) {
					uiValue = returnVal;
					parameterField.setText(uiValue.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterField.setDisable(disable);
		directoryDialogButton.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterField, 1, row);
		pane.add(directoryDialogButton, 2, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public TextField getField() {
		return parameterField;
	}

	public Button getDialogButton() {
		return directoryDialogButton;
	}
}
