/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

public class PreviewPreferences extends Preferences {

	public static Preference<Integer> resolution = new Preference<Integer>(256, Integer.class,
			"net.worldsynth.patcher.preview.resolution",
			"Resolution", "Build",
			"The max resolution in the horizontal directions to build the preview at");
	
	public PreviewPreferences() {
		super("Preview");
		
		registerPreference(resolution);
	}
}
