/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

public class PatchEditorPreferences extends Preferences {
	
	public static Preference<ConnectorStyle> connectorStyle = new EnumPreference<>(ConnectorStyle.BEZIER, ConnectorStyle.class,
			"net.worldsynth.patcher.patcheditor.connectorStyle",
			"Connector style", "View",
			"Style to use for rendering module connectors");
	
	public static Preference<Boolean> showGrid = new Preference<Boolean>(true, Boolean.class,
			"net.worldsynth.patcher.patcheditor.showGrid",
			"Show grid", "View",
			"Show background grid");
	
	public PatchEditorPreferences() {
		super("Patche editor");
		
		registerPreference(connectorStyle);
		registerPreference(showGrid);
	}
	
	public enum ConnectorStyle {
		STRAIGHT,
		ANGULAR,
		SQUARE,
		BEZIER,
		METRO
	}
}
