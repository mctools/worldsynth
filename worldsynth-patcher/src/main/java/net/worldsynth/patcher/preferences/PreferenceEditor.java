/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

import java.util.List;

import org.controlsfx.control.PropertySheet;
import org.controlsfx.control.PropertySheet.Item;

import com.sun.javafx.collections.ObservableListWrapper;

import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.standalone.ui.stage.UtilityStage;

public class PreferenceEditor extends UtilityStage {
	
	private final SplitPane splitPane;
	
	public PreferenceEditor(List<Preferences> prefs) {
		super();
		
		TreeItem<Preferences> rootItem = new TreeItem<Preferences>();
		addChildPrefs(rootItem, prefs);
		rootItem.setExpanded(true);
		
		TreeView<Preferences> treeView = new TreeView<Preferences>(rootItem);
		treeView.setShowRoot(false);
		
		splitPane = new SplitPane(treeView, new PropertySheet());
		splitPane.setPrefSize(800, 600);
		splitPane.setDividerPositions(0.3);
		
		treeView.getSelectionModel().selectedItemProperty().addListener((ob, ov, nv) -> {
			splitPane.getItems().set(1, getPropSheet(nv.getValue()));
		});
		
		treeView.getSelectionModel().select(0);
		
		Scene scene = new Scene(splitPane);
		scene.getStylesheets().add(WorldSynthPatcher.stylesheet);
		setScene(scene);
		
		setTitle("Preferences");
		setResizable(true);
	}
	
	private PropertySheet getPropSheet(Preferences pref) {
		List<Item> items = pref.getPreferenceItems();
		return new PropertySheet(new ObservableListWrapper<Item>(items));
	}
	
	private void addChildPrefs(TreeItem<Preferences> parent, List<Preferences> prefs) {
		for (Preferences pref: prefs) {
			TreeItem<Preferences> prefItem = new TreeItem<Preferences>(pref);
			if (pref.getSubPreferences() != null) {
				addChildPrefs(prefItem, pref.getSubPreferences());
			}
			parent.getChildren().add(prefItem);
			prefItem.setExpanded(true);
		}
	}
}
