/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

public class PatcherPreferences extends Preferences {
	
	public static Preference<Boolean> showWelcomeSplashOnStartup = new Preference<Boolean>(true, Boolean.class,
			"net.worldsynth.patcher.showWelcomeSplashOnStartup",
			"Show welcome splash on startup", "Splash",
			null);
	
	public PatcherPreferences() {
		super("Patcher", new PatchEditorPreferences(), new PreviewPreferences());
		
		registerPreference(showWelcomeSplashOnStartup);
	}
	
	
}
