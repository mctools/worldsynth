/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.PropertySheet.Item;

import javafx.beans.value.ObservableValue;
import net.worldsynth.patcher.data.PreferenceData;

public class Preference<T> {
	
	public static final Logger LOGGER = LogManager.getLogger(Preference.class);
	
	private final String pathName;
	private final String displayName;
	private final String category;
	private final String description;

	private Class<T> valueClass;
	private T value;
	
	private PreferenceData prefData;
	
	public Preference(T defaultValue, Class<T> valueClass, String pathName, String displayName, String category, String description) {
		this.value = defaultValue;
		
		this.valueClass = valueClass;
		
		this.pathName = pathName;
		this.displayName = displayName;
		this.category = category;
		this.description = description;
	}
	
	public final void setPrefData(PreferenceData prefData) {
		this.prefData = prefData;
		
		if (prefData.getPreferenceMap().containsKey(pathName)) {
			try {
				value = castDeserializedValue(prefData.getPreferenceMap().get(pathName));
				LOGGER.info("Parsed preference: " + pathName);
			} catch (ClassCastException e) {
				LOGGER.error("Could not parse preference: " + pathName, e);
			}
		}
		else {
			prefData.getPreferenceMap().put(pathName, value);
		}
	}
	
	protected final Class<T> getValueClass() {
		return valueClass;
	}
	
	@SuppressWarnings("unchecked")
	protected T castDeserializedValue(Object o) throws ClassCastException {
		if (getValueClass().isAssignableFrom(o.getClass())) {
			return (T) o;
		}
		throw new ClassCastException("Could not cast value: \"" + o + "\" to type " + getValueClass());
	}
	
	public String getPathName() {
		return pathName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getDescription() {
		return description;
	}
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
		
		if (prefData != null) {
			prefData.getPreferenceMap().put(pathName, value);
			prefData.save();
		}
	}
	
	public Item asItem() {
		return new Item() {
			@Override
			public Class<?> getType() {
				return value.getClass();
			}
			
			@Override
			public String getCategory() {
				return category;
			}
			
			@Override
			public String getName() {
				return displayName;
			}
			
			@Override
			public String getDescription() {
				return description;
			}
			
			@Override
			public Object getValue() {
				return Preference.this.getValue();
			}
			
			@SuppressWarnings("unchecked")
			@Override
			public void setValue(Object value) {
				Preference.this.setValue((T) value);
			}
			
			@Override
			public Optional<ObservableValue<? extends Object>> getObservableValue() {
				return Optional.empty();
			}
		};
	}
}
