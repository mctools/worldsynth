/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.standalone.ui.stage.ModuleNameEditorStage;

public class ModuleEditContextMenu extends ContextMenu {
	
	public ModuleEditContextMenu(ModuleWrapper moduleWrapper, PatchEditorPane patchEditor) {
		
		patchEditor.setOnMousePressed(e -> {
			hide();
		});
		
		// Parameters
		MenuItem openModuleUI = new MenuItem("Parameters");
		openModuleUI.setOnAction(e -> {
			patchEditor.openModuleParametersEditor(moduleWrapper);
		});
		getItems().add(openModuleUI);
		
		// Rename
		MenuItem moduleWrapperCustomName = new MenuItem("Rename");
		moduleWrapperCustomName.setOnAction(e -> {
			new ModuleNameEditorStage(moduleWrapper, patchEditor);
		});
		getItems().add(moduleWrapperCustomName);
		
		// Bypass
		MenuItem bypasDevice = new MenuItem("Bypass");
		bypasDevice.setDisable(!moduleWrapper.isBypassable());
		bypasDevice.setOnAction(e -> {
			patchEditor.bypassModuleWrapper(moduleWrapper, !moduleWrapper.isBypassed());
		});
		getItems().add(bypasDevice);
		
		// Delete
		MenuItem deleteModuleWrapper = new MenuItem("Delete");
		deleteModuleWrapper.setOnAction(e -> {
			patchEditor.removeModuleWrapper(moduleWrapper, true, true, true);
		});
		getItems().add(deleteModuleWrapper);
		
		// Lock preview
		boolean thisLocked = WorldSynthEditor.isPreviewLocked() && WorldSynthEditor.getCurrentPreviewWrapper() == moduleWrapper;
		MenuItem lockPreview = new MenuItem(thisLocked ? "Unlock preview" : "Lock preview");
		lockPreview.setOnAction(e -> {
			if (WorldSynthEditor.isPreviewLocked()) {
				if (WorldSynthEditor.getCurrentPreviewWrapper() == moduleWrapper) {
					WorldSynthEditor.setPreviewLocked(false);
				}
				else {
					WorldSynthEditor.setPreviewLocked(false);
					patchEditor.getEditorSession().updatePreviewWrapper(moduleWrapper);
					WorldSynthEditor.setPreviewLocked(true);
				}
			}
			else {
				patchEditor.getEditorSession().updatePreviewWrapper(moduleWrapper);
				WorldSynthEditor.setPreviewLocked(true);
			}
		});
		getItems().add(lockPreview);
	}
}
