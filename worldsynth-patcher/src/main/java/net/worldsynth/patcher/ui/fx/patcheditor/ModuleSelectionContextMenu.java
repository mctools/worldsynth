/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.WorldSynth;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.patcher.data.PatcherDataManager;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;

public class ModuleSelectionContextMenu extends ContextMenu {
	private static Logger logger = LogManager.getLogger(ModuleSelectionContextMenu.class);
	
	private final PatchEditorPane patchEditor;
	
	public ModuleSelectionContextMenu(PatchEditorPane patchEditor) {
		this.patchEditor = patchEditor;
		
		patchEditor.setOnMousePressed(e -> {
			hide();
		});
		
		// Synth
		getItems().add(new SynthImportMenuItem());
		getItems().add(new SeparatorMenuItem());
		
		// Build the module menu structure
		ModuleMenuStructure structure = new ModuleMenuStructure("root");
		for (ModuleEntry moduleEntry: WorldSynth.moduleRegister.getRegisteredModuleEntries()) {
			structure.addModuleToStructure(moduleEntry);
		}
		
		// Convert the module menu structure to JMenu
		for (ModuleMenuStructure item: structure.subStructures) {
			MenuItem newRootItem = convertStructureToMenuItem(item);
			getItems().add(newRootItem);
		}
	}
	
	private MenuItem convertStructureToMenuItem(ModuleMenuStructure structure) {
		MenuItem item;
		
		// If item is a module
		if (structure.module != null) {
			item = new MenuItem(structure.itemName);
			item.setOnAction(e -> {
				try {
					PatcherDataManager.getWorkspaceData().incrementModuleUsage(structure.module);
					patchEditor.setTempWrapper(new ModuleWrapper(structure.module, patchEditor.getPatch(), 0.0, 0.0));
				} catch (Exception ex) {
					logger.error(ex);
				}
			});
			
			return item;
		}
		
		// Else item is a parent item
		else {
			item = new Menu(structure.itemName);
			for (ModuleMenuStructure subStructure: structure.subStructures) {
				MenuItem subItem = convertStructureToMenuItem(subStructure);
				((Menu) item).getItems().add(subItem);
			}
			return item;
		}
	}
	
	private class ModuleMenuStructure {
		ArrayList<ModuleMenuStructure> subStructures = new ArrayList<ModuleMenuStructure>();
		
		Class<? extends AbstractModule> module = null;
		String itemName = null;
		
		public ModuleMenuStructure(String itemName) {
			this.itemName = itemName;
		}

		public ModuleMenuStructure(Class<? extends AbstractModule> module, String moduleName) {
			this.module = module;
			this.itemName = moduleName;
		}
		
		private void addModuleToStructure(Class<? extends AbstractModule> moduleClass, String menuPath, String moduleName) {
			if (menuPath.startsWith("\\")) {
				menuPath = menuPath.replaceFirst("\\\\", "");
				String subStructureName = menuPath.split("\\\\", 2)[0];
				menuPath = menuPath.replaceFirst(subStructureName, "");
				
				for (ModuleMenuStructure subStructure: subStructures) {
					if (subStructure.itemName.equals(subStructureName)) {
						subStructure.addModuleToStructure(moduleClass, menuPath, moduleName);
						return;
					}
				}
				
				ModuleMenuStructure newSubItem = new ModuleMenuStructure(subStructureName);
				newSubItem.addModuleToStructure(moduleClass, menuPath, moduleName);
				subStructures.add(newSubItem);
				return;
			}
			else {
				subStructures.add(new ModuleMenuStructure(moduleClass, moduleName));
			}
		}
		
		public void addModuleToStructure(ModuleEntry entry) {
			String menuPath = entry.getModuleMenuPath();
			String moduleName = entry.getModuleName();
			Class<? extends AbstractModule> moduleClass = entry.getModuleClass();
			
			addModuleToStructure(moduleClass, menuPath, moduleName);
		}
	}
	
	private class SynthImportMenuItem extends Menu {

		public SynthImportMenuItem() {
			super("Import");
			
			getItems().add(new BlueprintItem());
		}
	}
	
	private class BlueprintItem extends MenuItem {
		
		public BlueprintItem() {
			super("Blueprint");
			
			setOnAction(e -> {
				File synthFile = getSynthFile();
				if (synthFile == null) {
					return;
				}
				else if (!synthFile.exists()) {
					return;
				}
				
				
				try {
					Synth blueprintSynth = SynthManager.readUnmanagedSynthFromFile(synthFile);
					patchEditor.setTempPatch(new TempPatch(blueprintSynth.getPatch().toJson(), patchEditor.getPatch()));
				} catch (IOException ex) {
					logger.error(ex);
				}
			});
		}
	}
	
	private File getSynthFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));
		
		File synthFile = fileChooser.showOpenDialog(WorldSynthPatcher.primaryStage);
		
		return synthFile;
	}
}