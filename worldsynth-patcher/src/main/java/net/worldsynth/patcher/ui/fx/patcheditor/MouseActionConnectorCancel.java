/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane.MouseListener;

class MouseActionConnectorCancel implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_CLICKED, // Mouse event type
			MouseButton.SECONDARY, // Mouse button
			1, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.YES, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		editor.tempConnector = null;
		t.dragConnector = false;

		editor.repaint();

		if (t.replacingConnector != null) {
			t.replacingConnector = null;
			editor.getEditorSession().updatePreview();
		}
	}
}