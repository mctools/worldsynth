/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patcher.preferences.PatchEditorPreferences;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.navcanvas.Pixel;

/**
 * This class contains various methods used in relations to connectors in the
 * patch editor.
 */
class Connectors {
	
	/**
	 * Paints the connector in the given editor with the graphics context.
	 * 
	 * @param editor
	 * @param g
	 * @param connector
	 */
	public static void paintConnector(PatchEditorPane editor, GraphicsContext g, ModuleConnector connector) {
		double startX = 0;
		double startY = 0;
		double endX = 0;
		double endY = 0;
		
		if (connector.module1 != null && connector.module1Io != null) {
			// Connector is connected to the output of a wrapper
			double x = connector.module1.posX + connector.module1Io.posX;
			double y = connector.module1.posY + connector.module1Io.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			startX = pixel.x;
			startY = pixel.y;
		}
		else if (editor.wrapperIoOver != null) {
			// An IO has active hover over in the editor
			double x = editor.wrapperOver.posX + editor.wrapperIoOver.posX;
			double y = editor.wrapperOver.posY + editor.wrapperIoOver.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			startX = pixel.x;
			startY = pixel.y;
		}
		else {
			// Draw the connector to the mouse pointer
			startX = editor.mouseListener.currentEvent.getX();
			startY = editor.mouseListener.currentEvent.getY();
		}
		
		if (connector.module2 != null && connector.module2Io != null) {
			// Connector is connected to the input of a wrapper
			double x = connector.module2.posX + connector.module2Io.posX;
			double y = connector.module2.posY + connector.module2Io.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			endX = pixel.x;
			endY = pixel.y;
		}
		else if (editor.wrapperIoOver != null) {
			// An IO has active hover over in the editor
			double x = editor.wrapperOver.posX + editor.wrapperIoOver.posX;
			double y = editor.wrapperOver.posY + editor.wrapperIoOver.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			endX = pixel.x;
			endY = pixel.y;
		}
		else {
			// Draw the connector to the mouse pointer
			endX = editor.mouseListener.currentEvent.getX();
			endY = editor.mouseListener.currentEvent.getY();
		}
		
		paintConnector(editor, g, startX, startY, endX, endY);
	}
	
	private static void paintConnector(PatchEditorPane editor, GraphicsContext g, double startX, double startY, double endX, double endY) {
		g.setLineCap(StrokeLineCap.ROUND);
		g.setStroke(Color.DARKGRAY);
		g.setLineWidth(2*editor.zoom);
		
		switch (PatchEditorPreferences.connectorStyle.getValue()) {
			case STRAIGHT:
				g.strokeLine(startX, startY, endX, endY);
				break;
			case ANGULAR:
				g.strokeLine(startX, startY, startX+30*editor.zoom, startY);
				g.strokeLine(startX+30*editor.zoom, startY, endX-30*editor.zoom, endY);
				g.strokeLine(endX-30*editor.zoom, endY, endX, endY);
				break;
			case SQUARE:
				double midpointX = (startX + endX) / 2.0;
				g.strokeLine(startX, startY, midpointX, startY);
				g.strokeLine(midpointX, startY, midpointX, endY);
				g.strokeLine(midpointX, endY, endX, endY);
				break;
			case BEZIER:
				double x1 = startX, x2 = Math.max(startX + 50.0*editor.zoom, startX + (endX-startX)/2.0), x3 = Math.min(endX - 50.0*editor.zoom, endX - (endX-startX)/2.0), x4 = endX;
				double y1 = startY, y2 = startY, y3 = endY, y4 = endY;
				
				double x = x1, y = y1;
				for (double b = 0.0; b < 1.0; b += 0.05) {
					double nx = p(x1, x2, x3, x4, b+0.05);
					double ny = p(y1, y2, y3, y4, b+0.05);
					g.strokeLine(x, y, nx, ny);
					x = nx;
					y = ny;
				}
				break;
			case METRO:
				midpointX = (startX + endX) / 2.0;
				double diffX = endX - startX;
				double diffY = endY - startY;
				double signY = Math.signum(diffY);
				double diagonal = Math.min(Math.abs(diffX), Math.abs(diffY));
				if (diffX > 0) {
					if (diffX > diagonal) {
						g.strokeLine(startX, startY, midpointX-diagonal/2, startY);
						g.strokeLine(midpointX-diagonal/2, startY, midpointX+diagonal/2, endY);
						g.strokeLine(midpointX+diagonal/2, endY, endX, endY);
					}
					else {
						g.strokeLine(midpointX-diagonal/2, startY, midpointX, startY+signY*diagonal/2);
						g.strokeLine(midpointX, startY+signY*diagonal/2, midpointX, endY-signY*diagonal/2);
						g.strokeLine(midpointX, endY-signY*diagonal/2, midpointX+diagonal/2, endY);
					}
				}
				else {
					g.strokeLine(startX, startY, startX-diagonal/2, startY+signY*diagonal/2);
					g.strokeLine(startX-diagonal/2, startY+signY*diagonal/2, endX+diagonal/2, endY-signY*diagonal/2);
					g.strokeLine(endX+diagonal/2, endY-signY*diagonal/2, endX, endY);
				}
				break;
			default:
				g.strokeLine(startX, startY, endX, endY);
		}
	}
	
	/**
	 * For bezier connectors
	 */
	private static double p(double a1, double a2, double a3, double a4, double b) {
		return lerp(lerp(lerp(a1, a2, b), lerp(a2, a3, b), b), lerp(lerp(a2, a3, b), lerp(a3, a4, b), b), b);
	}
	
	/**
	 * For bezier connectors
	 */
	private static double lerp(double a1, double a2, double b) {
		return a1 + b * (a2 - a1);
	}
}
