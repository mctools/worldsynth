/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BoxBlur;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.navcanvas.Pixel;

/**
 * This class contains various methods used in relations to wrappers in the
 * patch editor.
 */
class Wrappers {
	
	/**
	 * Paints the wrapper in the given editor with the graphics context.
	 * 
	 * @param editor
	 * @param g
	 * @param wrapper
	 */
	public static void paintWrapper(PatchEditorPane editor, GraphicsContext g, ModuleWrapper wrapper) {
		float ioRenderSize = ModuleWrapperIO.IO_RENDERSIZE;
		
		Color backgroundColor = Color.web("#303030");
		
		// Module colors
		Color moduleShadowColor = backgroundColor.interpolate(Color.BLACK, 1.0). interpolate(Color.TRANSPARENT, 0.5);
		Color moduleColor = backgroundColor.interpolate(Color.WHITE, 0.2).interpolate(Color.TRANSPARENT, 0.1);
		Color moduleBypassedColor = moduleColor.interpolate(Color.RED, 0.2);
		
		Color moduleStrokeColor = Color.BLACK;
		Color moduleOverStrokeColor = Color.GRAY;
		Color moduleSelectedStrokeColor = Color.WHITE;
		
		Color ioStrokeColor = Color.BLACK;
		Color ioOverStrokeColor = Color.GRAY;
		
		Coordinate coord = new Coordinate(wrapper.posX, wrapper.posY);
		Pixel pixel = new Pixel(coord, editor);
		
		// Draw module shadow
		g.setFill(moduleShadowColor);
		g.setEffect(new BoxBlur(10.0, 10.0, 2));
		g.fillRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*editor.zoom, wrapper.wrapperHeight*editor.zoom, 10.0*editor.zoom, 10.0*editor.zoom);
		g.setEffect(null);
		
		// Draw module selection
		g.setLineWidth(1.0);
		if (wrapper == WorldSynthEditor.getCurrentPreviewWrapper()) {
			if (WorldSynthEditor.isPreviewLocked()) {
				g.setFill(Color.YELLOW);
			}
			else {
				g.setFill(Color.WHITE);
			}
			g.setEffect(new BoxBlur(10.0*editor.zoom, 10.0*editor.zoom, 3));
			g.fillRoundRect(pixel.x-2.0*editor.zoom, pixel.y-2.0*editor.zoom, (wrapper.wrapperWidth+4.0)*editor.zoom, (wrapper.wrapperHeight+4.0)*editor.zoom, 12.0*editor.zoom, 12.0*editor.zoom);
			g.setEffect(null);
		}
		
		
		// Draw main module rectangle;
		if (wrapper.isBypassed()) {
			g.setFill(moduleBypassedColor);
		}
		else {
			g.setFill(moduleColor);
		}
		g.fillRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*editor.zoom, wrapper.wrapperHeight*editor.zoom, 10.0*editor.zoom, 10.0*editor.zoom);
		g.setFill(wrapper.module.getModuleColor());
		g.fillRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*editor.zoom, 10.0*editor.zoom, 10.0*editor.zoom, 10.0*editor.zoom);
		g.fillRect(pixel.x, pixel.y + 5.0*editor.zoom, wrapper.wrapperWidth*editor.zoom, 10.0*editor.zoom);
		
		g.setLineWidth(1.0);
		if (editor.selectedWrappers.contains(wrapper)) {
			g.setLineWidth(2.0);
			g.setStroke(moduleSelectedStrokeColor);
		}
		else if (editor.wrapperOver == wrapper && editor.wrapperIoOver == null) {
			g.setStroke(moduleOverStrokeColor);
		}
		else {
			g.setStroke(moduleStrokeColor);
		}
		g.strokeRoundRect(pixel.x, pixel.y, wrapper.wrapperWidth*editor.zoom, wrapper.wrapperHeight*editor.zoom, 10.0*editor.zoom, 10.0*editor.zoom);

		// Draw cache indication dots
		g.setStroke(Color.BLACK);
		g.setFill(Color.WHITE);
		g.setLineWidth(editor.zoom*2);
		for (int i = 0; i < editor.editorSession.getBuildCache().hasCachedData(wrapper); i++) {
			g.strokeOval(pixel.x + wrapper.wrapperWidth*editor.zoom - (10*i + 10.0)*editor.zoom, pixel.y + 5.0*editor.zoom, 5.0*editor.zoom, 5.0*editor.zoom);
			g.fillOval(pixel.x + wrapper.wrapperWidth*editor.zoom - (10*i + 10.0)*editor.zoom, pixel.y + 5.0*editor.zoom, 5.0*editor.zoom, 5.0*editor.zoom);
		}

		// Draw device bypass connection
		if (wrapper.isBypassed()) {
			g.setStroke(Color.RED);
			g.setLineWidth(editor.zoom*2);
			// Draw line between bypass input and main output
			ModuleWrapperIO bypassInput = wrapper.getWrapperIoByModuleIo(wrapper.module.getBypassInput());
			Pixel mi = new Pixel(new Coordinate(wrapper.posX, wrapper.posY + bypassInput.posY), editor);
			Pixel mo = new Pixel(new Coordinate(wrapper.posX + wrapper.wrapperWidth, wrapper.posY + 25.0), editor);
			g.strokeLine(mi.x, mi.y, mi.x + 10.0*editor.zoom, mi.y);
			g.strokeLine(mi.x + 10.0*editor.zoom, mi.y, mo.x - 10.0*editor.zoom, mo.y);
			g.strokeLine(mo.x - 10.0*editor.zoom, mo.y, mo.x, mo.y);
		}
		
		// Draw module inputs
		if (wrapper.module.getInputs() != null) {
			for (ModuleWrapperIO io: wrapper.wrapperInputs.values()) {
				double x = pixel.x + (io.posX-ioRenderSize/2.0)*editor.zoom;
				double y = pixel.y + (io.posY-ioRenderSize/2.0)*editor.zoom;
				double d = ioRenderSize * editor.zoom;
				
				if (!io.getIO().isVisible()) {
					continue;
				}
				else if (io.getDatatype() instanceof DatatypeMultitype) {
					// Draw multitype io with several colors in several vertical stripes
					DatatypeMultitype multitype = (DatatypeMultitype) io.getDatatype();
					int typeCount = multitype.getDatatypes().length;
					for (int i = 0; i < typeCount; i++) {
						g.setFill(multitype.getDatatypes()[i].getDatatypeColor());
						if (io == editor.wrapperIoOver) {
							g.setFill(multitype.getDatatypes()[i].getDatatypeColor().darker());
						}
						g.fillArc(x, y, d, d, i*360.0/typeCount, 360.0/typeCount, ArcType.ROUND);
					}
				}
				else {
					// Draw normal io with one color
					g.setFill(io.getDatatype().getDatatypeColor());
					if (io == editor.wrapperIoOver) {
						g.setFill(io.getDatatype().getDatatypeColor().darker());
					}
					g.fillOval(x, y, d, d);
				}
				
				g.setLineWidth(1);
				g.setStroke(io == editor.wrapperIoOver ? ioOverStrokeColor : ioStrokeColor);
				g.strokeOval(x, y, d, d);
				
				g.setFill(Color.WHITE);
				g.setTextAlign(TextAlignment.LEFT);
				g.setTextBaseline(VPos.CENTER);
				g.setFont(new Font("SansSerif", 10.0*editor.zoom));
				g.fillText(io.getName(), x + 15*editor.zoom, y + 5*editor.zoom);
			}
		}
		
		// Draw module outputs
		if (wrapper.module.getOutputs() != null) {
			for (ModuleWrapperIO io: wrapper.wrapperOutputs.values()) {
				double x = pixel.x + (io.posX-ioRenderSize/2.0)*editor.zoom;
				double y = pixel.y + (io.posY-ioRenderSize/2.0)*editor.zoom;
				double d = ioRenderSize * editor.zoom;
				
				if (!io.getIO().isVisible()) {
					continue;
				}
				else {
					// Draw normal io with one color
					g.setFill(io.getDatatype().getDatatypeColor());
					if (io == editor.wrapperIoOver) {
						g.setFill(io.getDatatype().getDatatypeColor().darker());
					}
					g.fillOval(x, y, d, d);
				}
				
				g.setLineWidth(1);
				g.setStroke(io == editor.wrapperIoOver ? ioOverStrokeColor : ioStrokeColor);
				g.strokeOval(x, y, d, d);
				
				g.setFill(Color.WHITE);
				g.setTextAlign(TextAlignment.RIGHT);
				g.setTextBaseline(VPos.CENTER);
				g.setFont(new Font("SansSerif", 10.0*editor.zoom));
				g.fillText(io.getName(), x - 5*editor.zoom, y + 5*editor.zoom);
			}
		}
		
		// Draw text
		g.setFill(Color.WHITE);
		g.setTextAlign(TextAlignment.LEFT);
		g.setTextBaseline(VPos.BASELINE);
		g.setFont(new Font("SansSerif", 15.0*editor.zoom));
		g.fillText(wrapper.toString(), pixel.x, pixel.y - 5*editor.zoom);
		
		// Draw a string to indicate the io name if hvering over one
		if (editor.wrapperOver == wrapper && editor.wrapperIoOver != null) {
			g.setFill(Color.GRAY);
			g.fillText(editor.wrapperIoOver.toString(), pixel.x, pixel.y + (wrapper.wrapperHeight + 15)*editor.zoom);
		}
	}
	
	public static boolean isCoordinateInsideWrapper(PatchEditorPane editor, ModuleWrapper wrapper, double xCoord, double yCoord) {
		return isCoordinateInsideWrapper(editor, wrapper, xCoord, yCoord, 0.0);
	}
	
	public static boolean isCoordinateInsideWrapper(PatchEditorPane editor, ModuleWrapper wrapper, double xCoord, double yCoord, double offset) {
		if (xCoord+offset >= wrapper.posX && xCoord-offset <= wrapper.posX+wrapper.wrapperWidth && yCoord+offset >= wrapper.posY && yCoord-offset <= wrapper.posY+wrapper.wrapperHeight) {
			return distanceToWrapper(wrapper, xCoord, yCoord) <= offset;
		}
		return false;
	}
	
	public static double distanceToWrapper(ModuleWrapper wrapper, double xCoord, double yCoord) {
		double wrapperCenterOffsetX = wrapper.posX+wrapper.wrapperWidth/2.0;
		double wrapperCenterOffsetY = wrapper.posY+wrapper.wrapperHeight/2.0;
		return Math.max(boxDist(wrapper.wrapperWidth/2.0 - 5.0, wrapper.wrapperHeight/2.0 - 5.0, xCoord-wrapperCenterOffsetX, yCoord-wrapperCenterOffsetY) - 5.0, 0.0);
	}
	
	public static boolean isCoordinateInsideIo(PatchEditorPane editor, ModuleWrapper wrapper, ModuleWrapperIO io, double xCoord, double yCoord) {
		return isCoordinateInsideIo(editor, wrapper, io, xCoord, yCoord, 0.0);
	}
	
	public static boolean isCoordinateInsideIo(PatchEditorPane editor, ModuleWrapper wrapper, ModuleWrapperIO io, double xCoord, double yCoord, double offset) {
		return distanceToIo(wrapper, io, xCoord, yCoord) <= offset;
	}
	
	public static double distanceToIo(ModuleWrapper wrapper, ModuleWrapperIO io, double xCoord, double yCoord) {
		double ioCenterOffsetX = wrapper.posX+io.posX;
		double ioCenterOffsetY = wrapper.posY+io.posY;
		return circleDist(ModuleWrapperIO.IO_RENDERSIZE/2.0, xCoord-ioCenterOffsetX, yCoord-ioCenterOffsetY);
	}
	
	private static double boxDist(double halfWidth, double halfHeight, double x, double y) {
		double a = Math.max(Math.abs(x)-halfWidth, 0.0);
		double b = Math.max(Math.abs(y)-halfHeight, 0.0);
		return Math.sqrt(a*a + b*b);
	}
	
	private static double circleDist(double radius, double x, double y) {
		return Math.sqrt(x*x + y*y) - radius;
	}
}
