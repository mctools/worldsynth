/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.extentseditor;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorSession;
import net.worldsynth.util.event.EventHandler;
import net.worldsynth.util.event.ExtentEvent;

import java.util.ArrayList;

public class ExtentsEditor extends BorderPane {

	private SynthEditorSession currentEditorSession = null;
	private WorldExtentManager currentExtentsManager = null;
	
	public WorldPreviewPane worldPreview = new WorldPreviewPane(this);
	
	private TextField valueFieldName = new TextField("");
	private WorldSynthCustomDoubleNumberField valueFieldX = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldY = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldZ = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldWidth = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldHeight = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private WorldSynthCustomDoubleNumberField valueFieldLength = new WorldSynthCustomDoubleNumberField(Double.NaN);
	private CheckBox checkBoxLockSizeRatio = new CheckBox("Lock width/length size ratio");
	private ListView<WorldExtent> extentList = new ListView<WorldExtent>();
	private int newExtentCount = 0;

	private ArrayList<EventHandler> listeners = new ArrayList<>();
	
	public ExtentsEditor() {
		//Name field events
		valueFieldName.setOnAction(e -> {
			WorldExtent currentExtent = currentExtentsManager.getCurrentWorldExtent();
			currentExtent.setName(valueFieldName.getText());
			currentExtentsManager.setCurrentWorldExtent(null);
			currentExtentsManager.setCurrentWorldExtent(currentExtent);
			
			extentList.refresh();
		});
		
		valueFieldName.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			if (newValue == false) {
				//Reset the name in the name field if the field losses focus without the user applying the new name by hitting enter
				valueFieldName.setText(currentExtentsManager.getCurrentWorldExtent().getName());
			}
		});
		
		//Create button for adding new extent to the list, and set the action handler for it to add the new extent.
		Button addExtentButton = new Button("Add extent");
		addExtentButton.setOnAction(e -> {
			WorldExtent newExtent = new WorldExtent("Unnamed extent " + newExtentCount++, -128, -128, 256);
			currentExtentsManager.addWorldExtent(newExtent);
			currentExtentsManager.setCurrentWorldExtent(newExtent);
			getCurrentEditorSession().commitHistory();
		});
		
		//Create button for removing new extent to the list, and set the action handler for it to remove the current extent.
		Button removeExtentButton = new Button("Remove extent");
		removeExtentButton.setOnAction(e -> {
			currentExtentsManager.removeWorldExtent(currentExtentsManager.getCurrentWorldExtent());
			getCurrentEditorSession().commitHistory();
		});
		
		//All editable fields are disabled until an extent has been bound to them trough bindNewExtent(WorldExtent, WorldExtent)
		disableAllFields(true);
		
		//Build the UI, adding all components of it.
		GridPane extentsPane = new GridPane();
		extentsPane.setPadding(new Insets(10.0));
		extentsPane.setHgap(10.0);
		extentsPane.setVgap(5.0);
		
		Label extentLabel = new Label("Current extent:");
		GridPane.setColumnSpan(extentLabel, 2);
		extentsPane.add(extentLabel, 0, 0);
		GridPane.setColumnSpan(valueFieldName, 2);
		extentsPane.add(valueFieldName, 0, 1);
		
		Label extentXLabel = new Label("X:");
		extentsPane.add(extentXLabel, 0, 2);
		extentsPane.add(valueFieldX, 1, 2);
		
		Label extentYLabel = new Label("Y:");
		extentsPane.add(extentYLabel, 0, 3);
		extentsPane.add(valueFieldY, 1, 3);
		
		Label extentZLabel = new Label("Z:");
		extentsPane.add(extentZLabel, 0, 4);
		extentsPane.add(valueFieldZ, 1, 4);
		
		GridPane.setColumnSpan(checkBoxLockSizeRatio, 2);
		extentsPane.add(checkBoxLockSizeRatio, 0, 5);
		
		Label extentWidthLabel = new Label("Width:");
		extentsPane.add(extentWidthLabel, 0, 6);
		extentsPane.add(valueFieldWidth, 1, 6);
		
		Label extentHeightLabel = new Label("Height:");
		extentsPane.add(extentHeightLabel, 0, 7);
		extentsPane.add(valueFieldHeight, 1, 7);
		
		Label extentLengthLabel = new Label("Length:");
		extentsPane.add(extentLengthLabel, 0, 8);
		extentsPane.add(valueFieldLength, 1, 8);
		
		GridPane.setColumnSpan(extentList, 2);
		extentsPane.add(extentList, 0, 9);
		
		extentsPane.add(addExtentButton, 0, 10);
		extentsPane.add(removeExtentButton, 1, 10);
		
		setLeft(extentsPane);
		setCenter(worldPreview);


		valueFieldName.textProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setName(nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
		});
		valueFieldX.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setX((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		valueFieldY.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setY((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		valueFieldZ.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setZ((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		valueFieldWidth.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setWidth((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		valueFieldHeight.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setHeight((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		valueFieldLength.valueProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setLength((double) nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
			WorldSynthEditor.updatePreview(false);
		});
		checkBoxLockSizeRatio.selectedProperty().addListener((ob, ov, nv) -> {
			if (programIsEditingFields) return;
			getCurrentExtentManager().getCurrentWorldExtent().setSizeRatioLock(nv);
			getCurrentEditorSession().commitHistory();
			getCurrentEditorSession().registerUnsavedChangePerformed();
		});
	}

	private boolean programIsEditingFields = false;
	private void disableAllFields(boolean value) {
		valueFieldName.setDisable(value);
		valueFieldX.setDisable(value);
		valueFieldY.setDisable(value);
		valueFieldZ.setDisable(value);
		checkBoxLockSizeRatio.setDisable(value);
		valueFieldWidth.setDisable(value);
		valueFieldHeight.setDisable(value);
		valueFieldLength.setDisable(value);
	}

	private void clearAllFields() {
		programIsEditingFields = true;

		valueFieldName.setText("");
		valueFieldX.setValue(Double.NaN);
		valueFieldY.setValue(Double.NaN);
		valueFieldZ.setValue(Double.NaN);
		valueFieldWidth.setValue(Double.NaN);
		valueFieldHeight.setValue(Double.NaN);
		valueFieldLength.setValue(Double.NaN);
		checkBoxLockSizeRatio.setSelected(false);

		programIsEditingFields = false;
	}

	private void setFieldValues(WorldExtent extent) {
		programIsEditingFields = true;

		valueFieldName.setText(extent.getName());
		valueFieldX.setValue(extent.getX());
		valueFieldY.setValue(extent.getY());
		valueFieldZ.setValue(extent.getZ());
		valueFieldWidth.setValue(extent.getWidth());
		valueFieldHeight.setValue(extent.getHeight());
		valueFieldLength.setValue(extent.getLength());
		checkBoxLockSizeRatio.setSelected(extent.getSizeRatioLock());

		programIsEditingFields = false;
	}

	/**
	 * This listener is used to listen for changes to the current extent and update the preview window in top left
	 * corner if the extent changes in position or size.
	 */
	private EventHandler<ExtentEvent> extentChangeListener = e -> {
		setFieldValues((WorldExtent) e.getSource());
	};

	/**
	 * Changes the extent bound to the UI controls for extent configuring.<br>
	 * Unbinds the old extent if there is one from the UI controls for its properties and binds the new in its place if there is one.
	 */
	private void bindNewExtent(WorldExtent oldExtent, WorldExtent newExtent) {
		if (oldExtent != null) {
			oldExtent.removeEventHandler(ExtentEvent.EXTENT_EDITED, extentChangeListener);
		}
		
		if (newExtent == null) {
			disableAllFields(true);
			clearAllFields();
		}
		else {
			disableAllFields(false);
			setFieldValues(newExtent);
			newExtent.addEventHandler(ExtentEvent.EXTENT_EDITED, extentChangeListener);
		}
	}
	
	/**
	 * This listener listens for change in the current extent in the extents manager.<br>
	 * The listener is registered to the appropriate properties inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentManagerSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		extentList.getSelectionModel().select(newValue);
		bindNewExtent(oldValue, newValue);
	};
	
	/**
	 * This listener listens for change in the selected extent in the extents list.<br>
	 * The listener is added and removed to the appropriate property inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentListSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		if (currentExtentsManager != null) {
			currentExtentsManager.setCurrentWorldExtent(newValue);
		}
	};
	
	public void setEditorSession(SynthEditorSession session) {
		extentList.getSelectionModel().selectedItemProperty().removeListener(extentListSelectionChangeListener);
		WorldExtentManager oldExtentManger = currentExtentsManager;
		if (currentExtentsManager != null) {
			currentExtentsManager.currentWorldExtentProperty().removeListener(extentManagerSelectionChangeListener);
			bindNewExtent(oldExtentManger.getCurrentWorldExtent(), null);
		}
		currentEditorSession = session;
		currentExtentsManager = session.getSynth().getExtentManager();

		if (currentExtentsManager == null) {
			extentList.setItems(FXCollections.emptyObservableList());
			extentList.getSelectionModel().clearSelection();
		}
		else {
			currentExtentsManager.currentWorldExtentProperty().addListener(extentManagerSelectionChangeListener);
			extentList.setItems(currentExtentsManager.getObservableExtentsList());
			extentList.getSelectionModel().select(currentExtentsManager.getCurrentWorldExtent());
			bindNewExtent(null, currentExtentsManager.getCurrentWorldExtent());
		}
		extentList.getSelectionModel().selectedItemProperty().addListener(extentListSelectionChangeListener);
	}
	
	public WorldExtentManager getCurrentExtentManager() {
		return currentExtentsManager;
	}

	public SynthEditorSession getCurrentEditorSession() {
		return currentEditorSession;
	}
}
