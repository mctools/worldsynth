/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomespace;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class Biomespace3DRender extends AbstractPreviewRenderCanvas {
	
	@SuppressWarnings("unused")
	private Biome[][][] biomespace;
	
	public Biomespace3DRender() {
		super();
	}
	
	public void setVoxelBiomespace(Biome[][][] biomespace) {
		this.biomespace = biomespace;
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeBiomespace castData = (DatatypeBiomespace) data;
		setVoxelBiomespace(castData.getBiomespace());
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(Color.RED);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
}
