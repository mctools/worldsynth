/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.patcheditor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TreeItem;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.preferences.PatchEditorPreferences;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.patcher.ui.fx.syntheditor.SynthEditorSession;
import net.worldsynth.patcher.ui.navcanvas.Coordinate;
import net.worldsynth.patcher.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.patcher.ui.navcanvas.Pixel;
import net.worldsynth.standalone.ui.stage.ModuleParametersStage;

public class PatchEditorPane extends Pane implements NavigationalCanvas {
	
	final SynthEditorSession editorSession;
	
	final Patch patch;
	
	private final Canvas canvas;
	private TreeItem<ModuleWrapper> moduleTree;
	ModuleSearchPopupFx moduleSearchPopup = new ModuleSearchPopupFx(this);
	
	/**
	 * The {@link ModuleWrapper} that the mouse is hovering over
	 */
	ModuleWrapper wrapperOver = null;
	/**
	 * The {@link ModuleWrapperIO} that the mouse is hovering over
	 */
	ModuleWrapperIO wrapperIoOver = null;
	/**
	 * The list of {@link ModuleWrapper}s that are currently selected
	 */
	ArrayList<ModuleWrapper> selectedWrappers = new ArrayList<ModuleWrapper>();
	
	TempPatch tempPatch = null;
	ModuleWrapper tempWrapper = null;
	ModuleConnector tempConnector = null;
	SelectionRectangle selectionRectangle = null;
	SelectionLasso selectionLasso = null;
	
	double centerCoordX = 0.0;
	double centerCoordY = 0.0;
	double zoom = 1.0;
	
	MouseListener mouseListener = new MouseListener();
	KeyboardListener keyboardListener = new KeyboardListener();
	
	public PatchEditorPane(Patch patch, SynthEditorSession editorSession) {
		this.patch = patch;
		this.editorSession = editorSession;
		
		//Setup style
		getStyleClass().add("syntheditor");
		
		//Build tree browser for the synth
		moduleTree = buildModuleTree(patch);
		
		//Build editor canvas
		canvas = new Canvas(1100, 800);
		getChildren().add(canvas);
		
		canvas.addEventHandler(MouseEvent.ANY, mouseListener);
		canvas.addEventHandler(KeyEvent.ANY, keyboardListener);
		
		canvas.addEventHandler(ScrollEvent.SCROLL, e -> {
			//Do not zoom on direct input
			if (e.isDirect()) return;
			
			double maxZoom = 5.0;
			double minZoom = 0.05;
			
			zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
			if (zoom < minZoom) zoom = minZoom;
			else if (zoom > maxZoom) zoom = maxZoom;
			repaint();
		});
		
		repaint();
	}
	
	public SynthEditorSession getEditorSession() {
		return editorSession;
	}
	
	public Patch getPatch() {
		return patch;
	}
	
	private TreeItem<ModuleWrapper> buildModuleTree(Patch patch) {
		TreeItem<ModuleWrapper> treeRoot = new TreeItem<ModuleWrapper>();
		
		for (ModuleWrapper mw: patch.getModuleWrapperList()) {
			TreeItem<ModuleWrapper> deviceItem = new TreeItem<ModuleWrapper>(mw);
			treeRoot.getChildren().add(deviceItem);
		}
		
		return treeRoot;
	}
	
	public TreeItem<ModuleWrapper> getModuleTree() {
		return moduleTree;
	}
	
	//---------------------------------------------------------------------------//
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
        repaint();
    }
	
	//---------------------------------------------------------------------------//
	
	@Override
	public double getZoom() {
		return zoom;
	}
	
	@Override
	public double getCenterCoordinateX() {
		return centerCoordX;
	}
	
	@Override
	public double getCenterCoordinateY() {
		return centerCoordY;
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- EDITS ----------------------------------//
	//---------------------------------------------------------------------------//
	
	public void addModuleWrapper(ModuleWrapper wrapper, boolean registerToHistory, boolean repaint) {
		addModuleWrappers(new ModuleWrapper[]{wrapper}, registerToHistory, repaint);
	}
	
	public void addModuleWrappers(ModuleWrapper[] wrappers, boolean registerToHistory, boolean repaint) {
		for (ModuleWrapper mw: wrappers) {
			patch.addModuleWrapper(mw);
			moduleTree.getChildren().add(new TreeItem<ModuleWrapper>(mw));
		}
		
		// Register changes in history
		if (registerToHistory) {
			editorSession.commitHistory();
		}

		// Indicate unsaved changes and invalidate cache
		editorSession.registerUnsavedChangePerformed();
		
		if (repaint) {
			repaint();
		}
	}
	
	public void removeModuleWrapper(ModuleWrapper wrapper, boolean connectBypassPath, boolean registerToHistory, boolean repaint) {
		ArrayList<ModuleConnector> bypassConnectors = new ArrayList<ModuleConnector>();
		if (connectBypassPath && wrapper.isBypassable()) {
			// Find bypass paths
			ModuleWrapperIO bypassInput = wrapper.getWrapperIoByModuleIo(wrapper.module.getBypassInput());
			List<ModuleConnector> bypassInputConnectors = patch.getModuleConnectorsByWrapperIo(bypassInput);
			
			ModuleWrapperIO bypassOutput = wrapper.getWrapperIoByModuleIo(wrapper.module.getOutput(0));
			List<ModuleConnector> bypassOutputConnectors = patch.getModuleConnectorsByWrapperIo(bypassOutput);
			
			if (!bypassInputConnectors.isEmpty()) {
				ModuleConnector c1 = bypassInputConnectors.get(0);
				for (ModuleConnector c2: bypassOutputConnectors) {
					bypassConnectors.add(new ModuleConnector(c1.module1, c1.module1Io, c2.module2, c2.module2Io));
				}
			}
		}
		
		removeModuleWrappers(new ModuleWrapper[]{wrapper}, registerToHistory, repaint);
		
		if (!bypassConnectors.isEmpty()) {
			// Connect bypass paths if available
			addModuleConnectors(bypassConnectors.toArray(new ModuleConnector[0]), registerToHistory, repaint);
		}
	}
	
	public void removeModuleWrappers(ModuleWrapper[] wrappers, boolean registerToHistory, boolean repaint) {
		ArrayList<ModuleConnector> removedConnectors = new ArrayList<ModuleConnector>();
		
		for (ModuleWrapper mw: wrappers) {
			//Remove module and add register the removed connectors
			List<ModuleConnector> rc = patch.removeModuleWrapper(mw);
			removedConnectors.addAll(rc);
			
			//Remove item from tree
			for (TreeItem<ModuleWrapper> item: moduleTree.getChildren()) {
				if (item.getValue() == mw) {
					moduleTree.getChildren().remove(item);
					break;
				}
			}
			
			//Remove module from selection
			removeWrapperFromSelection(mw);
		}

		// Register changes in history
		if (registerToHistory) {
			//First we concentrate all the subjects into one array
			Object[] subjects = new Object[wrappers.length + removedConnectors.size()];
			for (int i = 0; i < wrappers.length; i++) {
				subjects[i] = wrappers[i];
			}
			for (int i = 0; i < removedConnectors.size(); i++) {
				subjects[wrappers.length + i] = removedConnectors.get(i);
			}
			editorSession.commitHistory();
		}

		// Indicate unsaved changes and invalidate cache
		editorSession.registerUnsavedChangePerformed();
		
		if (repaint) {
			repaint();
		}
	}
	
	public void addModuleConnector(ModuleConnector connector, boolean registerToHistory, boolean repaint) {
		addModuleConnectors(new ModuleConnector[] {connector}, registerToHistory, repaint);
	}
	
	public void addModuleConnectors(ModuleConnector[] connectors, boolean registerToHistory, boolean repaint) {
		for (ModuleConnector mc: connectors) {
			patch.addModuleConnector(mc);
		}
		
		// Register changes in history
		if (registerToHistory) {
			editorSession.commitHistory();
		}

		// Indicate unsaved changes and invalidate cache
		editorSession.registerUnsavedChangePerformed();

		if (repaint) {
			repaint();
		}
	}
	
	public void removeModuleConnector(ModuleConnector connector, boolean registerToHistory, boolean repaint) {
		removeModuleConnectors(new ModuleConnector[] {connector}, registerToHistory, repaint);
	}
	
	public void removeModuleConnectors(ModuleConnector[] connectors, boolean registerToHistory, boolean repaint) {
		for (ModuleConnector mc: connectors) {
			patch.removeModuleConnector(mc);
		}
		
		// Register changes in history
		if (registerToHistory) {
			editorSession.commitHistory();
		}
		
		// Indicate unsaved changes and invalidate cache
		editorSession.registerUnsavedChangePerformed();

		if (repaint) {
			repaint();
		}
	}
	
	void setTempWrapper(ModuleWrapper tempWrapper) {
		this.tempWrapper = tempWrapper;
		if (tempWrapper != null) {
			tempWrapper.posX = mouseListener.currentMouseCoordinateX;
			tempWrapper.posY = mouseListener.currentMouseCoordinateY;
		}
		repaint();
	}
	
	void applyTempWrapper() {
		addModuleWrapper(tempWrapper, true, true);
		// Instance a new device of same type
		try {
			tempWrapper = new ModuleWrapper(tempWrapper.module.getClass(), patch, tempWrapper.posX, tempWrapper.posY);
		} catch (Exception e) {
			e.printStackTrace();
			tempWrapper = null;
		}
	}
	
	void setTempPatch(TempPatch tempPatch) {
		this.tempPatch = tempPatch;
		if (tempPatch != null) {
			tempPatch.setCenterTo(mouseListener.currentMouseCoordinateX, mouseListener.currentMouseCoordinateY);
		}
		repaint();
	}
	
	void applyTempPatch() {
		ArrayList<Object> added = new ArrayList<Object>();
		
		for (ModuleWrapper mw: tempPatch.getModuleWrapperList()) {
			addModuleWrapper(mw, false, false);
			added.add(mw);
		}
		for (ModuleConnector mc: tempPatch.getModuleConnectorList()) {
			addModuleConnector(mc, false, false);
			added.add(mc);
		}
		tempPatch.reinstance();
		
		// Register changes in history
		editorSession.commitHistory();

		// Register unsaved changes
		editorSession.registerUnsavedChangePerformed();

		repaint();
	}
	
	public void openModuleParametersEditor(ModuleWrapper wrapper) {
		new ModuleParametersStage(wrapper, e -> {
			// Register changes in history
			editorSession.commitHistory();

			// Indicate unsaved changes and invalidate cache
			editorSession.registerUnsavedChangePerformed();

			editorSession.updatePreview();
		});
	}
	
	public void renameModuleWrapper(ModuleWrapper wrapper, String customName) {
		if (wrapper.getCustomName().equals(customName)) return;
		String oldName = wrapper.getCustomName();
		wrapper.setCustomName(customName);

		// Commit changes in history
		editorSession.commitHistory();
		
		// Register unsaved changes
		editorSession.registerUnsavedChangePerformed();

		WorldSynthEditor.getPatchTreeView().refresh();
		repaint();
	}
	
	public void bypassModuleWrapper(ModuleWrapper wrapper, boolean bypass) {
		if (wrapper.isBypassed() == bypass) return;
		boolean oldBypassState = wrapper.isBypassed();
		wrapper.setBypassed(bypass);

		// Register changes in history
		editorSession.commitHistory();

		// Indicate unsaved changes and invalidate cache
		editorSession.registerUnsavedChangePerformed();

		repaint();
		editorSession.updatePreview();
	}
	
	//---------------------------------------------------------------------------//
	//----------------------------- EDITS SELECTION -----------------------------//
	//---------------------------------------------------------------------------//
	
	public ModuleWrapper[] getSelectedWrappers() {
		ModuleWrapper[] wrappers = new ModuleWrapper[0];
		wrappers = selectedWrappers.toArray(wrappers);
		return wrappers;
	}
	
	public ModuleWrapper getLatestSelectedWrapper() {
		if (selectedWrappers.size() == 0) {
			return null;
		}
		return selectedWrappers.get(selectedWrappers.size() - 1);
	}
	
	public void setSelectedWrapper(ModuleWrapper wrapper) {
		if (wrapper == null) {
			return;
		}
		
		setSelectedWrappers(new ModuleWrapper[]{wrapperOver}, false);
		//Update preview
		getEditorSession().updatePreviewWrapper(getLatestSelectedWrapper());

		repaint();
	}
	
	private boolean ignoreSelectionCallFromTreeView = false;
	public void setSelectedWrappers(ModuleWrapper[] wrappers, boolean calledFromTreview) {
		if (calledFromTreview && ignoreSelectionCallFromTreeView) {
			return;
		}
		
		//TODO check if new selection has single new selection and use that for updating preview in that case
		
		//Check that the list of devices is not empty
		if (wrappers.length == 0) {
			return;
		}
		//Check that the devices is part of the synth being edited in this editor
		for (ModuleWrapper mw: wrappers) {
			if (!patch.containsModuleWrapper(mw)) {
				return;
			}
		}
		
		//If it contains all the devices set the selection
		//Clear selection and add the device as the only entry
		selectedWrappers.clear();
		for (ModuleWrapper mw: wrappers) {
			selectedWrappers.add(mw);
		}
		
		if (calledFromTreview) { 
			//Update preview
			getEditorSession().updatePreviewWrapper(getLatestSelectedWrapper());
		}
		else {
			//Update tree view with latest selected devices
			updateTreeSelection(getSelectedWrappers());
		}
		
		repaint();
	}
	
	public void toggleWrapperSelection(ModuleWrapper wrapper) {
		//Check that the device is part of the synth being edited in this editor
		if (patch.containsModuleWrapper(wrapper)) {
			//Check if the device is already contained in the selection
			//If the device already is part of the selection remove it so the instance only is contained once when added at the end
			boolean updatePreview = false;
			if (selectedWrappers.contains(wrapper)) {
				if (getLatestSelectedWrapper() == wrapper) {
					selectedWrappers.remove(wrapper);
				}
				else {
					selectedWrappers.remove(wrapper);
					selectedWrappers.add(wrapper);
					updatePreview = true;
				}
			}
			else {
				selectedWrappers.add(wrapper);
				updatePreview = true;
			}
			
			//Update tree view with latest selected devices
			updateTreeSelection(getSelectedWrappers());
			//Update preview
			if (updatePreview) {
				WorldSynthEditor.updatePreviewWrapper(getLatestSelectedWrapper(), getEditorSession().getBuildCache());
			}
			
			repaint();
		}
	}
	
	public void removeWrapperFromSelection(ModuleWrapper wrapper) {
		selectedWrappers.remove(wrapper);
		
		repaint();
		
		//Update tree view with latest selected devices
		updateTreeSelection(getSelectedWrappers());
	}
	
	public void clearSelectedWrappers() {
		selectedWrappers.clear();
		
		repaint();
		
		//Update tree view with latest selected devices
		updateTreeSelection(getSelectedWrappers());
	}
	
	private void updateTreeSelection(ModuleWrapper[] selectedWrappers) {
		//Update tree selection
		ignoreSelectionCallFromTreeView = true;
		WorldSynthEditor.getPatchTreeView().setSelection(selectedWrappers);
		ignoreSelectionCallFromTreeView = false;
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- PAINT ----------------------------------//
	//---------------------------------------------------------------------------//
	
	public void repaint() {
		paintEditor();
	}
	
	private void paintEditor() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(Color.web("#303030"));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// Draw background grid if it's enabled in preferences
		if (PatchEditorPreferences.showGrid.getValue()) {
			paintGrid(g);
		}
		
		//Draw deviceconnectors
		for (ModuleConnector mc: patch.getModuleConnectorList()) {
			Connectors.paintConnector(this, g, mc);
		}
		
		//Draw devices
		for (ModuleWrapper mw: patch.getModuleWrapperList()) {
			Wrappers.paintWrapper(this, g, mw);
		}
		
		//Draw the temp device
		if (tempWrapper != null) {
			Wrappers.paintWrapper(this, g, tempWrapper);
		}
		
		//Draw the temp deviceconnector
		if (tempConnector != null) {
			Connectors.paintConnector(this, g, tempConnector);
		}
		
		//Draw the tempsynth
		if (tempPatch != null) {
			for (ModuleWrapper mw: tempPatch.getModuleWrapperList()) {
				Wrappers.paintWrapper(this, g, mw);
			}
			for (ModuleConnector mc: tempPatch.getModuleConnectorList()) {
				Connectors.paintConnector(this, g, mc);
			}
		}
		
		//Draw the selectionrectangle
		if (selectionRectangle != null) {
			selectionRectangle.paint(this, g);
		}
		
		//Draw the selectionlasso
		if (selectionLasso != null) {
			selectionLasso.paint(this, g);
		}
		
		//TODO Draw more when implementing grouping, comments...
	}
	
	private void paintGrid(GraphicsContext g) {
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(1.0, zoom*2-0.5))));
		g.setLineWidth(1.0);
		float gridIncrement = 50;
		for (float cx = gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x < getWidth(); cx += gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for (float cx = -gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x > 0; cx -= gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for (float cy = gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y < getHeight(); cy += gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		for (float cy = -gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y > 0; cy -= gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		
		//Draw center cross;
		g.setStroke(Color.web("#606060"));
		Pixel centerCoordinatePixel = new Pixel(new Coordinate(0, 0), this);
		g.strokeLine(centerCoordinatePixel.x, 0, centerCoordinatePixel.x, getHeight());
		g.strokeLine(0, centerCoordinatePixel.y, getWidth(), centerCoordinatePixel.y);
	}
	
	//---------------------------------------------------------------------------//
	//---------------------------------- EVENT ----------------------------------//
	//---------------------------------------------------------------------------//
	
	class MouseListener implements EventHandler<MouseEvent> {
		public MouseEvent currentEvent;
		
		public double lastMouseCoordinateX, lastMouseCoordinateY;
		public double currentMouseCoordinateX, currentMouseCoordinateY;
		public boolean mouseOverEditor = false;

		LinkedHashMap<ModuleWrapper, Coordinate> moveOldCoordinates;
		boolean dragConnector = false;
		ModuleConnector replacingConnector = null; // The current tempConnector is replacing a previous connector
		
		ArrayList<MouseAction> mouseActions = new ArrayList<MouseAction>();
		
		public MouseListener() {
			mouseActions.add(new MouseActionMouseMove());
			mouseActions.add(new MouseActionViewMove());
			
			mouseActions.add(new MouseActionContextmenuAddModule());
			mouseActions.add(new MouseActionContextmenuEditModule());
			
			mouseActions.add(new MouseActionModuleSelect());
			mouseActions.add(new MouseActionModuleMove());
			mouseActions.add(new MouseActionModuleMoveEnd());
			mouseActions.add(new MouseActionModuleOpenParameters());
			
			mouseActions.add(new MouseActionConnectorStart());
			mouseActions.add(new MouseActionConnectorEnd());
			mouseActions.add(new MouseActionConnectorCancel());
			
			mouseActions.add(new MouseActionConnectorDragStart());
			mouseActions.add(new MouseActionConnectorDragUpdate());
			mouseActions.add(new MouseActionConnectorDragEnd());
			mouseActions.add(new MouseActionConnectorDragCancel());
			
			mouseActions.add(new MouseActionTempModuleMove());
			mouseActions.add(new MouseActionTempModuleApply());
			mouseActions.add(new MouseActionTempModuleCancel());
			
			mouseActions.add(new MouseActionTempPatchMove());
			mouseActions.add(new MouseActionTempPatchApply());
			mouseActions.add(new MouseActionTempPatchCancel());
			
			mouseActions.add(new MouseActionAreaSelectionStart());
			mouseActions.add(new MouseActionAreaSelectionUpdate());
			mouseActions.add(new MouseActionAreaSelectionEnd());
		}
		
		@Override
		public void handle(MouseEvent event) {
			currentEvent = event;
			
			if (event.getEventType() == MouseEvent.MOUSE_CLICKED && !event.isStillSincePress()) {
				// If a click was not still it's not regarded as a click and should be ignored... PERIOD
				return;
			}
			if (event.getEventType() == MouseEvent.MOUSE_DRAGGED && event.isStillSincePress()) {
				// This should be an impossibility, but I'll have a case to ignore it either anyway
				return;
			}
			if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
				wrapperOver = null;
				wrapperIoOver = null;
				mouseOverEditor = false;
				repaint();
			}
			if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
				mouseOverEditor = true;
				repaint();
			}
			
			lastMouseCoordinateX = currentMouseCoordinateX;
			lastMouseCoordinateY = currentMouseCoordinateY;
			
			// Update mouse coodinate position
			Coordinate mouseCoordinate = new Coordinate(new Pixel(event.getX(), event.getY()), PatchEditorPane.this);
			currentMouseCoordinateX = mouseCoordinate.x;
			currentMouseCoordinateY = mouseCoordinate.y;
			
			fireAppropriateAction();
		}
		
		private void fireAppropriateAction() {
			for (MouseAction action: mouseActions) {
				if (action.filterEvent(PatchEditorPane.this, this)) {
					action.action(PatchEditorPane.this, this);
				}
			}
		}
	}
	
	class KeyboardListener implements EventHandler<KeyEvent> {
		public KeyEvent currentEvent;
		
		ArrayList<KeyboardAction> keyActions = new ArrayList<KeyboardAction>();
		
		public KeyboardListener() {
			keyActions.add(new KeyboardActionEscape());
			
			keyActions.add(new KeyboardActionModuleDelete());
			keyActions.add(new KeyboardActionModuleRename());
			keyActions.add(new KeyboardActionModuleBypass());
			keyActions.add(new KeyboardActionModuleOpenParameters());
			
			keyActions.add(new KeyboardActionSearchAddModule());
			
			keyActions.add(new KeyboardActionCut());
			keyActions.add(new KeyboardActionCopy());
			keyActions.add(new KeyboardActionPaste());
		}
		
		@Override
		public void handle(final KeyEvent event) {
			if (!mouseListener.mouseOverEditor) {
//				return;
			}
			if (moduleSearchPopup.isShowing()) {
				//Ignore keyboard actions if device search is open
				return;
			}
			currentEvent = event;
			
			fireAppropriateAction(event);
		}
		
		private void fireAppropriateAction(final KeyEvent event) {
			for (KeyboardAction action: keyActions) {
				if (action.filterEvent(PatchEditorPane.this, this)) {
					action.action(PatchEditorPane.this, this);
				}
			}
		}
	}
	
	public EventHandler<KeyEvent> getKeyboardListener() {
		return keyboardListener;
	}
}