/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.syntheditor.history;

import net.worldsynth.util.event.HistoricalEvent;

import java.util.ArrayList;

public class EditorHistory {

	private ArrayList<HistoricalEvent> stagedHistory;
	private ArrayList<ArrayList<HistoricalEvent>> history = new ArrayList<>();

	private final int maxHistoryLength;
	private int historyIndex = -1;
	
	public EditorHistory(int maxHistoryLength) {
		this.maxHistoryLength = maxHistoryLength;
	}

	public void addHistoryEvent(HistoricalEvent event) {
		if (stagedHistory == null) {
			stagedHistory = new ArrayList<>();
		}

		stagedHistory.add(event);
	}

	public void commit() {
		if (stagedHistory == null || stagedHistory.size() == 0) return;

		// If new history is committed while not at the end of the history, remove old branch from history and create new branch
		while (historyIndex < history.size()-1) {
			history.remove(history.size()-1);
		}

		history.add(stagedHistory);
		stagedHistory = null;
		historyIndex++;

		// If history gets longer than the specified history length, remove the oldest history entry
		if (history.size() > maxHistoryLength) {
			history.remove(0);
			historyIndex--;
		}
	}

	public void clear() {
		stagedHistory = null;
	}
	
	public ArrayList<HistoricalEvent> getLastHistoryEvents() {
		if (historyIndex < 0) {
			return null;
		}
		return history.get(historyIndex--);
	}
	
	public ArrayList<HistoricalEvent> getNextHistoryEvents() {
		if (historyIndex+1 > history.size()-1) {
			return null;
		}
		return history.get(++historyIndex);
	}
}
