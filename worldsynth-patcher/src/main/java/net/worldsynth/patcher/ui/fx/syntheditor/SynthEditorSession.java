/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.fx.syntheditor;

import java.io.File;
import java.util.*;

import net.worldsynth.build.BuildCache;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.util.event.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.control.Tab;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.patcher.data.PatcherDataManager;
import net.worldsynth.patcher.ui.fx.WorldSynthEditor;
import net.worldsynth.patcher.ui.fx.patcheditor.PatchEditorPane;
import net.worldsynth.patcher.ui.syntheditor.history.EditorHistory;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;

public class SynthEditorSession {
	public static final Logger logger = LogManager.getLogger(SynthEditorSession.class);
	
	private final Synth synth;
	private final WorldSynthEditor editor;
	private final EditorHistory history = new EditorHistory(20);
	
	private final HashMap<Patch, Tab> patchTabs = new HashMap<Patch, Tab>();
	private final HashMap<Patch, PatchEditorPane> patchEditors = new HashMap<Patch, PatchEditorPane>();

	private boolean unsavedChanges = false;

	private ModuleWrapper currentPreviewWrapper;
	
	public SynthEditorSession(Synth synth, WorldSynthEditor editor) {
		this.synth = synth;
		this.editor = editor;
		
		openPatch(synth.getPatch());

		synth.addEventHandler(SynthEvent.SYNTH_PATCH, e -> {
			PatchEvent patchEvent = e.getPatchEvent();
			if (patchEvent.getEventType() == PatchEvent.PATCH_MODULE_MODIFIED) {
				WrapperEvent wrapperEvent = patchEvent.getWrapperEvent();
				if (wrapperEvent.getEventType() == WrapperEvent.WRAPPER_MODULE_PARAMETERS_CHANGED || wrapperEvent.getEventType() == WrapperEvent.WRAPPER_BYPASS_CHANGED) {
					invalidateCache(patchEvent.getWrapper());
//					updatePreview();
				}
			}
			else if (patchEvent.getEventType() == PatchEvent.PATCH_CONNECTOR_ADDED || patchEvent.getEventType() == PatchEvent.PATCH_CONNECTOR_REMOVED) {
				invalidateCache((patchEvent.getModuleConnector().module2));
//				updatePreview();
			}

			addHistoryEvent(e);
		});

		synth.addEventHandler(SynthEvent.SYNTH_EXTENT, e -> {
			addHistoryEvent(e);
		});
	}
	
	public Synth getSynth() {
		return synth;
	}
	
	public void openPatch(Patch patch) {
		if (patchTabs.containsKey(patch)) {
			editor.editorTabPane.getSelectionModel().select(patchTabs.get(patch));
			return;
		}
		
		// If the patch is not the main patch of the synth, indicate that it is a macro with "[M]"
		Tab patchEditorTab = new Tab(patch == synth.getPatch() ? synth.getName() : "[M] " + synth.getName());
		if (hasUnsavedChanges()) {
			patchEditorTab.setText(patch == synth.getPatch() ? "*" + synth.getName() : "*[M] " + synth.getName());
		}
		
		PatchEditorPane patchEditor = patchEditors.get(patch);
		if (patchEditor == null) {
			patchEditor = new PatchEditorPane(patch, this);
		}
		patchEditorTab.setContent(patchEditor);
		
		patchEditorTab.setOnClosed(e -> {
			patchTabs.remove(patch);
			
			if (patch == synth.getPatch()) {
				editor.closeSynth(synth);
			}
		});
		
		patchTabs.put(patch, patchEditorTab);
		patchEditors.put(patch, patchEditor);
		
		editor.editorTabPane.getTabs().add(patchEditorTab);
		editor.editorTabPane.getSelectionModel().select(patchEditorTab);
	}
	
	public void repaintPatch(Patch patch) {
		if (patchEditors.containsKey(patch)) {
			patchEditors.get(patch).repaint();
		}
	}
	
	public void save() {
		File file = synth.getFile();
		if (file == null) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open WorldSynth project");
			fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

			file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		}
		if (file == null) {
			return;
		}
		
		String synthName = file.getName();
		synthName = synthName.substring(0, synthName.lastIndexOf(".wsynth"));
		renameSynthInEditor(synthName);
		synth.setFile(file);
		
		SynthManager.saveSynth(synth);
		PatcherDataManager.getWorkspaceData().setMostRecentSynth(file);
		
		unsavedChanges = false;
		for (Patch p: patchTabs.keySet()) {
			if (p == synth.getPatch()) {
				patchTabs.get(p).setText(synth.getName());
			}
			else {
				patchTabs.get(p).setText("[M] " + synth.getName());
			}
		}
	}
	
	public void saveAs() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

		File file = fileChooser.showSaveDialog(WorldSynthPatcher.primaryStage);
		if (file == null) {
			return;
		}
		synth.setFile(file);
		
		save();
	}
	
	private void renameSynthInEditor(String name) {
		synth.setName(name);
		Tab mainPatchTab = patchTabs.get(synth.getPatch());
		mainPatchTab.setText(name);
	}
	
	public boolean hasUnsavedChanges() {
		return unsavedChanges;
	}
	
	public void registerUnsavedChangePerformed() {
		unsavedChanges = true;
		for (Patch p: patchTabs.keySet()) {
			if (p == synth.getPatch()) {
				patchTabs.get(p).setText("*" + synth.getName());
			}
			else {
				patchTabs.get(p).setText("*[M] " + synth.getName());
			}
		}
		editor.makePreviewRefreshable();
	}
	
	public void closeAllPatches() {
		Iterator<Patch> it = patchTabs.keySet().iterator();
		while (it.hasNext()) {
			Patch p = it.next();
			editor.editorTabPane.getTabs().remove(patchTabs.get(p));
			patchEditors.remove(p);
			it.remove();
		}
	}
	
	public EditorHistory getHistory() {
		return history;
	}
	
	public void addHistoryEvent(HistoricalEvent event) {
		history.addHistoryEvent(event);
	}

	public void commitHistory() {
		history.commit();
	}
	
	public void undo() {
		ArrayList<HistoricalEvent> historyEvents = history.getLastHistoryEvents();
		if (historyEvents != null) {
			// Undo events in reverse order
			for (int i = historyEvents.size()-1; i >= 0; i--) {
				historyEvents.get(i).undo();
			}
			// Discard events caused by the undo operation
			history.clear();
		}
	}
	
	public void redo() {
		ArrayList<HistoricalEvent> historyEvents = history.getNextHistoryEvents();
		if (historyEvents != null) {
			for (HistoricalEvent e: historyEvents) {
				e.redo();
			}
			// Discard events caused by the redo operation
			history.clear();
		}
	}

	public static void updatePreview() {
		WorldSynthEditor.updatePreview();
	}

	public static void updatePreview(boolean clearExtentsEditorPreview) {
		WorldSynthEditor.updatePreview(clearExtentsEditorPreview);
	}

	public void updatePreviewWrapper(ModuleWrapper moduleWrapper) {
		WorldSynthEditor.updatePreviewWrapper(moduleWrapper, getBuildCache());
	}

	public BuildCache getBuildCache() {
		return editor.buildCache;
	}

	public Set<ModuleWrapper> invalidateCache(ModuleWrapper wrapper) {
		ArrayList<ModuleWrapper> wrappersList = new ArrayList<>();
		wrappersList.add(wrapper);
		return invalidateCache(wrappersList);
	}

	public Set<ModuleWrapper> invalidateCache(List<ModuleWrapper> wrappers) {
		Set<ModuleWrapper> invalidatedSet = traverseForward(wrappers);
		for (ModuleWrapper wrapper: invalidatedSet) {
			getBuildCache().clearCachedData(wrapper);
		}
		return invalidatedSet;
	}

	private Set<ModuleWrapper> traverseForward(List<ModuleWrapper> wrappers) {
		Set<ModuleWrapper> traversed = Collections.newSetFromMap(new IdentityHashMap<>());
		for (ModuleWrapper wrapper: wrappers) {
			traverseForward(wrapper, traversed);
		}
		return traversed;
	}

	private void traverseForward(ModuleWrapper wrapper, Set<ModuleWrapper> traversed) {
		// Add the module to the traversed set
		traversed.add(wrapper);

		// Traverse patch forward from the wrapper to dependents
		for (ModuleWrapperIO output : wrapper.wrapperOutputs.values()) {
			List<ModuleConnector> connectors = wrapper.getParentPatch().getModuleConnectorsByWrapperIo(output);
			for (ModuleConnector con : connectors) {
				ModuleWrapper nextWrapper = con.module2;
				if (!traversed.contains(nextWrapper)) {
					traverseForward(nextWrapper, traversed);
				}
			}
		}
	}
}
