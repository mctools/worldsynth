/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.patcher.WorldSynthPatcher;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.Synth;

public class SynthParametersStage extends UtilityStage {
	
	public SynthParametersStage(Synth synth) {
		setTitle("Synth properties - " + synth.getName());
		BorderPane rootPane = new BorderPane();
		
		GridPane propertiesPane = new GridPane();
		propertiesPane.setPadding(new Insets(10.0));
		propertiesPane.setHgap(2.0);
		propertiesPane.setVgap(5.0);
		
		// Add properties to editor
		ArrayList<ParameterUiElement<?>> parameterUis = new ArrayList<>();
		int i = 0;
		for (AbstractParameter<?> parameter: synth.getParameters().getParameters()) {
			ParameterUiElement<?> parameterUi = parameter.getUi();
			parameterUis.add(parameterUi);
			parameterUi.addToGrid(propertiesPane, i++);
		}
		
		// Bottom line buttons
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> {
			close();
		});
		
		Button applyButton = new Button("Apply");
		applyButton.setOnAction(e -> {
			for (ParameterUiElement<?> parameterUi: parameterUis) {
				parameterUi.applyUiValue();
			}
		});
		
		Button okButton = new Button("OK");
		okButton.setOnAction(e -> {
			close();
			for (ParameterUiElement<?> parameterUi: parameterUis) {
				parameterUi.applyUiValue();
			}
		});
		
		HBox buttonsPane = new HBox();
		buttonsPane.setPadding(new Insets(0.0, 10.0, 5.0, 10.0));
		
		Pane spacerPane = new Pane();
		HBox.setHgrow(spacerPane, Priority.SOMETIMES);
		buttonsPane.getChildren().add(spacerPane);
		
		buttonsPane.getChildren().add(cancelButton);
		buttonsPane.getChildren().add(applyButton);
		buttonsPane.getChildren().add(okButton);
		
		rootPane.setCenter(propertiesPane);
		rootPane.setBottom(buttonsPane);
		
		Scene scene = new Scene(rootPane, rootPane.getPrefWidth(), rootPane.getPrefHeight());
		scene.getStylesheets().add(WorldSynthPatcher.stylesheet);
		setScene(scene);
		sizeToScene();
	}
	
}
